package thread;
import java.io.BufferedReader;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import test.ProcesosRecurrentes;
import test.TiposDeEndoso;
import utils.BrowserFactory;
import utils.DriverFactory;
import utils.JavaTools;
import utils.SeleniumTools;

public class ThreadEndoso extends Thread {
	
	JavaTools jt = new JavaTools();
	SeleniumTools st = new SeleniumTools();
	ProcesosRecurrentes procesosRecurrentes = new ProcesosRecurrentes();
	
	public WebDriver driver = null;
	String linea;
	String lineaNrosPoliza;
	
	Logger logger=Logger.getLogger("ThreadEndoso");
	String rutaDirectorio=System.getProperty("user.dir");
	
	BufferedReader bufer = jt.abrirLectura(rutaDirectorio,"ParametrosAmbiente.txt");
	String lineaParametrosAmbiente = jt.lecturaLinea(bufer);
	String separador1[] = lineaParametrosAmbiente.split("=");
	String PaginaWeb = separador1[3];// Direccion http de la prueba
	int tiempoDelay = Integer.parseInt(separador1[0]);// tiempo de delay de automatizacion
	int tiempoDivWait = Integer.parseInt(separador1[1]);// tiempo wait Big Layer
	int tiempoThrowWait = Integer.parseInt(separador1[2]);// tiempo de los wait
	
	
	
	String ruta = rutaDirectorio + "/nrosEndosos.txt";
	String rutaParametrosEndoso = rutaDirectorio + "/ParametrosEndoso.txt";
	


	public ThreadEndoso(String linea, String lineaNrosPoliza) {
		super();
		this.linea = linea;
		this.lineaNrosPoliza=lineaNrosPoliza;
		
		driver = BrowserFactory.CreateInstance("firefox");
		DriverFactory.getInstance().setDriver(driver);
		driver= DriverFactory.getInstance().getDriver();
		
	}

	public void run () {
		
		//ABRIENDO LOG DE ESTADO EJECUCION
		String rutaestadoEjecucion = rutaDirectorio + "/estadoEjecucion.txt";
				
		try {
			JavaTools jt = new JavaTools();
			TiposDeEndoso tiposDeEndoso = new TiposDeEndoso();
			ProcesosRecurrentes procesosRecurrentes = new ProcesosRecurrentes();
				
			Logger logger = Logger.getLogger("testEndoso");
			PropertyConfigurator.configure("Log4j.properties");

			String SecurityLogin = separador1[4];// login
			String SecurityPassword = separador1[5];// password
			String USER_PREFERENCE_COUNTRY_NAME = separador1[6];// USER_PREFERENCE_COUNTRY_NAME
			String PARAMETER_LOCALE_NAME = separador1[7];// PARAMETER_LOCALE_NAME

			logger.info("SecurityLogin: " + SecurityLogin + " \n");
			logger.info("SecurityPassword: " + SecurityPassword + " \n");
			logger.info("USER_PREFERENCE_COUNTRY_NAME: " + USER_PREFERENCE_COUNTRY_NAME + " \n");
			logger.info("PARAMETER_LOCALE_NAME: " + PARAMETER_LOCALE_NAME + " \n");
			jt.cerrarLectura(bufer);

			logger.info("Parametros \n");
			logger.info("driverPath: " + rutaDirectorio + " \n");
			logger.info("PaginaWeb: " + PaginaWeb + " \n");
			logger.info("******************* \n");
			logger.info("Open Browser and navigate to designed URL \n");
				
			driver.get(PaginaWeb);
			
			logger.info("tiempo de inicio de test " + java.time.LocalDateTime.now());

			procesosRecurrentes.iniciarSesion(driver, "cotizacion", PaginaWeb, SecurityLogin, SecurityPassword,
					USER_PREFERENCE_COUNTRY_NAME, PARAMETER_LOCALE_NAME);

			procesosRecurrentes.validandoSesion(driver, PaginaWeb);
			
			
			///////ENTRAR EN EL MENU
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e2) {
				
				e2.printStackTrace();
			}
			WebElement elemento = driver
					.findElement(By.id("Menu2_2_4"));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click()", elemento);

			st.cambiarVentana(driver);
			
			///////////////////////////
			String separador2[] = this.linea.split("=");
			
			logger.info("info separador 2: "+separador2[0]+"\n");
			

			String NroPoliza = separador2[0];// NroPoliza
			String tipoEndoso=(separador2[1]); // Tipo de endoso
			
			//Accediendo al menu endoso y procediendo con el endoso
			try {
				
				if(NroPoliza.equals("NO")){
					procesosRecurrentes.endosar(driver, tiempoThrowWait, logger, tiempoDivWait, lineaNrosPoliza, tipoEndoso,"IngresarSolicitud");
				}
				else {
					procesosRecurrentes.endosar(driver, tiempoThrowWait, logger, tiempoDivWait, NroPoliza, tipoEndoso,"IngresarSolicitud");
				}	
				
			} catch (InterruptedException e1) {
				
				e1.printStackTrace();
			}
			

			if(separador2[1].equals("Endosos Sin Valor")){ //VERIFICA EL SI EL ENDOSO ES CON VALOR O SIN VALOR
				
				logger.info("Procediendo endoso sin valor \n");
					
				if(separador2[2].equals("Cambio en Beneficiarios")) { //Cambio en Beneficiarios
					
					String beneficiarioName = separador2[4]; // Nombre predeterminado del beneficiario
					String TBeneficiario = separador2[5]; //Tipo de beneficiario
					try {
						tiposDeEndoso.cambioBeneficiario(driver,logger, beneficiarioName, TBeneficiario, tiempoThrowWait, tiempoDivWait);
					} catch (InterruptedException e) {
						
						e.printStackTrace();
					}
				}

				
			}
			else{//Endosos Con Valor
				logger.info("Procediendo endoso con valor \n");
			
					if(separador2[2].equals("Aporte Extraordinario")) { //Aporte Extraordinario
					
						String monto = separador2[4];
						try {
							tiposDeEndoso.aporteExtraordinario(driver, logger, monto, tiempoThrowWait, tiempoDivWait);
						} catch (InterruptedException e) {
							
							e.printStackTrace();
						}	
						driver.findElement(By.id("OAVidaInversion_PeriodoCobertura")).click();
						st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
						driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[2]/div[2]/div[2]/div[3]/div/div[2]/div/div[5]/div/div[2]/div[2]/div/div/div[3]/form/div[2]/div[2]/div[3]/div/input[2]")).click();
						st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
							
				}
				
			}

			// CALCULAR
			logger.info("Haciendo click en btn calcular calculateButton 1\n");
			try {
				procesosRecurrentes.calcularEndoso(driver, tiempoThrowWait, logger, tiempoDivWait);
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
			
			driver.quit(); // SE DESCOMENTA PARA CERRAR LA VENTANA AL TERMINAR EL ENDOSO
			
			///////////////////////INICIANDO NAVEGADOR PARA AUTENTIFICAR ENDOSO ////////////////////////////////////////////
						
			driver = st.iniciarDriver(PaginaWeb, rutaDirectorio+"/geckodriver", tiempoDelay);	

			logger.info("Open Browser and navigate to designed URL \n");
			driver.get(PaginaWeb);
			driver.manage().window();
			
				
			procesosRecurrentes.iniciarSesion(driver, "cotizacion", PaginaWeb, SecurityLogin, SecurityPassword,
					USER_PREFERENCE_COUNTRY_NAME, PARAMETER_LOCALE_NAME);

			procesosRecurrentes.validandoSesion(driver, PaginaWeb);

			logger.info("login exitoso \n");
			
			

			///////////////////////////////////AUTENTIFICAR ENDOSO/////////////////////////////////////////////////////////
			
			logger.info("AUTENTICAR ENDOSO" + "\n");
			
			///////ENTRAR EN EL MENU
			WebElement elemento2 = driver
					.findElement(By.id("Menu2_2_5"));
			JavascriptExecutor executor2 = (JavascriptExecutor) driver;
			executor2.executeScript("arguments[0].click()", elemento2);
			
			st.cambiarVentana(driver);
			
			try {

				if(NroPoliza.equals("NO")){
					procesosRecurrentes.endosar(driver, tiempoThrowWait, logger, tiempoDivWait, lineaNrosPoliza, tipoEndoso,"Autentificar");
				}
				else {
					procesosRecurrentes.endosar(driver, tiempoThrowWait, logger, tiempoDivWait, NroPoliza, tipoEndoso,"Autentificar");
				}	

				
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
			
								
			if(separador2[2].equals("Cambio en Beneficiarios")) { //Cambio en Beneficiarios
				logger.info("Procediendo confirmar endoso sin valor de cambio de beneficiario\n");
							
				//SELECCIONANDO CAMBIO EN BENEFICIARIO
				driver.findElement(By.xpath("/html/body/div[6]/form/div/div[2]/div/div/div/div[2]/div/div/div/div/div[2]/div[2]/div[1]/div[1]/table/tbody/tr/td[1]/div[4]/div/div/div[4]/div[2]/div/input")).click();
				logger.info("seleccionando Cambio en Beneficiario \n");
				st.waitSearchWicket(driver);
				
				//CONTINUAR ENDOSO
				driver.findElement(By.name("EventSection:content:Form:continueButton")).click();
				logger.info("seleccionando Continuar \n");
				st.waitSearchWicket(driver);
				}
			
				//APORTE EXTRAORDINARIO
				else if(separador2[2].equals("Aporte Extraordinario")) { //Aporte Extraordinario
				
				logger.info("Procediendo a confirmar endoso con valor, aporte extraordinario \n");
				
				//SELECCIONANDO APORTE EXTRAORDINARIO
				st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
				driver.findElement(By.xpath("/html/body/div[6]/form/div/div[2]/div/div/div/div[2]/div/div/div/div/div[2]/div[2]/div[1]/div[1]/table/tbody/tr/td[1]/div[4]/div/div/div[4]/div[8]/div/input")).click();
				logger.info("seleccionando Cambio en Beneficiario \n");
				st.waitSearchWicket(driver);
				
				//CONTINUAR ENDOSO
				driver.findElement(By.name("EventSection:content:Form:continueButton")).click();
				logger.info("seleccionando Continuar \n");
				st.waitSearchWicket(driver);
				}
			
				else{
				logger.info("El parametro 2 esta mal configurado, las opciones son: Aporte Extraordinario o Cambio en Beneficiarios \n");
				}
			
			// CALCULAR
			logger.info("Haciendo click en btn calcular calculateButton 2\n");
			try {
				procesosRecurrentes.calcularEndoso(driver, tiempoThrowWait, logger, tiempoDivWait);
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
			
			driver.quit(); // SE DESCOMENTA PARA CERRAR LA VENTANA AL TERMINAR LA APROBACION DEL ENDOSO
			
			///////////////////////INICIANDO NAVEGADOR PARA APROBAR ENDOSO ////////////////////////////////////////////
			
			driver = st.iniciarDriver(PaginaWeb, rutaDirectorio+"/geckodriver", tiempoDelay);	

			logger.info("Open Browser and navigate to designed URL \n");
			driver.get(PaginaWeb);
			driver.manage().window();
			

			procesosRecurrentes.iniciarSesion(driver, "cotizacion", PaginaWeb, SecurityLogin, SecurityPassword,
			USER_PREFERENCE_COUNTRY_NAME, PARAMETER_LOCALE_NAME);

			procesosRecurrentes.validandoSesion(driver, PaginaWeb);

			logger.info("login exitoso \n");

			//Thread.sleep(tiempoThrowWait);
			
			///////////////////////////////////APROBAR ENDOSO/////////////////////////////////////////////////////////
			
			logger.info("APROBAR ENDOSO" + "\n");
			
			///////ENTRAR EN EL MENU
			WebElement elemento3 = driver
					.findElement(By.id("Menu2_2_5"));
			JavascriptExecutor executor3 = (JavascriptExecutor) driver;
			executor3.executeScript("arguments[0].click()", elemento3);
			

			st.cambiarVentana(driver);

			try {
				String AprobarSolicitud=separador2[3];
				
				if(NroPoliza.equals("NO")){
					procesosRecurrentes.endosar(driver, tiempoThrowWait, logger, tiempoDivWait, lineaNrosPoliza, tipoEndoso,AprobarSolicitud);
				}
				else {
					procesosRecurrentes.endosar(driver, tiempoThrowWait, logger, tiempoDivWait, NroPoliza, tipoEndoso,AprobarSolicitud);
				}	
				
				
				
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}

			
			if(separador2[2].equals("Cambio en Beneficiarios")) { //Cambio en Beneficiarios
				logger.info("Procediendo confirmar endoso sin valor de cambio de beneficiario\n");

				//SELECCIONANDO CAMBIO EN BENEFICIARIO
				driver.findElement(By.xpath("/html/body/div[6]/form/div/div[2]/div/div/div/div[2]/div/div/div/div/div[2]/div[2]/div[1]/div[1]/table/tbody/tr/td[1]/div[4]/div/div/div[4]/div[2]/div/input")).click();
				logger.info("seleccionando Cambio en Beneficiario \n");
				st.waitSearchWicket(driver);

				//CONTINUAR ENDOSO
				driver.findElement(By.name("EventSection:content:Form:continueButton")).click();
				logger.info("seleccionando Continuar \n");
				st.waitSearchWicket(driver);
			}

			//APORTE EXTRAORDINARIO
			else if(separador2[2].equals("Aporte Extraordinario")) { //Aporte Extraordinario

				logger.info("Procediendo a confirmar endoso con valor, aporte extraordinario \n");

				//SELECCIONANDO APORTE EXTRAORDINARIO
				driver.findElement(By.xpath("/html/body/div[6]/form/div/div[2]/div/div/div/div[2]/div/div/div/div/div[2]/div[2]/div[1]/div[1]/table/tbody/tr/td[1]/div[4]/div/div/div[4]/div[8]/div/input")).click();
				logger.info("seleccionando Cambio en Beneficiario \n");
				st.waitSearchWicket(driver);

				//CONTINUAR ENDOSO
				driver.findElement(By.name("EventSection:content:Form:continueButton")).click();
				logger.info("seleccionando Continuar \n");
				st.waitSearchWicket(driver);
			}

			else{
				logger.info("El parametro 2 esta mal configurado, las opciones son: Aporte Extraordinario o Cambio en Beneficiarios \n");
			}
			
			//SCREENSHOT
			st.captureScreen(driver, rutaDirectorio+"//Screenshots","Endoso");
			
			// CALCULAR
			logger.info("Haciendo click en btn calcular calculateButton 3\n");
			try {
				procesosRecurrentes.calcularEndoso(driver, tiempoThrowWait, logger, tiempoDivWait);
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
			
			jt.escribirTxt(ruta,"Endosos creados satisfactoriamente \n");
			logger.info("tiempo de fin de test " + java.time.LocalDateTime.now());
			logger.info("Finalizando endosos");
			//CAMBIAR A VENTANA PPAL
			st.cambiarVentanaPPAL(driver);
			driver.close();
			jt.escribirTxt(rutaestadoEjecucion, "ENDOSO OK");
			return;
		} catch (Exception e) {
			st.captureScreen(driver, rutaDirectorio+"//Screenshots","EndosoFAIL");
			jt.escribirTxt(rutaestadoEjecucion, "ENDOSO NOK: "+e.getMessage());
			driver.quit();
			e.printStackTrace();
		}
		
	}
	

	

}
