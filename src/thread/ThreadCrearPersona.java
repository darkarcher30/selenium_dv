package thread;
import java.io.BufferedReader;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import test.ProcesosRecurrentes;
import utils.BrowserFactory;
import utils.DriverFactory;
import utils.JavaTools;
import utils.SeleniumTools;

public class ThreadCrearPersona extends Thread {
	
	JavaTools jt = new JavaTools();
	SeleniumTools st = new SeleniumTools();
	ProcesosRecurrentes procesosRecurrentes = new ProcesosRecurrentes();
	
	public WebDriver driver = null;
	String linea;
	
	Logger logger=Logger.getLogger("testCrearPersona");
	String rutaDirectorio=System.getProperty("user.dir");
	
	BufferedReader bufer = jt.abrirLectura(rutaDirectorio,"ParametrosAmbiente.txt");
	String lineaParametrosAmbiente = jt.lecturaLinea(bufer);
	String separador1[] = lineaParametrosAmbiente.split("=");
	String PaginaWeb = separador1[3];// Direccion http de la prueba
	int tiempoDelay = Integer.parseInt(separador1[0]);// tiempo de delay de automatizacion
	int tiempoDivWait = Integer.parseInt(separador1[1]);// tiempo wait Big Layer
	int tiempoThrowWait = Integer.parseInt(separador1[2]);// tiempo de los wait
	
	String ruta = rutaDirectorio + "/nrosEndosos.txt";
	String rutaParametrosEndoso = rutaDirectorio + "/ParametrosEndoso.txt";
	


	public ThreadCrearPersona(String linea) {
		super();
		this.linea = linea;
		
		driver = BrowserFactory.CreateInstance("firefox");
		DriverFactory.getInstance().setDriver(driver);
		driver= DriverFactory.getInstance().getDriver();
		
	}

	public void run () {
		
		//ABRIENDO LOG DE ESTADO EJECUCION
		String rutaestadoEjecucion = rutaDirectorio + "/estadoEjecucion.txt";
		
		try {
				
		JavaTools jt = new JavaTools();
		ProcesosRecurrentes procesosRecurrentes = new ProcesosRecurrentes();
		
		String rutaPolizasCreadas = rutaDirectorio + "/PolizasCreadas.txt";
		jt.crearTxt(rutaPolizasCreadas);
	
		Logger logger = Logger.getLogger("testCreacionPersonaFisica");
		PropertyConfigurator.configure("Log4j.properties");

		String SecurityLogin = separador1[4];// login
		String SecurityPassword = separador1[5];// password
		String USER_PREFERENCE_COUNTRY_NAME = separador1[6];// USER_PREFERENCE_COUNTRY_NAME
		String PARAMETER_LOCALE_NAME = separador1[7];// PARAMETER_LOCALE_NAME

		logger.info("SecurityLogin: " + SecurityLogin + " \n");
		logger.info("SecurityPassword: " + SecurityPassword + " \n");
		logger.info("USER_PREFERENCE_COUNTRY_NAME: " + USER_PREFERENCE_COUNTRY_NAME + " \n");
		logger.info("PARAMETER_LOCALE_NAME: " + PARAMETER_LOCALE_NAME + " \n");
		jt.cerrarLectura(bufer);

		logger.info("Parametros \n");
		logger.info("driverPath: " + rutaDirectorio + " \n");
		logger.info("PaginaWeb: " + PaginaWeb + " \n");
		logger.info("******************* \n");
		logger.info("Open Browser and navigate to designed URL \n");
			
		driver.get(PaginaWeb);
		
		logger.info("tiempo de inicio de test " + java.time.LocalDateTime.now());

		procesosRecurrentes.iniciarSesion(driver, "cotizacion", PaginaWeb, SecurityLogin, SecurityPassword,
				USER_PREFERENCE_COUNTRY_NAME, PARAMETER_LOCALE_NAME);

		procesosRecurrentes.validandoSesion(driver, PaginaWeb);
		
		
		///////ENTRAR EN EL MENU
		WebElement elemento = driver
				.findElement(By.id("Menu4_2_1"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click()", elemento);

		st.cambiarVentana(driver);
		
		String separador2[] = this.linea.split("=");
		
		logger.info("info separador 2: "+separador2[0]+"\n");
		
		///////////////////////////
		
			
			st.waitSelectPopulatename(driver, 3,"ThirdInformationContent:ThirdInformation:thirdPartyTypes");
			
			Select Ttercero = new Select(driver.findElement(By.name(
					"ThirdInformationContent:ThirdInformation:thirdPartyTypes")));
			Ttercero.selectByVisibleText(separador2[0]);
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			logger.info("Comienza creaci�n de tercero del tipo "+separador2[0]+ "\n");
			
			
				if(separador2[0].equals("Persona Fisica"))
				{ //VERIFICA SI ES PERSONA FISICA

					//CLIENTE INTERNO HSCBC
					logger.info("Cliente interno: "+separador2[1]+ "\n");
					st.waitSelectPopulateid(driver, 3,"_ClienteHSBC");
					Select clientei = new Select(driver.findElement(By.id("_ClienteHSBC")));
					clientei.selectByVisibleText(separador2[1]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			
					//ROL
					logger.info("Rol: "+separador2[2]+ "\n");	
					st.waitSelectPopulatename(driver, 3,"DataTemplate:repeaterPanel1:1:fila:repeaterSelect:1:field");
					Select rol = new Select(driver.findElement(By.id("_Rol")));
					rol.selectByVisibleText(separador2[2]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(3000);
			
					//PAIS DE ORIGEN
					logger.info("Pais de origen: "+separador2[3]+ "\n");
					st.waitSelectPopulateid(driver, 5,"_PaisOrigenTer");
					Select paiso = new Select(driver.findElement(By.id("_PaisOrigenTer")));
					Thread.sleep(1000);
					paiso.selectByVisibleText(separador2[3]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					
			
					//LUGAR DE RESIDENCIA PAIS
					logger.info("Lugar de residencia pais: "+separador2[4]+ "\n");
					st.waitSelectPopulateid(driver, 5,"_LugarResidenciaPais");
					Select lugr = new Select(driver.findElement(By.id("_LugarResidenciaPais")));
					lugr.selectByVisibleText(separador2[4]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(3000);
			
					//NACIONALIDAD
					logger.info("Nacionalidad: "+separador2[5]+ "\n");
					st.waitSelectPopulateid(driver, 5,"_NacionalidadTerc");
					Select naci = new Select(driver.findElement(By.id("_NacionalidadTerc")));
					naci.selectByVisibleText(separador2[5]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(3000);
			
					//NOMBRE
					logger.info("Nombre: "+separador2[6]+ "\n");
					WebElement tbNombre = driver.findElement(By.id("_FirstName"));
					tbNombre.sendKeys(separador2[6]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					
			
					//SALVAR NOMBRE
					driver.findElement(By.id("_PredeterminedName")).click();
					st.waitSearchWicketDisapear(driver, "bigLayer", 5);
					logger.info("Salvando nombre \n");
					Thread.sleep(3000);
			
					//APELLIDO
					logger.info("Apellido: "+separador2[7]+ "\n");
					WebElement tbApellido = driver.findElement(By.id("_LastName"));
					tbApellido.sendKeys(separador2[7]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					st.waitSearchWicketDisapear(driver, "bigLayer", 5);
					
			
					//SALVAR APELLIDO
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando nombre \n");
					Thread.sleep(3000);
				
					//FECHA NACIMIENTO
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					String FechaNac = separador2[8];// Fecha Nacimiento
					logger.info("Fecha Nacimiento " + FechaNac + "\n");
					driver.findElement(By.id("_BirthDate")).sendKeys(FechaNac);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					
			
					//TIPO DE DOCUMENTO DE IDENTIFICACION
					logger.info("_TipoDocIdentificacion: "+separador2[5]+ "\n");
					st.waitSelectPopulateid(driver, 5,"_TipoDocIdentificacion");
					Select tdi = new Select(driver.findElement(By.id("_TipoDocIdentificacion")));
					tdi.selectByVisibleText(separador2[9]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(3000);
			
					//GENERO
					logger.info("_Sex: "+separador2[10]+ "\n");
					st.waitSelectPopulateid(driver, 5,"_Sex");
					Select sex = new Select(driver.findElement(By.id("_Sex")));
					Thread.sleep(1000);
					sex.selectByVisibleText(separador2[10]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(3000);
					

					//INGRESO ANUAL
					logger.info("Ingreso Anual: "+separador2[11]+ "\n");
					WebElement tbIngA = driver.findElement(By.id("_TO_IngresoAnual"));
					tbIngA.sendKeys(separador2[11]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					
			
					//SALVANDO INGRESO ANUAL
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando Ingreso Anual \n");
					Thread.sleep(3000);
					
			
					//INGRESO MENSUAL
					logger.info("Ingreso Mensual: "+separador2[12]+ "\n");
					WebElement tbIngM = driver.findElement(By.id("_TO_IngresoMensualDPS"));
					tbIngM.sendKeys(separador2[12]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			
					//SALVANDO INGRESO MENSUAL
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando Ingreso Mensual \n");
					Thread.sleep(3000);
			
					//OTROS INGRESOS
					logger.info("Otros Ingresos: "+separador2[13]+ "\n");
					WebElement tbIngO = driver.findElement(By.id("_TO_OtrosIngresos"));
					tbIngO.sendKeys(separador2[13]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			
					//SALVANDO OTROS INGRESOS
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando Otros Ingresos \n");
					Thread.sleep(3000);
			
					//FUENTE DE INGRESOS
					logger.info("Fuente de Ingresos: "+separador2[14]+ "\n");
					WebElement tbFuenteIng = driver.findElement(By.id("_EspecifiqueFuente"));
					tbFuenteIng.sendKeys(separador2[14]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			
					//SALVANDO FUENTE DE INGRESOS
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando Fuente de Ingresos \n");
					Thread.sleep(3000);
					
			
					//CORREO PRINCIPAL
					logger.info("Correo Principal: "+separador2[15]+ "\n");
					WebElement tbCorreoP = driver.findElement(By.id("_Email"));
					tbCorreoP.sendKeys(separador2[15]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					st.waitSearchWicketDisapear(driver, "bigLayer", 3);
			
					//SALVANDO CORREO PRINCIPAL
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando Correo Principal \n");
					Thread.sleep(4000);
			
					//OCUPACION
					logger.info("_Ocupacion: "+separador2[16]+ "\n");
					st.waitSelectPopulateid(driver, 10,"_Ocupacion");
					Select Ocup = new Select(driver.findElement(By.id("_Ocupacion")));
					Ocup.selectByVisibleText(separador2[16]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			
					//ESTADO CIVIL
					logger.info("_MaritalStatus: "+separador2[17]+ "\n");
					st.waitSelectPopulateid(driver, 10,"_MaritalStatus");
					Select ECivil = new Select(driver.findElement(By.id("_MaritalStatus")));
					ECivil.selectByVisibleText(separador2[17]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			
					//TELEFONO DE DOMICILIO
					logger.info("Telefono de Domicilio: "+separador2[18]+ "\n");
					WebElement tbTelef = driver.findElement(By.id("_TelefonoFijo"));
					tbTelef.sendKeys(separador2[18]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			
					//SALVANDO TELEFONO DOMICILIO
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando Telefono de Domicilio \n");
					Thread.sleep(3000);
			
					//DESCRIPCION DE LA OCUPACION
					logger.info("Descripcion de la ocupacion: "+separador2[19]+ "\n");
					WebElement tbDOcup = driver.findElement(By.id("_DescOcupacion"));
					tbDOcup.sendKeys(separador2[19]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			
					//SALVANDO DESCRIPCION DE LA OCUPACION
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando Descripcion de la Ocupacion \n");
					Thread.sleep(3000);
			
					//TELEFONO CELULAR
					logger.info("Descripcion de la ocupacions: "+separador2[20]+ "\n");
					WebElement tbTelfCel = driver.findElement(By.id("_TelefonoCelular"));
					tbTelfCel.sendKeys(separador2[20]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			
					//SALVANDO TELEFONO CELULAR
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando Telefono Celular \n");
					Thread.sleep(3000);
			
					//EMPRESA ASEGURADO
					logger.info("Empresa Asegurado: "+separador2[21]+ "\n");
					WebElement tbEmpresaAseg = driver.findElement(By.id("_NombreEmpresaAseg"));
					tbEmpresaAseg.sendKeys(separador2[21]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			
					//SALVANDO EMPRESA ASEGURADO
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando Empresa Asegurado \n");
					Thread.sleep(3000);
			
					//ACTIVIDAD ECONOMICA
					logger.info("_ActividadEconom: "+separador2[22]+ "\n");
					st.waitSelectPopulateid(driver, 10,"_ActividadEconom");
					Select ActEc = new Select(driver.findElement(By.id("_ActividadEconom")));
					st.waitSearchWicketDisapear(driver, "bigLayer", 5);
					ActEc.selectByVisibleText(separador2[22]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(3000);
			
					//GIRO
					logger.info("_Giro: "+separador2[23]+ "\n");
					st.waitSelectPopulateid(driver, 10,"_Giro");
					Select Giro = new Select(driver.findElement(By.id("_Giro")));
					Giro.selectByVisibleText(separador2[23]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(3000);
			
					//CLIENTE PEPS
					logger.info("_ReqClientePEPs: "+separador2[24]+ "\n");
					st.waitSelectPopulateid(driver, 10,"_ReqClientePEPs");
					Select CPeps = new Select(driver.findElement(By.id("_ReqClientePEPs")));
					CPeps.selectByVisibleText(separador2[24]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(4000);
			
					//CLIENTE FATCA
					logger.info("_ClienteFATCA: "+separador2[25]+ "\n");
					st.waitSelectPopulateid(driver, 10,"_ClienteFATCA");
					Select CFatca = new Select(driver.findElement(By.id("_ClienteFATCA")));
					CFatca.selectByVisibleText(separador2[25]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(4000);
			
					//FIEL
					logger.info("FIEL: "+separador2[26]+ "\n");
					st.waitSelectPopulateid(driver, 10,"_FIEL");
					Select Fiel = new Select(driver.findElement(By.id("_FIEL")));
					Fiel.selectByVisibleText(separador2[26]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(4000);
			
					//NUMERO DE IDENTIFICACION
					logger.info("Numero de Identificacion: "+separador2[27]+ "\n");
					WebElement tbNumID = driver.findElement(By.id("_NroDoc"));
					tbNumID.sendKeys(separador2[27]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			
					//SALVANDO NUMERO IDENTIFICACION
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando Numero de Identificacion \n");
					Thread.sleep(4000);
			
					//REPRESENTANTE LEGAL
					logger.info("Representante Legal: "+separador2[28]+ "\n");
					WebElement tbRepLegal = driver.findElement(By.id("_RepresentanteLegal"));
					tbRepLegal.sendKeys(separador2[28]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			
					//SALVANDO REPRESENTANTE LEGAL
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando Representante Legal \n");
					Thread.sleep(4000);
			
					//CURP
					logger.info("CURP: "+separador2[29]+ "\n");
					WebElement tbCURP = driver.findElement(By.id("_CURP"));
					tbCURP.sendKeys(separador2[29]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			
					//SALVANDO CURP
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando CURP \n");
					Thread.sleep(4000);
			
					//RFC
					logger.info("RFC: "+separador2[30]+ "\n");
					WebElement tbRFC = driver.findElement(By.id("_RFC"));
					tbRFC.sendKeys(separador2[30]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			
					//SALVANDO RFC
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando RFC \n");
					Thread.sleep(4000);
			
					//TIPO DE RELACION
					logger.info("Tipo de Relacion: "+separador2[31]+ "\n");
					Thread.sleep(1000);
					st.waitSelectPopulateid(driver, 10,"_TipoRelacionPFAE");
					Select TRelacion = new Select(driver.findElement(By.id("_TipoRelacionPFAE")));
					TRelacion.selectByVisibleText(separador2[31]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			
					//**GUARDAR INFORMACION DE TERCERO
					driver.findElement(By.id("ThirdInformation_searchButton")).click();
					logger.info("Guardar Tercero \n");
					Thread.sleep(6000);
			
					//AGREGAR DIRECCION
					Thread.sleep(6000);
					st.waitSearchWicketDisapear(driver, "bigLayer", 5);
					driver.findElement(By.id("panelAddress_addButton")).click();
					logger.info("Agregar Direccion \n");
					Thread.sleep(4000);
			
					//ROLNP
					logger.info("ROLNP: "+separador2[32]+ "\n");
					st.waitSelectPopulateid(driver, 10,"_ROLNP");
					Select ROLNP = new Select(driver.findElement(By.id("_ROLNP")));
					ROLNP.selectByVisibleText(separador2[32]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(4000);
			
					//TIPO DE DIRECCION
					logger.info("Tipo de Domicilio: "+separador2[33]+ "\n");
					st.waitSelectPopulateid(driver, 5,"_TipoDomicilio");
					Select TipDom = new Select(driver.findElement(By.id("_TipoDomicilio")));
					TipDom.selectByVisibleText(separador2[33]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(4000);
			
					//PAIS
					logger.info("PAIS: "+separador2[34]+ "\n");
					st.waitSelectPopulateid(driver, 10,"_Pais");
					Select PaisD = new Select(driver.findElement(By.id("_Pais")));
					PaisD.selectByVisibleText(separador2[34]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(4000);
			
					//CODIGO POSTAL
					logger.info("Codigo Postal: "+separador2[35]+ "\n");
					WebElement tbCPos = driver.findElement(By.id("_CodPostal"));
					tbCPos.sendKeys(separador2[35]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(4000);
			
					//SALVANDO CODIGO POSTAL
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando Codigo Postal \n");
					Thread.sleep(4000);
			
					//COLONIA
					logger.info("Colonia: "+separador2[36]+ "\n");
					WebElement tbColonia = driver.findElement(By.id("_Colonia"));
					tbColonia.sendKeys(separador2[36]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(2000);
			
					//SALVANDO COLONIA
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando Colonia \n");
					Thread.sleep(4000);
			
					//CALLE
					logger.info("Calle: "+separador2[37]+ "\n");
					WebElement tbCalle= driver.findElement(By.id("_CalleNroExtNroInt"));
					tbCalle.sendKeys(separador2[37]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(4000);
			
					//SALVANDO CALLE
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando Calle \n");
					Thread.sleep(2000);
			
					//**GUARDAR DIRECCION
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					driver.findElements(By.name("saveButton")).get(1).click();
					//driver.findElement(By.xpath("//*[@id=\"id81d\"]")).click();
					logger.info("Guardar Tercero \n");
					Thread.sleep(4000);
			
			
					//**AGREGAR ROL			
					//ROL
					Thread.sleep(2000);
					logger.info("Tipo de Rol: "+separador2[38]+ "\n");
					st.waitSelectPopulateid(driver, 10,"panelRol_selectRol");
					Select TRol = new Select(driver.findElement(By.id("panelRol_selectRol")));
					TRol.selectByVisibleText(separador2[38]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(4000);
			
					//AGREGAR ROL
					driver.findElement(By.id("panelRol_addButton")).click();
					logger.info("Agregar Rol \n");
					Thread.sleep(4000);
			
					//SEGMENTO
					Thread.sleep(2000);
					logger.info("Segmento: "+separador2[39]+ "\n");
					st.waitSelectPopulateid(driver, 10,"Contratante_Segmento");
					Select Segment = new Select(driver.findElement(By.id("Contratante_Segmento")));
					Segment.selectByVisibleText(separador2[39]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(4000);
			
					//PAIS DE RESIDENCIA FISCAL
					logger.info("Pais de residencia Fiscal: "+separador2[40]+ "\n");
					st.waitSelectPopulateid(driver, 10,"Contratante_PaisResFiscal");
					Select PResfis = new Select(driver.findElement(By.id("Contratante_PaisResFiscal")));
					PResfis.selectByVisibleText(separador2[40]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(4000);
			
					//RAZON DE NO ENTREGA FISCAL ID
					Thread.sleep(1000);
					logger.info("Razon de no entrega fiscal ID: "+separador2[41]+ "\n");
					WebElement tbRNEFID= driver.findElement(By.id("Contratante_RazonNoIDFiscal"));
					tbRNEFID.sendKeys(separador2[41]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(3000);
			
					//SALVANDO RAZON DE NO ENTREGA FISCAL ID
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando Raazon de no entrega Fiscal \n");
					Thread.sleep(4000);
			
					//TIPO DE CUENTA
					logger.info("Tipo de Cuenta: "+separador2[42]+ "\n");
					st.waitSelectPopulateid(driver, 10,"Contratante_TipoCuenta");
					st.waitSearchWicketDisapear(driver, "bigLayer", 10);
					Select TCuenta = new Select(driver.findElement(By.id("Contratante_TipoCuenta")));
					TCuenta.selectByVisibleText(separador2[42]);
					Thread.sleep(4000);
			
					//TITULARIDAD DE CUENTA
					logger.info("Titularidad de Cuenta: "+separador2[43]+ "\n");
					st.waitSelectPopulateid(driver, 10,"Contratante_TitularCuenta");
					st.waitSearchWicketDisapear(driver, "bigLayer", 10);
					Select TitCuenta = new Select(driver.findElement(By.id("Contratante_TitularCuenta")));
					TitCuenta.selectByVisibleText(separador2[43]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(4000);
			
					//NOMBRE DEL BANCO
					logger.info("Nombre del Banco: "+separador2[44]+ "\n");
					st.waitSelectPopulateid(driver, 10,"Contratante_NomBanco");
					Select NomBanco = new Select(driver.findElement(By.id("Contratante_NomBanco")));
					NomBanco.selectByVisibleText(separador2[44]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(4000);
			
					//NOMBRE DEL TITULAR DE LA CUENTA
					logger.info("Nombre del Titular de la Cuenta: "+separador2[45]+ "\n");
					WebElement tbNomTCuenta= driver.findElement(By.id("Contratante_NomCuentaBancaria"));
					tbNomTCuenta.sendKeys(separador2[45]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(4000);
			
					//SALVANDO NOMBRE DEL TITULAR DE LA CUENTA
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando Titular de la Cuenta \n");
					Thread.sleep(3000);
			
					//NUMERO DE CUENTA
					logger.info("Numero de Cuenta: "+separador2[46]+ "\n");
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					WebElement tbNumCuenta= driver.findElement(By.id("Contratante_NumCuenta"));
					tbNumCuenta.sendKeys(separador2[46]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					logger.info("Numero de Cuenta enviado\n");

			
					//SALVANDO NUMERO DE CUENTA
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando Numero de cuenta bancaria \n");
					Thread.sleep(5000);
			
			
					//GUARDAR CONTRATANTE
					st.waitSearchWicketDisapear(driver, "bigLayer", 5);
					driver.findElements(By.name("saveButton")).get(1).click();
					logger.info("Salvando Contratante \n");
					Thread.sleep(4000);
			
			
					//**MEDIOS DE COBRO	
					Thread.sleep(3000);
					logger.info("Medios de cobro: "+separador2[47]+ "\n");
					st.waitSelectPopulateid(driver, 10,"PanelPayment_Mode_selectPayMode");
					Select MedCob = new Select(driver.findElement(By.id("PanelPayment_Mode_selectPayMode")));
					MedCob.selectByVisibleText(separador2[47]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(4000);
					
			
					//AGREGAR MEDIO DE COBRO
					driver.findElement(By.id("PanelPayment_Mode_addButton")).click();
					logger.info("Agregando Medio de Cobro \n");
					Thread.sleep(4000);
			
			
					//CLAVE SUBPRODUCTO
					logger.info("Clave Subproducto: "+separador2[48]+ "\n");
					WebElement tbClaveS= driver.findElement(By.id("_CveSubprod"));
					Thread.sleep(1000);
					tbClaveS.sendKeys(separador2[48]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(2000);
			
					//SALVANDO Clave de Subproducto
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando Clave de Subproducto \n");
					Thread.sleep(3000);
			
					//SUBPRODUCTO
					logger.info("Subproducto: "+separador2[49]+ "\n");
					WebElement tbSubProd= driver.findElement(By.id("_SubProducto"));
					Thread.sleep(1000);
					tbSubProd.sendKeys(separador2[49]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(2000);
			
					//SALVANDO SUBPRODUCTO
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando Subproducto \n");
					Thread.sleep(3000);
			
					//NOMBRE BANCO EMISOR
					Thread.sleep(1000);
					logger.info("Nombre Banco Emisor: "+separador2[50]+ "\n");
					st.waitSelectPopulateid(driver, 5,"_NomBancoEmisor");
					Select NomBancEmi = new Select(driver.findElement(By.id("_NomBancoEmisor")));
					NomBancEmi.selectByVisibleText(separador2[50]);
					st.waitSearchWicketDisapear(driver, "bigLayer", 5);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(3000);
			
					//NUMERO DE CUENTA
					logger.info("Numero de Cuenta: "+separador2[51]+ "\n");
					WebElement tbNumCuentaCob= driver.findElement(By.id("_NroDDA"));
					Thread.sleep(1000);
					tbNumCuentaCob.sendKeys(separador2[51]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(2000);
			
					//SALVANDO NUMERO DE CUENTA
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando Numero de Cuenta DDA \n");
					Thread.sleep(4000);
			
					//FECHA DE ALTA DE LA CUENTA
					logger.info("Alta de la Cuenta: "+separador2[52]+ "\n");
					WebElement tbFechAlta= driver.findElement(By.id("_FechaDeAlta"));
					tbFechAlta.sendKeys(separador2[52]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(2000);
			
					//SALVANDO FECHA DE ALTA DE LA CUENTA
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando Fecha de Alta de la Cuenta \n");
					Thread.sleep(3000);

					
					
					// ASOCIANDO COLLECTOR
					logger.info("Collector: "+separador2[53]+ "\n");
					WebElement colect= driver.findElement(By.xpath("/html/body/div[1]/div[3]/div/div[2]/div[6]/div/div[2]/div[2]/div[1]/div/form/div[3]/div[2]/div[3]/input[1]"));
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);//test
					colect.sendKeys(separador2[53]);
					Thread.sleep(3000);

					driver.findElement(By.xpath("/html/body/div[1]/div[3]/div/div[2]/div[6]/div/div[2]/div[2]/div[1]/div/form/div[3]/div[2]/div[3]/input[1]")).sendKeys(Keys.DOWN);
					Thread.sleep(500);
					driver.findElement(By.xpath("/html/body/div[1]/div[3]/div/div[2]/div[6]/div/div[2]/div[2]/div[1]/div/form/div[3]/div[2]/div[3]/input[1]")).sendKeys(Keys.DOWN);
					Thread.sleep(500);
					driver.findElement(By.xpath("/html/body/div[1]/div[3]/div/div[2]/div[6]/div/div[2]/div[2]/div[1]/div/form/div[3]/div[2]/div[3]/input[1]")).sendKeys(Keys.ENTER);
					logger.info("Asociar collector \n");
					
					//SALVANDO MODO DE PAGO
					driver.findElements(By.name("saveButton")).get(1).click();
					logger.info("Salvando Modo de Pago \n");
					Thread.sleep(4000);
					//FIN DE CREACION DE TERCERO FISICO
					
			}

				
				else{//CREACION DE TERCERO MORAL	
					
					
					//CLIENTE INTERNO HSCBC
					logger.info("Cliente interno: "+separador2[1]+ "\n");
					st.waitSelectPopulateid(driver, 5,"_ClienteHSBC");
					Select clientei = new Select(driver.findElement(By.id("_ClienteHSBC")));
					clientei.selectByVisibleText(separador2[1]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);

					
					//NOMBRE COMPLETO DEL CONTACTO
					logger.info("Nombre Completo del Contacto: "+separador2[2]+ "\n");
					WebElement tbNombreMoral = driver.findElement(By.id("_ContatcName"));
					tbNombreMoral.sendKeys(separador2[2]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					
					
					//SALVAR NOMBRE COMPLETO DEL CONTACTO
					driver.findElement(By.id("_PredeterminedName")).click();
					st.waitSearchWicketDisapear(driver, "bigLayer", 5);
					logger.info("Salvando Nombre completo del Contacto \n");
					
					
					//RAZON SOCIAL
					logger.info("Nombre Completo del Contacto: "+separador2[3]+ "\n");
					WebElement tbRazonSocial = driver.findElement(By.id("_LegalName"));
					tbRazonSocial.sendKeys(separador2[3]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					
					
					//SALVAR RAZON SOCIAL
					driver.findElement(By.id("_PredeterminedName")).click();
					st.waitSearchWicketDisapear(driver, "bigLayer", 5);
					logger.info("Salvando Razon Social \n");

					
					//TELEFONO DE LA EMPRESA
					logger.info("Nombre Completo del Contacto: "+separador2[4]+ "\n");
					WebElement tbTelefonoS = driver.findElement(By.id("_TelefonoEmpresa"));
					tbTelefonoS.sendKeys(separador2[4]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					
					
					//SALVAR TELEFONO DE LA EMPRESA
					driver.findElement(By.id("_PredeterminedName")).click();
					st.waitSearchWicketDisapear(driver, "bigLayer", 5);
					logger.info("Salvando Telefono de la Empresa \n");
					
					//SCREENSHOT
					st.captureScreen(driver, rutaDirectorio+"//Screenshots","CrearPersona");
					
					
					/*				
					//PAIS DE ORIGEN
					logger.info("Pais de origen: "+separador2[3]+ "\n");
					st.waitSelectPopulateid(driver, 5,"_PaisOrigenTer");
					Select paiso = new Select(driver.findElement(By.id("_PaisOrigenTer")));
					Thread.sleep(1000);
					paiso.selectByVisibleText(separador2[3]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(3000);
					
					//RAZON SOCIAL
					logger.info("Razon social: "+separador2[4]+ "\n");
					st.waitSelectPopulateid(driver, 5,"_LegalName");
					Select lugr = new Select(driver.findElement(By.id("_LegalName")));
					lugr.selectByVisibleText(separador2[4]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(3000);
			

					//TELEFONO DE LA EMPRESA
					logger.info("Telefono de la Empresa: "+separador2[5]+ "\n");
					st.waitSelectPopulateid(driver, 5,"_TelefonoEmpresa");
					Select naci = new Select(driver.findElement(By.id("_TelefonoEmpresa")));
					naci.selectByVisibleText(separador2[5]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(3000);
			

					//RFC
					logger.info("RFC: "+separador2[6]+ "\n");
					WebElement tbNombreR = driver.findElement(By.id("_RFCMoral"));
					tbNombreR.sendKeys(separador2[6]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					
					
					//SALVAR RFC
					driver.findElement(By.id("_PredeterminedName")).click();
					st.waitSearchWicketDisapear(driver, "bigLayer", 5);
					logger.info("Salvando RFC \n");
					Thread.sleep(3000);
			
					
					//APELLIDO
					logger.info("Apellido: "+separador2[7]+ "\n");
					WebElement tbApellido = driver.findElement(By.id("_LastName"));
					tbApellido.sendKeys(separador2[7]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					st.waitSearchWicketDisapear(driver, "bigLayer", 5);
					
			
					//SALVAR APELLIDO
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando nombre \n");
					Thread.sleep(3000);
				
					//FECHA NACIMIENTO
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					String FechaNac = separador2[8];// Fecha Nacimiento
					logger.info("Fecha Nacimiento " + FechaNac + "\n");
					driver.findElement(By.id("_BirthDate")).sendKeys(FechaNac);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					
			
					//TIPO DE DOCUMENTO DE IDENTIFICACION
					logger.info("_TipoDocIdentificacion: "+separador2[5]+ "\n");
					st.waitSelectPopulateid(driver, 5,"_TipoDocIdentificacion");
					Select tdi = new Select(driver.findElement(By.id("_TipoDocIdentificacion")));
					tdi.selectByVisibleText(separador2[9]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(3000);
			
					//GENERO
					logger.info("_Sex: "+separador2[10]+ "\n");
					st.waitSelectPopulateid(driver, 5,"_Sex");
					Select sex = new Select(driver.findElement(By.id("_Sex")));
					Thread.sleep(1000);
					sex.selectByVisibleText(separador2[10]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(3000);
					

					//INGRESO ANUAL
					logger.info("Ingreso Anual: "+separador2[11]+ "\n");
					WebElement tbIngA = driver.findElement(By.id("_TO_IngresoAnual"));
					tbIngA.sendKeys(separador2[11]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					
			
					//SALVANDO INGRESO ANUAL
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando Ingreso Anual \n");
					Thread.sleep(3000);
					
			
					//INGRESO MENSUAL
					logger.info("Ingreso Mensual: "+separador2[12]+ "\n");
					WebElement tbIngM = driver.findElement(By.id("_TO_IngresoMensualDPS"));
					tbIngM.sendKeys(separador2[12]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			
					//SALVANDO INGRESO MENSUAL
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando Ingreso Mensual \n");
					Thread.sleep(3000);
			
					//OTROS INGRESOS
					logger.info("Otros Ingresos: "+separador2[13]+ "\n");
					WebElement tbIngO = driver.findElement(By.id("_TO_OtrosIngresos"));
					tbIngO.sendKeys(separador2[13]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			
					//SALVANDO OTROS INGRESOS
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando Otros Ingresos \n");
					Thread.sleep(3000);
			
					//FUENTE DE INGRESOS
					logger.info("Fuente de Ingresos: "+separador2[14]+ "\n");
					WebElement tbFuenteIng = driver.findElement(By.id("_EspecifiqueFuente"));
					tbFuenteIng.sendKeys(separador2[14]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			
					//SALVANDO FUENTE DE INGRESOS
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando Fuente de Ingresos \n");
					Thread.sleep(3000);
					
			
					//CORREO PRINCIPAL
					logger.info("Correo Principal: "+separador2[15]+ "\n");
					WebElement tbCorreoP = driver.findElement(By.id("_Email"));
					tbCorreoP.sendKeys(separador2[15]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					st.waitSearchWicketDisapear(driver, "bigLayer", 3);
			
					//SALVANDO CORREO PRINCIPAL
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando Correo Principal \n");
					Thread.sleep(4000);
			
					//OCUPACION
					logger.info("_Ocupacion: "+separador2[16]+ "\n");
					st.waitSelectPopulateid(driver, 10,"_Ocupacion");
					Select Ocup = new Select(driver.findElement(By.id("_Ocupacion")));
					Ocup.selectByVisibleText(separador2[16]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			
					//ESTADO CIVIL
					logger.info("_MaritalStatus: "+separador2[17]+ "\n");
					st.waitSelectPopulateid(driver, 10,"_MaritalStatus");
					Select ECivil = new Select(driver.findElement(By.id("_MaritalStatus")));
					ECivil.selectByVisibleText(separador2[17]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			
					//TELEFONO DE DOMICILIO
					logger.info("Telefono de Domicilio: "+separador2[18]+ "\n");
					WebElement tbTelef = driver.findElement(By.id("_TelefonoFijo"));
					tbTelef.sendKeys(separador2[18]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			
					//SALVANDO TELEFONO DOMICILIO
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando Telefono de Domicilio \n");
					Thread.sleep(3000);
			
					//DESCRIPCION DE LA OCUPACION
					logger.info("Descripcion de la ocupacion: "+separador2[19]+ "\n");
					WebElement tbDOcup = driver.findElement(By.id("_DescOcupacion"));
					tbDOcup.sendKeys(separador2[19]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			
					//SALVANDO DESCRIPCION DE LA OCUPACION
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando Descripcion de la Ocupacion \n");
					Thread.sleep(3000);
			
					//TELEFONO CELULAR
					logger.info("Descripcion de la ocupacions: "+separador2[20]+ "\n");
					WebElement tbTelfCel = driver.findElement(By.id("_TelefonoCelular"));
					tbTelfCel.sendKeys(separador2[20]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			
					//SALVANDO TELEFONO CELULAR
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando Telefono Celular \n");
					Thread.sleep(3000);
			
					//EMPRESA ASEGURADO
					logger.info("Empresa Asegurado: "+separador2[21]+ "\n");
					WebElement tbEmpresaAseg = driver.findElement(By.id("_NombreEmpresaAseg"));
					tbEmpresaAseg.sendKeys(separador2[21]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			
					//SALVANDO EMPRESA ASEGURADO
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando Empresa Asegurado \n");
					Thread.sleep(3000);
			
					//ACTIVIDAD ECONOMICA
					logger.info("_ActividadEconom: "+separador2[22]+ "\n");
					st.waitSelectPopulateid(driver, 10,"_ActividadEconom");
					Select ActEc = new Select(driver.findElement(By.id("_ActividadEconom")));
					st.waitSearchWicketDisapear(driver, "bigLayer", 5);
					ActEc.selectByVisibleText(separador2[22]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(3000);
			
					//GIRO
					logger.info("_Giro: "+separador2[23]+ "\n");
					st.waitSelectPopulateid(driver, 10,"_Giro");
					Select Giro = new Select(driver.findElement(By.id("_Giro")));
					Giro.selectByVisibleText(separador2[23]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(3000);
			
					//CLIENTE PEPS
					logger.info("_ReqClientePEPs: "+separador2[24]+ "\n");
					st.waitSelectPopulateid(driver, 10,"_ReqClientePEPs");
					Select CPeps = new Select(driver.findElement(By.id("_ReqClientePEPs")));
					CPeps.selectByVisibleText(separador2[24]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(4000);
			
					//CLIENTE FATCA
					logger.info("_ClienteFATCA: "+separador2[25]+ "\n");
					st.waitSelectPopulateid(driver, 10,"_ClienteFATCA");
					Select CFatca = new Select(driver.findElement(By.id("_ClienteFATCA")));
					CFatca.selectByVisibleText(separador2[25]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(4000);
			
					//FIEL
					logger.info("FIEL: "+separador2[26]+ "\n");
					st.waitSelectPopulateid(driver, 10,"_FIEL");
					Select Fiel = new Select(driver.findElement(By.id("_FIEL")));
					Fiel.selectByVisibleText(separador2[26]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(4000);
			
					//NUMERO DE IDENTIFICACION
					logger.info("Numero de Identificacion: "+separador2[27]+ "\n");
					WebElement tbNumID = driver.findElement(By.id("_NroDoc"));
					tbNumID.sendKeys(separador2[27]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			
					//SALVANDO NUMERO IDENTIFICACION
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando Numero de Identificacion \n");
					Thread.sleep(4000);
			
					//REPRESENTANTE LEGAL
					logger.info("Representante Legal: "+separador2[28]+ "\n");
					WebElement tbRepLegal = driver.findElement(By.id("_RepresentanteLegal"));
					tbRepLegal.sendKeys(separador2[28]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			
					//SALVANDO REPRESENTANTE LEGAL
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando Representante Legal \n");
					Thread.sleep(4000);
			
					//CURP
					logger.info("CURP: "+separador2[29]+ "\n");
					WebElement tbCURP = driver.findElement(By.id("_CURP"));
					tbCURP.sendKeys(separador2[29]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			
					//SALVANDO CURP
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando CURP \n");
					Thread.sleep(4000);
			
					//RFC
					logger.info("RFC: "+separador2[30]+ "\n");
					WebElement tbRFC = driver.findElement(By.id("_RFC"));
					tbRFC.sendKeys(separador2[30]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			
					//SALVANDO RFC
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando RFC \n");
					Thread.sleep(4000);
			
					//TIPO DE RELACION
					logger.info("Tipo de Relacion: "+separador2[31]+ "\n");
					Thread.sleep(1000);
					st.waitSelectPopulateid(driver, 10,"_TipoRelacionPFAE");
					Select TRelacion = new Select(driver.findElement(By.id("_TipoRelacionPFAE")));
					TRelacion.selectByVisibleText(separador2[31]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			
					//**GUARDAR INFORMACION DE TERCERO
					driver.findElement(By.id("ThirdInformation_searchButton")).click();
					logger.info("Guardar Tercero \n");
					Thread.sleep(6000);
			
					//AGREGAR DIRECCION
					Thread.sleep(6000);
					st.waitSearchWicketDisapear(driver, "bigLayer", 5);
					driver.findElement(By.id("panelAddress_addButton")).click();
					logger.info("Agregar Direccion \n");
					Thread.sleep(4000);
			
					//ROLNP
					logger.info("ROLNP: "+separador2[32]+ "\n");
					st.waitSelectPopulateid(driver, 10,"_ROLNP");
					Select ROLNP = new Select(driver.findElement(By.id("_ROLNP")));
					ROLNP.selectByVisibleText(separador2[32]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(4000);
			
					//TIPO DE DIRECCION
					logger.info("Tipo de Domicilio: "+separador2[33]+ "\n");
					st.waitSelectPopulateid(driver, 5,"_TipoDomicilio");
					Select TipDom = new Select(driver.findElement(By.id("_TipoDomicilio")));
					TipDom.selectByVisibleText(separador2[33]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(4000);
			
					//PAIS
					logger.info("PAIS: "+separador2[34]+ "\n");
					st.waitSelectPopulateid(driver, 10,"_Pais");
					Select PaisD = new Select(driver.findElement(By.id("_Pais")));
					PaisD.selectByVisibleText(separador2[34]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(4000);
			
					//CODIGO POSTAL
					logger.info("Codigo Postal: "+separador2[35]+ "\n");
					WebElement tbCPos = driver.findElement(By.id("_CodPostal"));
					tbCPos.sendKeys(separador2[35]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(4000);
			
					//SALVANDO CODIGO POSTAL
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando Codigo Postal \n");
					Thread.sleep(4000);
			
					//COLONIA
					logger.info("Colonia: "+separador2[36]+ "\n");
					WebElement tbColonia = driver.findElement(By.id("_Colonia"));
					tbColonia.sendKeys(separador2[36]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(2000);
			
					//SALVANDO COLONIA
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando Colonia \n");
					Thread.sleep(4000);
			
					//CALLE
					logger.info("Calle: "+separador2[37]+ "\n");
					WebElement tbCalle= driver.findElement(By.id("_CalleNroExtNroInt"));
					tbCalle.sendKeys(separador2[37]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(4000);
			
					//SALVANDO CALLE
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando Calle \n");
					Thread.sleep(2000);
			
					//**GUARDAR DIRECCION
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					driver.findElements(By.name("saveButton")).get(1).click();
					//driver.findElement(By.xpath("//*[@id=\"id81d\"]")).click();
					logger.info("Guardar Tercero \n");
					Thread.sleep(4000);
			
			
					//**AGREGAR ROL			
					//ROL
					Thread.sleep(2000);
					logger.info("Tipo de Rol: "+separador2[38]+ "\n");
					st.waitSelectPopulateid(driver, 10,"panelRol_selectRol");
					Select TRol = new Select(driver.findElement(By.id("panelRol_selectRol")));
					TRol.selectByVisibleText(separador2[38]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(4000);
			
					//AGREGAR ROL
					driver.findElement(By.id("panelRol_addButton")).click();
					logger.info("Agregar Rol \n");
					Thread.sleep(4000);
			
					//SEGMENTO
					Thread.sleep(2000);
					logger.info("Segmento: "+separador2[39]+ "\n");
					st.waitSelectPopulateid(driver, 10,"Contratante_Segmento");
					Select Segment = new Select(driver.findElement(By.id("Contratante_Segmento")));
					Segment.selectByVisibleText(separador2[39]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(4000);
			
					//PAIS DE RESIDENCIA FISCAL
					logger.info("Pais de residencia Fiscal: "+separador2[40]+ "\n");
					st.waitSelectPopulateid(driver, 10,"Contratante_PaisResFiscal");
					Select PResfis = new Select(driver.findElement(By.id("Contratante_PaisResFiscal")));
					PResfis.selectByVisibleText(separador2[40]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(4000);
			
					//RAZON DE NO ENTREGA FISCAL ID
					Thread.sleep(1000);
					logger.info("Razon de no entrega fiscal ID: "+separador2[41]+ "\n");
					WebElement tbRNEFID= driver.findElement(By.id("Contratante_RazonNoIDFiscal"));
					tbRNEFID.sendKeys(separador2[41]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(3000);
			
					//SALVANDO RAZON DE NO ENTREGA FISCAL ID
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando Raazon de no entrega Fiscal \n");
					Thread.sleep(4000);
			
					//TIPO DE CUENTA
					logger.info("Tipo de Cuenta: "+separador2[42]+ "\n");
					st.waitSelectPopulateid(driver, 10,"Contratante_TipoCuenta");
					st.waitSearchWicketDisapear(driver, "bigLayer", 10);
					Select TCuenta = new Select(driver.findElement(By.id("Contratante_TipoCuenta")));
					TCuenta.selectByVisibleText(separador2[42]);
					Thread.sleep(4000);
			
					//TITULARIDAD DE CUENTA
					logger.info("Titularidad de Cuenta: "+separador2[43]+ "\n");
					st.waitSelectPopulateid(driver, 10,"Contratante_TitularCuenta");
					st.waitSearchWicketDisapear(driver, "bigLayer", 10);
					Select TitCuenta = new Select(driver.findElement(By.id("Contratante_TitularCuenta")));
					TitCuenta.selectByVisibleText(separador2[43]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(4000);
			
					//NOMBRE DEL BANCO
					logger.info("Nombre del Banco: "+separador2[44]+ "\n");
					st.waitSelectPopulateid(driver, 10,"Contratante_NomBanco");
					Select NomBanco = new Select(driver.findElement(By.id("Contratante_NomBanco")));
					NomBanco.selectByVisibleText(separador2[44]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(4000);
			
					//NOMBRE DEL TITULAR DE LA CUENTA
					logger.info("Nombre del Titular de la Cuenta: "+separador2[45]+ "\n");
					WebElement tbNomTCuenta= driver.findElement(By.id("Contratante_NomCuentaBancaria"));
					tbNomTCuenta.sendKeys(separador2[45]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(4000);
			
					//SALVANDO NOMBRE DEL TITULAR DE LA CUENTA
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando Titular de la Cuenta \n");
					Thread.sleep(3000);
			
					//NUMERO DE CUENTA
					logger.info("Numero de Cuenta: "+separador2[46]+ "\n");
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					WebElement tbNumCuenta= driver.findElement(By.id("Contratante_NumCuenta"));
					tbNumCuenta.sendKeys(separador2[46]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					logger.info("Numero de Cuenta enviado\n");

			
					//SALVANDO NUMERO DE CUENTA
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando Numero de cuenta bancaria \n");
					Thread.sleep(5000);
			
			
					//GUARDAR CONTRATANTE
					st.waitSearchWicketDisapear(driver, "bigLayer", 5);
					driver.findElements(By.name("saveButton")).get(1).click();
					logger.info("Salvando Contratante \n");
					Thread.sleep(4000);
			
			
					//**MEDIOS DE COBRO	
					Thread.sleep(3000);
					logger.info("Medios de cobro: "+separador2[47]+ "\n");
					st.waitSelectPopulateid(driver, 10,"PanelPayment_Mode_selectPayMode");
					Select MedCob = new Select(driver.findElement(By.id("PanelPayment_Mode_selectPayMode")));
					MedCob.selectByVisibleText(separador2[47]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(4000);
					
			
					//AGREGAR MEDIO DE COBRO
					driver.findElement(By.id("PanelPayment_Mode_addButton")).click();
					logger.info("Agregando Medio de Cobro \n");
					Thread.sleep(4000);
			
			
					//CLAVE SUBPRODUCTO
					logger.info("Clave Subproducto: "+separador2[48]+ "\n");
					WebElement tbClaveS= driver.findElement(By.id("_CveSubprod"));
					Thread.sleep(1000);
					tbClaveS.sendKeys(separador2[48]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(2000);
			
					//SALVANDO Clave de Subproducto
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando Clave de Subproducto \n");
					Thread.sleep(3000);
			
					//SUBPRODUCTO
					logger.info("Subproducto: "+separador2[49]+ "\n");
					WebElement tbSubProd= driver.findElement(By.id("_SubProducto"));
					Thread.sleep(1000);
					tbSubProd.sendKeys(separador2[49]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(2000);
			
					//SALVANDO SUBPRODUCTO
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando Subproducto \n");
					Thread.sleep(3000);
			
					//NOMBRE BANCO EMISOR
					Thread.sleep(1000);
					logger.info("Nombre Banco Emisor: "+separador2[50]+ "\n");
					st.waitSelectPopulateid(driver, 5,"_NomBancoEmisor");
					Select NomBancEmi = new Select(driver.findElement(By.id("_NomBancoEmisor")));
					NomBancEmi.selectByVisibleText(separador2[50]);
					st.waitSearchWicketDisapear(driver, "bigLayer", 5);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(3000);
			
					//NUMERO DE CUENTA
					logger.info("Numero de Cuenta: "+separador2[51]+ "\n");
					WebElement tbNumCuentaCob= driver.findElement(By.id("_NroDDA"));
					Thread.sleep(1000);
					tbNumCuentaCob.sendKeys(separador2[51]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(2000);
			
					//SALVANDO NUMERO DE CUENTA
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando Numero de Cuenta DDA \n");
					Thread.sleep(4000);
			
					//FECHA DE ALTA DE LA CUENTA
					logger.info("Alta de la Cuenta: "+separador2[52]+ "\n");
					WebElement tbFechAlta= driver.findElement(By.id("_FechaDeAlta"));
					tbFechAlta.sendKeys(separador2[52]);
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
					Thread.sleep(2000);
			
					//SALVANDO FECHA DE ALTA DE LA CUENTA
					driver.findElement(By.id("_PredeterminedName")).click();
					logger.info("Salvando Fecha de Alta de la Cuenta \n");
					Thread.sleep(3000);


					// ASOCIANDO COLLECTOR
					logger.info("Collector: "+separador2[53]+ "\n");
					WebElement colect= driver.findElement(By.xpath("/html/body/div[1]/div[3]/div/div[2]/div[6]/div/div[2]/div[2]/div[1]/div/form/div[3]/div[2]/div[3]/input[1]"));
					st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);//test
					colect.sendKeys(separador2[53]);
					Thread.sleep(3000);

					driver.findElement(By.xpath("/html/body/div[1]/div[3]/div/div[2]/div[6]/div/div[2]/div[2]/div[1]/div/form/div[3]/div[2]/div[3]/input[1]")).sendKeys(Keys.DOWN);
					Thread.sleep(500);
					driver.findElement(By.xpath("/html/body/div[1]/div[3]/div/div[2]/div[6]/div/div[2]/div[2]/div[1]/div/form/div[3]/div[2]/div[3]/input[1]")).sendKeys(Keys.DOWN);
					Thread.sleep(500);
					driver.findElement(By.xpath("/html/body/div[1]/div[3]/div/div[2]/div[6]/div/div[2]/div[2]/div[1]/div/form/div[3]/div[2]/div[3]/input[1]")).sendKeys(Keys.ENTER);
					logger.info("Asociar collector \n");
					
					//SALVANDO MODO DE PAGO
					driver.findElements(By.name("saveButton")).get(1).click();
					logger.info("Salvando Modo de Pago \n");
					Thread.sleep(4000);
					//FIN DE CREACION DE TERCERO FISICO
					 * 
					 * 
					 */	
				
			}
				

			//FIN DE CREACION DE TERCERO JURIDICO
			
		
				jt.escribirTxt(rutaestadoEjecucion, "CREAR PERSONA OK");
		
		} catch (Exception e) {
			st.captureScreen(driver, rutaDirectorio+"//Screenshots","CrearPersonaFAIL");
			jt.escribirTxt(rutaestadoEjecucion, "CREAR PERSONA NOK:"+e.getMessage());
			driver.quit();
			e.printStackTrace();
		}
		
		jt.escribirTxt(ruta,"Personas Creadas satisfactoriamente \n");
		logger.info("tiempo de fin de test " + java.time.LocalDateTime.now());
		logger.info("Finalizando creacion de personas");
		//CAMBIAR A VENTANA PPAL
		st.cambiarVentanaPPAL(driver);
		driver.close();
		return;
		
	}
	

	

}
