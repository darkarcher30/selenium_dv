package thread;
import java.io.BufferedReader;
import java.io.File;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import test.ProcesosRecurrentes;
import utils.BrowserFactory;
import utils.DriverFactory;
import utils.JavaTools;
import utils.SeleniumTools;

public class ThreadJobs extends Thread {
	
	JavaTools jt = new JavaTools();
	SeleniumTools st = new SeleniumTools();
	ProcesosRecurrentes procesosRecurrentes = new ProcesosRecurrentes();
	public WebDriver driver = null;
	String linea;
	
	Logger logger=Logger.getLogger("testJobs");
	String rutaDirectorio=System.getProperty("user.dir");
	
	BufferedReader bufer = jt.abrirLectura(rutaDirectorio,"ParametrosAmbiente.txt");
	String lineaParametrosAmbiente = jt.lecturaLinea(bufer);
	String separador1[] = lineaParametrosAmbiente.split("=");
	String PaginaWeb = separador1[3];// Direccion http de la prueba
	int tiempoDelay = Integer.parseInt(separador1[0]);// tiempo de delay de automatizacion
	int tiempoDivWait = Integer.parseInt(separador1[1]);// tiempo wait Big Layer
	int tiempoThrowWait = Integer.parseInt(separador1[2]);// tiempo de los wait
	
	
	public ThreadJobs(String linea) {
		super();
		this.linea = linea;
		
		driver = BrowserFactory.CreateInstance("firefox");
		DriverFactory.getInstance().setDriver(driver);
		driver= DriverFactory.getInstance().getDriver();
		
	}


	public void run () {
		
		//ABRIENDO LOG DE ESTADO EJECUCION
		String rutaestadoEjecucion = rutaDirectorio + "/estadoEjecucion.txt";
		
		//Crear directorio Reports si no existe
		File directory = new File(String.valueOf("Reports"));

		 if(!directory.exists()){

		             directory.mkdir();
		 }
		
		JavaTools jt = new JavaTools();
		
	
		ProcesosRecurrentes procesosRecurrentes = new ProcesosRecurrentes();
		
		Logger logger = Logger.getLogger("testJobs");
		PropertyConfigurator.configure("Log4j.properties");

		String SecurityLogin = separador1[4];// login
		String SecurityPassword = separador1[5];// password
		String USER_PREFERENCE_COUNTRY_NAME = separador1[6];// USER_PREFERENCE_COUNTRY_NAME
		String PARAMETER_LOCALE_NAME = separador1[7];// PARAMETER_LOCALE_NAME

		logger.info("SecurityLogin: " + SecurityLogin + " \n");
		logger.info("SecurityPassword: " + SecurityPassword + " \n");
		logger.info("USER_PREFERENCE_COUNTRY_NAME: " + USER_PREFERENCE_COUNTRY_NAME + " \n");
		logger.info("PARAMETER_LOCALE_NAME: " + PARAMETER_LOCALE_NAME + " \n");
		jt.cerrarLectura(bufer);

		logger.info("Parametros \n");
		logger.info("driverPath: " + rutaDirectorio + " \n");
		logger.info("PaginaWeb: " + PaginaWeb + " \n");
		logger.info("******************* \n");
		logger.info("Open Browser and navigate to designed URL \n");
			
		driver.get(PaginaWeb);
		
		logger.info("tiempo de inicio de test " + java.time.LocalDateTime.now());

		procesosRecurrentes.iniciarSesion(driver, "cotizacion", PaginaWeb, SecurityLogin, SecurityPassword,
				USER_PREFERENCE_COUNTRY_NAME, PARAMETER_LOCALE_NAME);

		procesosRecurrentes.validandoSesion(driver, PaginaWeb);
		
		
		///////////////  ENTRAR EN EL MENU EJECUCION DE TAREAS Menu4_9_2 /////////////////
		WebElement elemento = driver
				.findElement(By.id("Menu4_9_2"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click()", elemento);// metodo que abre menu de acsele de dos capas

		st.cambiarVentana(driver);
		
		try {

		
			String separador2[] = this.linea.split("=");
			Thread.sleep(4000);
			
			//ESCRIBIR NOMBRE DEL JOB EN /html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[3]/div/div[2]/div/div/input[2]
			logger.info("Nombre del job: "+separador2[0]+"\n");
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[3]/div/div[2]/div/div/input[2]")).sendKeys(separador2[0]);
			Thread.sleep(4000);
			
			//CLICK EN TEXTO Ejecución de tareas /html/body/div[1]/div/div[2]/div[2]/div/div[1]/div/span
			logger.info("click en texto Ejecución de tareas  \n");
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div[1]/div/span")).click();
			Thread.sleep(4000);
			
			//CLICK EN NOMBRE DEL JOB	/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[3]/div/div[3]/div[1]/table/tbody/tr/td[2]/div
			logger.info("click en nombre del JOB \n");
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[3]/div/div[3]/div[1]/table/tbody/tr/td[2]/div")).click();	
			Thread.sleep(4000);
			
			//CLICK EN EJECUTAR /html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[7]/div/div[9]/div/span
			logger.info("click en nombre ejecutar \n");
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[7]/div/div[9]/div/span")).click();
			Thread.sleep(4000);
			
			//CLICK EN ACCEPT /html/body/div[2]/div[3]/div/div/div[3]/div/div/div[2]/div/div[1]/div/span/span
			logger.info("click en btn Accept \n");
			driver.findElement(By.xpath("/html/body/div[2]/div[3]/div/div/div[3]/div/div/div[2]/div/div[1]/div/span/span")).click();
			Thread.sleep(4000);
			
			///////////////////VALIDACION DE LA EJECUCION DEL REPORTE //////////////////////
			
			st.cambiarVentanaPPAL(driver);

			logger.info("volviendo a menu ppal \n");
			
			Thread.sleep(tiempoThrowWait);
			
			///////////////  ENTRAR EN EL MENU Histórico de Procesos Menu4_9_4 /////////////////
			WebElement elemento2 = driver
					.findElement(By.id("Menu4_9_4"));
			JavascriptExecutor executor2 = (JavascriptExecutor) driver;
			executor2.executeScript("arguments[0].click()", elemento2);// metodo que abre menu de acsele de dos capas
			Thread.sleep(4000);
			
			st.cambiarVentana(driver);
			
			
			//REVISAR EL ULTIMO REPORTE EJECUTADO CON EXITO /html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[3]/div/div[3]/div[1]/table/tbody/tr[1]/td[2]/div	
			//OBTENIENDO NOMBRE DEL JOB  /html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[3]/div/div[3]/div[1]/table/tbody/tr[1]/td[2]/div
			logger.info("obteniendo nombre del job \n");
			String kk = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[3]/div/div[3]/div[1]/table/tbody/tr[1]/td[2]/div")).getAttribute("innerHTML");
			
			if(kk.equals(separador2[0])){
				logger.info("nombre del ultimo job ejecutado:"+kk+" coincide con el de archivo\n");	
			}else {
				logger.info("nombre del ultimo job ejecutado:"+kk+" NO coincide con el de archivo\n");	
			}
			
			Thread.sleep(4000);
			

			
			//OBTENIENDO STATUS DEL JOB  /html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[3]/div/div[3]/div[1]/table/tbody/tr[1]/td[6]/div
			logger.info("obteniendo status del job \n");
			String kk2 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[3]/div/div[3]/div[1]/table/tbody/tr[1]/td[6]/div")).getAttribute("innerHTML");
			logger.info("status del ultimo job ejecutado:"+kk2+"\n");
			Thread.sleep(4000);
			
			//SCREENSHOT
			st.captureScreen(driver, rutaDirectorio+"//Screenshots","JOBS");
			
			////////////////////////////////////////////////////////////////////////////////
			
			logger.info("tiempo de fin de test " + java.time.LocalDateTime.now());
			logger.info("Finalizando generacion de job");
		
			
			st.cambiarVentanaPPAL(driver);
			driver.close();
			jt.escribirTxt(rutaestadoEjecucion, "EJECUCION DE JOBS OK");
			} catch (Exception e) {
				st.captureScreen(driver, rutaDirectorio+"//Screenshots","JobsFAIL");
				jt.escribirTxt(rutaestadoEjecucion, "EJECUCION DE JOBS NOK: "+e.getMessage());
				driver.quit();
			e.printStackTrace();
			}	
	}
	
	

}
