package thread;

import java.io.BufferedReader;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import test.ProcesosRecurrentes;
import utils.BrowserFactory;
import utils.DriverFactory;
import utils.JavaTools;
import utils.SeleniumTools;

public class ThreadEmision extends Thread {

	JavaTools jt = new JavaTools();
	SeleniumTools st = new SeleniumTools();
	ProcesosRecurrentes procesosRecurrentes = new ProcesosRecurrentes();
	public WebDriver driver = null;
	String linea;

	Logger logger = Logger.getLogger("ThreadEmision");
	String rutaDirectorio = System.getProperty("user.dir");

	BufferedReader bufer = jt.abrirLectura(rutaDirectorio, "ParametrosAmbiente.txt");
	String lineaParametrosAmbiente = jt.lecturaLinea(bufer);
	String separador1[] = lineaParametrosAmbiente.split("=");
	String PaginaWeb = separador1[3];// Direccion http de la prueba
	int tiempoDelay = Integer.parseInt(separador1[0]);// tiempo de delay de automatizacion
	int tiempoDivWait = Integer.parseInt(separador1[1]);// tiempo wait Big Layer
	int tiempoThrowWait = Integer.parseInt(separador1[2]);// tiempo de los wait

	String ruta = rutaDirectorio + "/nrosCotizacionVidaInversion.txt";
	String rutaParametrosEmision = rutaDirectorio + "/ParametrosEmision.txt";

	public ThreadEmision(String linea) {
		super();
		this.linea = linea;

		driver = BrowserFactory.CreateInstance("firefox");
		DriverFactory.getInstance().setDriver(driver);
		driver = DriverFactory.getInstance().getDriver();

	}

	public void run() {

		// ABRIENDO LOG DE ESTADO EJECUCION
		String rutaestadoEjecucion = rutaDirectorio + "/estadoEjecucion.txt";

		try {
			JavaTools jt = new JavaTools();

			String rutaPolizasCreadas = rutaDirectorio + "/PolizasCreadas.txt";

			ProcesosRecurrentes procesosRecurrentes = new ProcesosRecurrentes();

			Logger logger = Logger.getLogger("ThreadEmision");
			PropertyConfigurator.configure("Log4j.properties");

			String SecurityLogin = separador1[4];// login
			String SecurityPassword = separador1[5];// password
			String USER_PREFERENCE_COUNTRY_NAME = separador1[6];// USER_PREFERENCE_COUNTRY_NAME
			String PARAMETER_LOCALE_NAME = separador1[7];// PARAMETER_LOCALE_NAME

			logger.info("SecurityLogin: " + SecurityLogin + " \n");
			logger.info("SecurityPassword: " + SecurityPassword + " \n");
			logger.info("USER_PREFERENCE_COUNTRY_NAME: " + USER_PREFERENCE_COUNTRY_NAME + " \n");
			logger.info("PARAMETER_LOCALE_NAME: " + PARAMETER_LOCALE_NAME + " \n");
			jt.cerrarLectura(bufer);

			logger.info("Parametros \n");
			logger.info("driverPath: " + rutaDirectorio + " \n");
			logger.info("PaginaWeb: " + PaginaWeb + " \n");
			logger.info("******************* \n");
			logger.info("Open Browser and navigate to designed URL \n");

			driver.get(PaginaWeb);

			logger.info("tiempo de inicio de test " + java.time.LocalDateTime.now());

			procesosRecurrentes.iniciarSesion(driver, "cotizacion", PaginaWeb, SecurityLogin, SecurityPassword,
					USER_PREFERENCE_COUNTRY_NAME, PARAMETER_LOCALE_NAME);

			procesosRecurrentes.validandoSesion(driver, PaginaWeb);

			/////// ENTRAR EN EL MENU
			WebElement elemento = driver.findElement(By.id("Menu2_2_4"));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click()", elemento);// metodo que abre menu de acsele de dos capas

			st.cambiarVentana(driver);

			// busqueda avanzada de cotizacion
			st.esperaXpath(driver, 10, "/html/body/div[1]/div[2]/div[3]/div/div/form/div[2]/div[4]/div[1]/a/span");
			driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[3]/div/div/form/div[2]/div[4]/div[1]/a/span"))
					.click();

			String NRO_COTIZACION = linea;// PARAMETER_LOCALE_NAME
			logger.info("NRO_COTIZACION " + linea + "\n");

			driver.findElement(By.name("templatePolicy:repeaterPanel1:7:fila:field")).sendKeys(NRO_COTIZACION);

			driver.findElement(By.name("searchButton")).click();

			logger.info("Buscando Cotizacion" + "\n");

			st.waitSearchWicket(driver);

			WebElement elemento2 = driver.findElement(By.xpath(
					"/html[1]/body[1]/div[1]/div[2]/div[3]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/div[1]/input[1]"));
			JavascriptExecutor executor2 = (JavascriptExecutor) driver;
			executor2.executeScript("arguments[0].click()", elemento2);// hacer click en radio btn de seleccionar poliza

			logger.info("Cotizacion encontrada" + "\n");

			driver.findElement(By.name("container:EmissionPolicyButton")).click();

			logger.info("emitiendo \n");

			logger.info("Click continuar cotizacion EventSection:content:Form:continueButton" + "\n");
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);

			driver.findElement(By.name("EventSection:content:Form:continueButton")).click();

			// CALCULAR EMISION
			String nro = null;
			try {
				nro = procesosRecurrentes.calcularSuscripcionEmision(driver, tiempoThrowWait, tiempoDivWait, logger);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			String nroSeparado = jt.separarNro(nro);
			logger.info("nro Poliza " + nroSeparado + "\n");
			jt.escribirTxt(rutaPolizasCreadas, nroSeparado);
			logger.info("Finalizando emisi�n y devolviendo control de webdriver \n");
			logger.info("tiempo de fin de test " + java.time.LocalDateTime.now());

			// SCREENSHOT
			st.captureScreen(driver, rutaDirectorio + "//Screenshots", "EmisionVidaEntera");

			// CAMBIAR A VENTANA PPAL
			st.cambiarVentanaPPAL(driver);
			driver.close();
			jt.escribirTxt(rutaestadoEjecucion, "EMISION OK");
		} catch (Exception e) {
			// SCREENSHOT
			st.captureScreen(driver, rutaDirectorio + "//Screenshots", "EmisionVidaEnteraFAIL");
			jt.escribirTxt(rutaestadoEjecucion, "EMISION NOK" + e.getMessage());
			driver.quit();
			e.printStackTrace();
		}
	}

}
