
package thread;
import java.io.BufferedReader;
import java.io.File;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import test.ProcesosRecurrentes;
import utils.BrowserFactory;
import utils.DriverFactory;
import utils.JavaTools;
import utils.SeleniumTools;

public class ThreadReportes extends Thread {
	
	JavaTools jt = new JavaTools();
	SeleniumTools st = new SeleniumTools();
	ProcesosRecurrentes procesosRecurrentes = new ProcesosRecurrentes();
	public WebDriver driver = null;
	String linea;
	
	Logger logger=Logger.getLogger("testReportesThread");
	String rutaDirectorio=System.getProperty("user.dir");
	
	BufferedReader bufer = jt.abrirLectura(rutaDirectorio,"ParametrosAmbiente.txt");
	String lineaParametrosAmbiente = jt.lecturaLinea(bufer);
	String separador1[] = lineaParametrosAmbiente.split("=");
	String PaginaWeb = separador1[3];// Direccion http de la prueba
	int tiempoDelay = Integer.parseInt(separador1[0]);// tiempo de delay de automatizacion
	int tiempoDivWait = Integer.parseInt(separador1[1]);// tiempo wait Big Layer
	int tiempoThrowWait = Integer.parseInt(separador1[2]);// tiempo de los wait
	
	
	public ThreadReportes(String linea) {
		super();
		this.linea = linea;
		
		driver = BrowserFactory.CreateInstance("firefox");
		DriverFactory.getInstance().setDriver(driver);
		driver= DriverFactory.getInstance().getDriver();
		
	}

	public void run () {
		
		//ABRIENDO LOG DE ESTADO EJECUCION
		String rutaestadoEjecucion = rutaDirectorio + "/estadoEjecucion.txt";
		
		//Crear directorio Reports si no existe
		File directory = new File(String.valueOf("Reports"));

		 if(!directory.exists()){

		             directory.mkdir();
		 }
		
		JavaTools jt = new JavaTools();
		
	
		ProcesosRecurrentes procesosRecurrentes = new ProcesosRecurrentes();
		
		Logger logger = Logger.getLogger("testReportes");
		PropertyConfigurator.configure("Log4j.properties");

		String SecurityLogin = separador1[4];// login
		String SecurityPassword = separador1[5];// password
		String USER_PREFERENCE_COUNTRY_NAME = separador1[6];// USER_PREFERENCE_COUNTRY_NAME
		String PARAMETER_LOCALE_NAME = separador1[7];// PARAMETER_LOCALE_NAME

		logger.info("SecurityLogin: " + SecurityLogin + " \n");
		logger.info("SecurityPassword: " + SecurityPassword + " \n");
		logger.info("USER_PREFERENCE_COUNTRY_NAME: " + USER_PREFERENCE_COUNTRY_NAME + " \n");
		logger.info("PARAMETER_LOCALE_NAME: " + PARAMETER_LOCALE_NAME + " \n");
		jt.cerrarLectura(bufer);

		logger.info("Parametros \n");
		logger.info("driverPath: " + rutaDirectorio + " \n");
		logger.info("PaginaWeb: " + PaginaWeb + " \n");
		logger.info("******************* \n");
		logger.info("Open Browser and navigate to designed URL \n");
			
		driver.get(PaginaWeb);
		
		logger.info("tiempo de inicio de test " + java.time.LocalDateTime.now());

		procesosRecurrentes.iniciarSesion(driver, "cotizacion", PaginaWeb, SecurityLogin, SecurityPassword,
				USER_PREFERENCE_COUNTRY_NAME, PARAMETER_LOCALE_NAME);

		procesosRecurrentes.validandoSesion(driver, PaginaWeb);
		
		
		///////////////  ENTRAR EN EL MENU REPORTES GENERALES VAADIN Menu2_7_5 /////////////////
		WebElement elemento = driver
				.findElement(By.id("Menu2_7_5"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click()", elemento);// metodo que abre menu de acsele de dos capas

		st.cambiarVentana(driver);
		
		try {

		
			String separador2[] = this.linea.split("=");
		
			//ESCRIBIR NOMBRE DE REPORTE EN /html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[3]/div/div[2]/div/div/input
			logger.info("Nombre del reporte: "+separador2[0]+"\n");
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[3]/div/div[2]/div/div/input")).sendKeys(separador2[0]);
			Thread.sleep(3000);
			
			//CLICK EN TEXTO REPORTES GENERALES /html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[1]/div/div/div
			logger.info("click en texto reportes generales \n");
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[1]/div/div/div")).click();
			Thread.sleep(4000);
			
			//CLICK EN NOMBRE DEL REPORTE	/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[3]/div/div[3]/div[1]/table/tbody/tr/td/div
			logger.info("click en nombre del reporte \n");
			
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[3]/div/div[3]/div[1]/table/tbody/tr/td/div")).click();	
			//List<WebElement> li = driver.findElements(By.xpath("//*[contains(text(), '"+separador2[0]+"')]"));
			//li.get(1).click();
			Thread.sleep(3000);
			
			//CLICK EN GENERAR EL REPORTE /html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[7]/div/div[1]/div/span/span
			logger.info("click en nombre generar reporte \n");
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[7]/div/div[1]/div/span/span")).click();
			Thread.sleep(3000);
			
			//SETEAR FECHA INICIAL /html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[5]/div/div[3]/div/div[3]/div/input
			logger.info("Setear fecha final: "+separador2[1]+"\n");
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[5]/div/div[3]/div/div[3]/div/input")).clear();
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[5]/div/div[3]/div/div[3]/div/input")).sendKeys(separador2[1]);
			Thread.sleep(3000);

	
			//CLICK EN GENERAR REPORTE /html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[7]/div/div[1]/div/span
			logger.info("click en btn generar reporte \n");
			//st.esperaXpathClickable(driver, 20, "/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[7]/div/div[1]/div/span/span");
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[7]/div/div[1]/div/span/span")).click();
			WebElement target = driver.findElement(By.xpath("//*[contains(text(), 'Generando el reporte, por favor espere...')]"));
			Actions builder = new Actions(driver);
			builder.moveToElement(target).perform();
			Thread.sleep(15000);
			
			
			//OBTENIENDO NOMBRE DEL REPORTE  /html/body/div[2]/div[3]/div/div/div[3]/div/div/div[1]/div/div/div/div
			logger.info("obteniendo nombre del reporte \n");
			//st.esperaXpathClickable(driver, 20, "/html/body/div[2]/div[3]/div/div/div[3]/div/div/div[1]/div/div/div/div");
			String kk = driver.findElement(By.xpath("/html/body/div[2]/div[3]/div/div/div[3]/div/div/div[1]/div/div/div/div")).getAttribute("innerHTML");
			String nombreReporte[] = kk.split(",");
			String nombreArchivo=nombreReporte[1].trim();
			nombreArchivo.trim();
			logger.info("obteniendo nombre del reporte:"+nombreArchivo+"\n");
			
			//SCREENSHOT
			st.captureScreen(driver, rutaDirectorio+"//Screenshots","Reportes");


			//CLICK EN ACCEPT /html/body/div[2]/div[3]/div/div/div[3]/div/div/div[3]/div/div/div[1]/div/span/span
			logger.info("click en btn Accept \n");
			driver.findElement(By.xpath("/html/body/div[2]/div[3]/div/div/div[3]/div/div/div[3]/div/div/div[1]/div/span/span")).click();
			Thread.sleep(3000);
			
		
			logger.info("tiempo de fin de test " + java.time.LocalDateTime.now());
			logger.info("Finalizando generacion de reporte");
			
			st.cambiarVentanaPPAL(driver);
			driver.close();
			jt.escribirTxt(rutaestadoEjecucion, "REPORTES OK");
			
			} catch (Exception e) {
				st.captureScreen(driver, rutaDirectorio+"//Screenshots","ReportesFail");
				jt.escribirTxt(rutaestadoEjecucion, "REPORTES NOK: "+e.getMessage());
				driver.quit();
				e.printStackTrace();
			}	
	}
	
	

}
