package thread;

import java.io.BufferedReader;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import test.ProcesosRecurrentes;
import utils.BrowserFactory;
import utils.DriverFactory;
import utils.JavaTools;
import utils.SeleniumTools;

public class ThreadPagoCaja extends Thread {

	JavaTools jt = new JavaTools();
	SeleniumTools st = new SeleniumTools();
	ProcesosRecurrentes procesosRecurrentes = new ProcesosRecurrentes();
	public WebDriver driver = null;
	String linea;
	String lineaNrosPoliza;

	Logger logger = Logger.getLogger("testPagoCaja");
	String rutaDirectorio = System.getProperty("user.dir");

	BufferedReader bufer = jt.abrirLectura(rutaDirectorio, "ParametrosAmbiente.txt");
	String lineaParametrosAmbiente = jt.lecturaLinea(bufer);
	String separador1[] = lineaParametrosAmbiente.split("=");
	String PaginaWeb = separador1[3];// Direccion http de la prueba
	int tiempoDelay = Integer.parseInt(separador1[0]);// tiempo de delay de automatizacion
	int tiempoDivWait = Integer.parseInt(separador1[1]);// tiempo wait Big Layer
	int tiempoThrowWait = Integer.parseInt(separador1[2]);// tiempo de los wait

	public ThreadPagoCaja(String linea, String lineaNrosPoliza) {
		super();
		this.linea = linea;
		this.lineaNrosPoliza = lineaNrosPoliza;

		driver = BrowserFactory.CreateInstance("firefox");
		DriverFactory.getInstance().setDriver(driver);
		driver = DriverFactory.getInstance().getDriver();

	}

	public void run() {

		// ABRIENDO LOG DE ESTADO EJECUCION
		String rutaestadoEjecucion = rutaDirectorio + "/estadoEjecucion.txt";

		try {
			JavaTools jt = new JavaTools();

			ProcesosRecurrentes procesosRecurrentes = new ProcesosRecurrentes();

			Logger logger = Logger.getLogger("testCaja");
			PropertyConfigurator.configure("Log4j.properties");

			String SecurityLogin = separador1[4];// login
			String SecurityPassword = separador1[5];// password
			String USER_PREFERENCE_COUNTRY_NAME = separador1[6];// USER_PREFERENCE_COUNTRY_NAME
			String PARAMETER_LOCALE_NAME = separador1[7];// PARAMETER_LOCALE_NAME

			logger.info("SecurityLogin: " + SecurityLogin + " \n");
			logger.info("SecurityPassword: " + SecurityPassword + " \n");
			logger.info("USER_PREFERENCE_COUNTRY_NAME: " + USER_PREFERENCE_COUNTRY_NAME + " \n");
			logger.info("PARAMETER_LOCALE_NAME: " + PARAMETER_LOCALE_NAME + " \n");
			jt.cerrarLectura(bufer);

			logger.info("Parametros \n");
			logger.info("driverPath: " + rutaDirectorio + " \n");
			logger.info("PaginaWeb: " + PaginaWeb + " \n");
			logger.info("******************* \n");
			logger.info("Open Browser and navigate to designed URL \n");

			driver.get(PaginaWeb);

			logger.info("tiempo de inicio de test " + java.time.LocalDateTime.now());

			procesosRecurrentes.iniciarSesion(driver, "cotizacion", PaginaWeb, SecurityLogin, SecurityPassword,
					USER_PREFERENCE_COUNTRY_NAME, PARAMETER_LOCALE_NAME);

			procesosRecurrentes.validandoSesion(driver, PaginaWeb);

			Thread.sleep(6000);

			/////// ENTRAR EN EL MENU
			WebElement elemento = driver.findElement(By.id("Menu2_5_1_17"));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click()", elemento);// metodo que abre menu de acsele de dos capas
			Thread.sleep(6000);

			st.cambiarVentana(driver);

			///////////////////////

			String separador2[] = this.linea.split("=");
			String lineaNrosPoliza = this.lineaNrosPoliza;

			String nombreCaja = separador2[0];// nombre de la caja

			Thread.sleep(6000);

			logger.info("Comprobando si la sesion esta activa \n");

			if (driver.findElements(By.xpath("//*[contains(text(), 'Sin sesi�n')]")).size() > 0) {

				logger.info("no tiene sesion en caja, buscando si la caja " + nombreCaja + " existe:  \n");

				if (driver.findElements(By.xpath("//*[contains(text(), '" + nombreCaja + "')]")).size() > 0) {

					logger.info("la caja existe: " + nombreCaja + " iniciando sesion \n");

					Thread.sleep(5000);
					driver.findElement(By.xpath("//*[contains(text(), '" + nombreCaja + "')]")).click();
					st.esperaXpathClickable(driver, 5,
							"/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[3]/div/div[3]/div/span");

					// BTN INICIAR SESION
					Thread.sleep(5000);
					driver.findElement(By.xpath(
							"/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[3]/div/div[1]/div/span/span"))
							.click();

					// BTN ACEPTAR
					Thread.sleep(5000);
					driver.findElement(
							By.xpath("/html/body/div[2]/div[3]/div/div/div[3]/div/div/div[2]/div/div[1]/div/span/span"))
							.click();

					// BTN CANCELAR
					Thread.sleep(5000);
					driver.findElement(
							By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[9]/div/div/div/span/span"))
							.click();

					logger.info("Fue iniciada la sesion en la caja: " + nombreCaja + "\n");

				} else {
					logger.info("La caja: " + nombreCaja + " No existe \n");
				}

			}

			else { // Si tiene una sesion activa la cierra y vuelve a abrir la sesion

				logger.info("El usuario posee una sesion de caja activa, cerrando sesion \n");

				// SELECCIONANDO CAJA
				Thread.sleep(4000);
				driver.findElement(By.xpath("//*[contains(text(), '" + nombreCaja + "')]")).click();

				// BTN CERRAR SESION
				Thread.sleep(8000);
				driver.findElement(
						By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[3]/div/div[3]/div/span"))
						.click();

				// SEGUNDO BTN CERRAR SESION
				Thread.sleep(4000);
				st.esperaXpathClickable(driver, 5,
						"/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[5]/div/div[1]/div/span");
				driver.findElement(
						By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[5]/div/div[1]/div/span"))
						.click();

				// ACEPTAR CERRAR SESION
				Thread.sleep(4000);
				st.esperaXpathClickable(driver, 5,
						"/html/body/div[2]/div[3]/div/div/div[3]/div/div/div[2]/div/div[1]/div/span/span");
				driver.findElement(
						By.xpath("/html/body/div[2]/div[3]/div/div/div[3]/div/div/div[2]/div/div[1]/div/span/span"))
						.click();

				logger.info("Iniciando una nueva sesion en la caja " + nombreCaja + " \n");

				// SELECCIONANDO CAJA
				Thread.sleep(4000);
				driver.findElement(By.xpath("//*[contains(text(), '" + nombreCaja + "')]")).click();

				// BTN INICIAR SESION
				Thread.sleep(4000);
				st.esperaXpathClickable(driver, 5,
						"/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[3]/div/div[1]/div/span/span");
				driver.findElement(
						By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[3]/div/div[1]/div/span/span"))
						.click();
				Thread.sleep(4000);

				// BTN ACEPTAR
				Thread.sleep(3000);
				driver.findElement(
						By.xpath("/html/body/div[2]/div[3]/div/div/div[3]/div/div/div[2]/div/div[1]/div/span/span"))
						.click();
				Thread.sleep(4000);

				// BTN CANCELAR
				Thread.sleep(3000);
				driver.findElement(
						By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[9]/div/div/div/span/span"))
						.click();
				Thread.sleep(4000);

				logger.info("La sesion fue creada con exito \n");

			}

			// Menu2_5_1_22 Coleccion de procesos

			////////////////////////////// ABRIENDO COLECCION DE
			////////////////////////////// PROCESOS////////////////////////////////

			st.cambiarVentanaPPAL(driver);

			Thread.sleep(8000);

			logger.info("volviendo a menu ppal \n");

			WebElement elemento2 = driver.findElement(By.id("Menu2_5_1_22"));
			JavascriptExecutor executor2 = (JavascriptExecutor) driver;
			executor2.executeScript("arguments[0].click()", elemento2);

			st.cambiarVentana(driver);

			// TIPO DE TRANSACCION
			// /html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[1]/div/div[3]/div/input
			// Pago
			Thread.sleep(5000);
			st.waitSearchWicketDisapear(driver, "v-loading-indicator third v-loading-indicator-wait", tiempoDivWait);
			driver.findElement(
					By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[1]/div/div[3]/div/input")).click();
			logger.info("Seleccionando tipo de transaccion Pago \n");
			Thread.sleep(4000);
			driver.findElement(
					By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[1]/div/div[3]/div/input"))
					.sendKeys(Keys.ARROW_DOWN);
			Thread.sleep(4000);
			driver.findElement(
					By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[1]/div/div[3]/div/input"))
					.sendKeys(Keys.ENTER);

			// CLICK A LUPA
			Thread.sleep(4000);
			logger.info("Click lupa \n");
			driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[5]/div/div[7]"))
					.click();

			/// NRO POLIZA id NumeroPoliza
			Thread.sleep(8000);
			String NroPoliza = separador2[2];

			if (NroPoliza.equals("NO")) {
				logger.info("NRO DE POLIZA EXTRAIDO DE ARCHIVO POLIZAS CREADAS: " + lineaNrosPoliza + "\n");
				driver.findElement(
						By.xpath("/html/body/div[2]/div[3]/div/div/div[3]/div/div/div[3]/div/div[2]/div/div/input[1]"))
						.sendKeys(lineaNrosPoliza);

				Thread.sleep(4000);
				driver.findElement(By.id("NumeroPoliza")).sendKeys(lineaNrosPoliza);
			} else {
				logger.info("NRO DE POLIZA: " + NroPoliza + "\n");
				driver.findElement(
						By.xpath("/html/body/div[2]/div[3]/div/div/div[3]/div/div/div[3]/div/div[2]/div/div/input[1]"))
						.sendKeys(NroPoliza);

				Thread.sleep(4000);
				driver.findElement(By.id("NumeroPoliza")).sendKeys(NroPoliza);
			}

			/// BTN BUSCAR
			/// /html/body/div[2]/div[3]/div/div/div[3]/div/div/div[1]/div/div[3]/div/div[1]/div/span/span
			Thread.sleep(5000);
			logger.info("BTN BUSCAR \n");
			driver.findElement(By.xpath(
					"/html/body/div[2]/div[3]/div/div/div[3]/div/div/div[1]/div/div[3]/div/div[1]/div/span/span"))
					.click();

			// SELECCIONAR POLIZA ENCONTRADA
			// /html/body/div[2]/div[3]/div/div/div[3]/div/div/div[3]/div/div[3]/div[1]/table/tbody/tr/td[1]/div
			Thread.sleep(4000);
			logger.info("SELECCIONAR POLIZA ENCONTRADA \n");
			driver.findElement(By.xpath(
					"/html/body/div[2]/div[3]/div/div/div[3]/div/div/div[3]/div/div[3]/div[1]/table/tbody/tr/td[1]/div"))
					.click();

			// BTN ACEPTAR
			// /html/body/div[2]/div[3]/div/div/div[3]/div/div/div[7]/div/div[1]/div/span/span
			Thread.sleep(4000);
			logger.info("CLICK EN ACEPTAR \n");
			driver.findElement(
					By.xpath("/html/body/div[2]/div[3]/div/div/div[3]/div/div/div[7]/div/div[1]/div/span/span"))
					.click();

			// BTN BUSCAR
			Thread.sleep(4000);
			logger.info("btn buscar \n");
			st.esperaXpathClickable(driver, 5,
					"/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[7]/div/div/div/span/span");
			driver.findElement(
					By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[7]/div/div/div/span/span"))
					.click();

			// SELECCIONAR TERCERO POR ROL Contratante
			// -/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[9]/div/div[3]/div[1]/table/tbody/tr/td[2]/div
			Thread.sleep(4000);
			logger.info("seleccionar tercero por rol: " + separador2[3] + " \n");
			driver.findElement(By.xpath(
					"/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[9]/div/div[3]/div[1]/table/tbody/tr/td[2]/div"))
					.click();
			/*
			 * driver.findElement(By.xpath("//*[contains(text(), '" + separador2[3] +
			 * "')]")).click();
			 */

			// BTN ACEPTAR
			// /html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[13]/div/div[1]/div/span/span
			Thread.sleep(4000);
			logger.info("btn aceptar \n");
			st.esperaXpathClickable(driver, 5,
					"/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[13]/div/div[1]/div/span/span");
			driver.findElement(
					By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[13]/div/div[1]/div/span/span"))
					.click();

			// Seleccionar por texto AporteInicial
			Thread.sleep(4000);
			logger.info("seleccionar por texto: " + separador2[4] + " \n");
			driver.findElement(By.xpath("//*[contains(text(), '" + separador2[4] + "')]")).click();

			// BTN ACEPTAR
			// /html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[5]/div/div[1]/div/span/span
			Thread.sleep(4000);
			logger.info("btn aceptar \n");
			st.esperaXpathClickable(driver, 5,
					"/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[5]/div/div[1]/div/span/span");
			driver.findElement(
					By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[5]/div/div[1]/div/span/span"))
					.click();

			// BTN AGREGAR
			// /html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[13]/div/div[3]/div/span/span
			Thread.sleep(4000);
			logger.info("btn agregar \n");
			st.esperaXpathClickable(driver, 5,
					"/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[13]/div/div[3]/div/span/span");
			driver.findElement(
					By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[13]/div/div[3]/div/span/span"))
					.click();

			// Tipo de pago
			// /html/body/div[2]/div[3]/div/div/div[3]/div/div/div[1]/div/div[3]/div/input
			// Efectivo
			Thread.sleep(4000);
			logger.info("Seleccionando tipo de pago " + separador2[5] + " \n");
			driver.findElement(By.xpath("/html/body/div[2]/div[3]/div/div/div[3]/div/div/div[1]/div/div[3]/div/input"))
					.click();
			Thread.sleep(2000);
			if (separador2[5].equals("Efectivo")) {
				driver.findElement(
						By.xpath("/html/body/div[2]/div[3]/div/div/div[3]/div/div/div[1]/div/div[3]/div/input"))
						.sendKeys(Keys.ARROW_DOWN);
				Thread.sleep(3000);
				driver.findElement(
						By.xpath("/html/body/div[2]/div[3]/div/div/div[3]/div/div/div[1]/div/div[3]/div/input"))
						.sendKeys(Keys.ARROW_DOWN);
				Thread.sleep(3000);
				driver.findElement(
						By.xpath("/html/body/div[2]/div[3]/div/div/div[3]/div/div/div[1]/div/div[3]/div/input"))
						.sendKeys(Keys.ARROW_DOWN);
				Thread.sleep(3000);
				driver.findElement(
						By.xpath("/html/body/div[2]/div[3]/div/div/div[3]/div/div/div[1]/div/div[3]/div/input"))
						.sendKeys(Keys.ARROW_DOWN);
				Thread.sleep(3000);
				driver.findElement(
						By.xpath("/html/body/div[2]/div[3]/div/div/div[3]/div/div/div[1]/div/div[3]/div/input"))
						.sendKeys(Keys.ENTER);
			} else {
				logger.info("Metodo de pago no listado en codigo consulte para agregarlo " + separador2[5] + " \n");
			}

			// Moneda
			// /html/body/div[2]/div[3]/div/div/div[3]/div/div/div[1]/div/div[5]/div/input
			// Pesos Mexicanos
			Thread.sleep(3000);
			logger.info("Seleccionando moneda " + separador2[6] + " \n");
			driver.findElement(By.xpath("/html/body/div[2]/div[3]/div/div/div[3]/div/div/div[1]/div/div[5]/div/input"))
					.click();
			Thread.sleep(2000);
			if (separador2[6].equals("Pesos Mexicanos")) {

				driver.findElement(
						By.xpath("/html/body/div[2]/div[3]/div/div/div[3]/div/div/div[1]/div/div[5]/div/input"))
						.sendKeys(Keys.ARROW_DOWN);
				Thread.sleep(3000);
				driver.findElement(
						By.xpath("/html/body/div[2]/div[3]/div/div/div[3]/div/div/div[1]/div/div[5]/div/input"))
						.sendKeys(Keys.ARROW_DOWN);
				Thread.sleep(3000);
				driver.findElement(
						By.xpath("/html/body/div[2]/div[3]/div/div/div[3]/div/div/div[1]/div/div[5]/div/input"))
						.sendKeys(Keys.ARROW_DOWN);
				Thread.sleep(3000);
				driver.findElement(
						By.xpath("/html/body/div[2]/div[3]/div/div/div[3]/div/div/div[1]/div/div[5]/div/input"))
						.sendKeys(Keys.ARROW_DOWN);
			} else if (separador2[6].equals("Dolares Americanos")) {
				Thread.sleep(3000);
				driver.findElement(
						By.xpath("/html/body/div[2]/div[3]/div/div/div[3]/div/div/div[1]/div/div[5]/div/input"))
						.sendKeys(Keys.ARROW_DOWN);
				Thread.sleep(3000);
				driver.findElement(
						By.xpath("/html/body/div[2]/div[3]/div/div/div[3]/div/div/div[1]/div/div[5]/div/input"))
						.sendKeys(Keys.ARROW_DOWN);
				Thread.sleep(3000);
				driver.findElement(
						By.xpath("/html/body/div[2]/div[3]/div/div/div[3]/div/div/div[1]/div/div[5]/div/input"))
						.sendKeys(Keys.ARROW_DOWN);
			} else {
				Thread.sleep(3000);
				driver.findElement(
						By.xpath("/html/body/div[2]/div[3]/div/div/div[3]/div/div/div[1]/div/div[5]/div/input"))
						.sendKeys(Keys.ARROW_DOWN);
				Thread.sleep(3000);
				driver.findElement(
						By.xpath("/html/body/div[2]/div[3]/div/div/div[3]/div/div/div[1]/div/div[5]/div/input"))
						.sendKeys(Keys.ARROW_DOWN);
			}
			Thread.sleep(3000);
			driver.findElement(By.xpath("/html/body/div[2]/div[3]/div/div/div[3]/div/div/div[1]/div/div[5]/div/input"))
					.sendKeys(Keys.ENTER);

			// Detectar Monto
			// /html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[5]/div/div[3]/div/div[2]/div[1]/table/tbody/tr/td[2]/div
			String kk = driver.findElement(By.xpath(
					"/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[5]/div/div[3]/div/div[2]/div[1]/table/tbody/tr/td[2]/div"))
					.getAttribute("innerHTML");
			logger.info("monto a pagar " + kk + " \n");

			// Monto /html/body/div[2]/div[3]/div/div/div[3]/div/div/div[1]/div/div[7]/input
			Thread.sleep(3000);
			logger.info("Ingresando monto " + kk + " \n");
			driver.findElement(By.xpath("/html/body/div[2]/div[3]/div/div/div[3]/div/div/div[1]/div/div[7]/input"))
					.sendKeys(kk);

			// Fecha de pago
			// /html/body/div[2]/div[3]/div/div/div[3]/div/div/div[1]/div/div[9]/div/input
			// DD/MM/YYYY
			Thread.sleep(3000);
			logger.info("fecha de pago " + separador2[7] + " \n");
			driver.findElement(By.xpath("/html/body/div[2]/div[3]/div/div/div[3]/div/div/div[1]/div/div[9]/div/input"))
					.sendKeys(separador2[7]);

			// ACEPTAR
			// /html/body/div[2]/div[3]/div/div/div[3]/div/div/div[3]/div/div[1]/div/span
			Thread.sleep(3000);
			st.waitSearchWicketDisapear(driver, "v-loading-indicator third v-loading-indicator-wait", tiempoDivWait);
			st.esperaXpathClickable(driver, 5,
					"/html/body/div[2]/div[3]/div/div/div[3]/div/div/div[3]/div/div[1]/div/span");
			driver.findElement(By.xpath("/html/body/div[2]/div[3]/div/div/div[3]/div/div/div[3]/div/div[1]/div/span"))
					.click();

			// SELECCION DE TEXTO POR TIPO DE PAGO Efectivo
			Thread.sleep(3000);
			logger.info("Seleccionando por texto " + separador2[5] + " \n");
			driver.findElement(By.xpath("//*[contains(text(), '" + separador2[5] + "')]")).click();

			// BTN DETALLE DE PAGO
			// /html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[13]/div/div[5]/div/span/span
			Thread.sleep(3000);
			logger.info("btn detalle de pago \n");
			st.esperaXpathClickable(driver, 5,
					"/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[13]/div/div[5]/div/span/span");
			driver.findElement(
					By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[13]/div/div[5]/div/span/span"))
					.click();

			// Nombre del pagador //*[@id="NombrePagadorCaja"] del txt cynthia
			Thread.sleep(3000);
			logger.info("nombre del pagador " + separador2[8] + " \n");
			driver.findElement(By.xpath(" //*[@id=\"NombrePagadorCaja\"]")).sendKeys(separador2[8]);

			// Dui pagador //*[@id="NumeroDUI"] del txt 5695
			Thread.sleep(3000);
			logger.info("NumeroDUI " + separador2[9] + " \n");
			driver.findElement(By.xpath("//*[@id=\"NumeroDUI\"]")).sendKeys(separador2[9]);

			// BTN ACEPTAR
			// /html/body/div[2]/div[3]/div/div/div[3]/div/div/div[3]/div/div[1]/div/span
			Thread.sleep(3000);
			st.esperaXpathClickable(driver, 5,
					"/html/body/div[2]/div[3]/div/div/div[3]/div/div/div[3]/div/div[1]/div/span");
			driver.findElement(By.xpath("/html/body/div[2]/div[3]/div/div/div[3]/div/div/div[3]/div/div[1]/div/span"))
					.click();

			/// ACEPTAR SOBRESCRIBIR RECURSO
			/// html/body/div[2]/div[5]/div/div/div[3]/div/div/div[2]/div/div[1]/div/span/span
			Thread.sleep(3000);
			st.esperaXpathClickable(driver, 5,
					"html/body/div[2]/div[5]/div/div/div[3]/div/div/div[2]/div/div[1]/div/span/span");
			driver.findElement(
					By.xpath("html/body/div[2]/div[5]/div/div/div[3]/div/div/div[2]/div/div[1]/div/span/span")).click();

			// BTN ACEPTAR
			// /html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[17]/div/div[1]/div/span/span
			Thread.sleep(3000);
			st.esperaXpathClickable(driver, 5,
					"/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[17]/div/div[1]/div/span/span");
			driver.findElement(
					By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[17]/div/div[1]/div/span/span"))
					.click();

			// SCREENSHOT
			st.captureScreen(driver, rutaDirectorio + "//Screenshots", "PagoCaja");

			// BTN APLICAR PAGOS
			// /html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[15]/div/div[1]/div/span/span
			Thread.sleep(3000);
			st.esperaXpathClickable(driver, 5,
					"/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[15]/div/div[1]/div/span/span");
			driver.findElement(
					By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[15]/div/div[1]/div/span/span"))
					.click();

			// ACEPTAR APLICAR PAGOS
			// /html/body/div[2]/div[3]/div/div/div[3]/div/div/div[2]/div/div[1]/div/span/span
			Thread.sleep(3000);

			st.esperaXpathClickable(driver, 5,
					"/html/body/div[2]/div[3]/div/div/div[3]/div/div/div[2]/div/div[1]/div/span/span");
			driver.findElement(
					By.xpath("/html/body/div[2]/div[3]/div/div/div[3]/div/div/div[2]/div/div[1]/div/span/span"))
					.click();

			st.cambiarVentanaPPAL(driver); // SE DESCOMENTA PARA CERRAR LA VENTANA AL TERMINAR EL PAGO EN CAJA Y VOLVER
											// AL MENU PPAL
			jt.escribirTxt(rutaestadoEjecucion, "PAGO CAJA OK");
			logger.info("tiempo de fin de test " + java.time.LocalDateTime.now());
			logger.info("Finalizando pago de caja");

			///////////////////////
			// CAMBIAR A VENTANA PPAL
			driver.quit();

		} catch (Exception e) {
			st.captureScreen(driver, rutaDirectorio + "//Screenshots", "PagoCajaFAIL");
			jt.escribirTxt(rutaestadoEjecucion, "PAGO CAJA NOK: " + e.getMessage());
			driver.quit();
			e.printStackTrace();
		}

	}

}
