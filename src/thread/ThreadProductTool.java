
package thread;
import java.io.BufferedReader;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import test.ProcesosRecurrentes;
import utils.BrowserFactory;
import utils.DriverFactory;
import utils.JavaTools;
import utils.SeleniumTools;

public class ThreadProductTool extends Thread {
	
	JavaTools jt = new JavaTools();
	SeleniumTools st = new SeleniumTools();
	ProcesosRecurrentes procesosRecurrentes = new ProcesosRecurrentes();
	public WebDriver driver = null;
	String linea;
	
	Logger logger=Logger.getLogger("testProductTool");
	String rutaDirectorio=System.getProperty("user.dir");
	
	BufferedReader bufer = jt.abrirLectura(rutaDirectorio,"ParametrosAmbiente.txt");
	String lineaParametrosAmbiente = jt.lecturaLinea(bufer);
	String separador1[] = lineaParametrosAmbiente.split("=");
	String PaginaWeb = separador1[3];// Direccion http de la prueba
	int tiempoDelay = Integer.parseInt(separador1[0]);// tiempo de delay de automatizacion
	int tiempoDivWait = Integer.parseInt(separador1[1]);// tiempo wait Big Layer
	int tiempoThrowWait = Integer.parseInt(separador1[2]);// tiempo de los wait
	
	
	public ThreadProductTool(String linea) {
		super();
		this.linea = linea;
		
		driver = BrowserFactory.CreateInstance("firefox");
		DriverFactory.getInstance().setDriver(driver);
		driver= DriverFactory.getInstance().getDriver();
		
	}

	public void run () {
		
		//ABRIENDO LOG DE ESTADO EJECUCION
		String rutaestadoEjecucion = rutaDirectorio + "/estadoEjecucion.txt";
		
		
		
		JavaTools jt = new JavaTools();
		
	
		ProcesosRecurrentes procesosRecurrentes = new ProcesosRecurrentes();
		
		Logger logger = Logger.getLogger("testReportes");
		PropertyConfigurator.configure("Log4j.properties");

		String SecurityLogin = separador1[4];// login
		String SecurityPassword = separador1[5];// password
		String USER_PREFERENCE_COUNTRY_NAME = separador1[6];// USER_PREFERENCE_COUNTRY_NAME
		String PARAMETER_LOCALE_NAME = separador1[7];// PARAMETER_LOCALE_NAME

		logger.info("SecurityLogin: " + SecurityLogin + " \n");
		logger.info("SecurityPassword: " + SecurityPassword + " \n");
		logger.info("USER_PREFERENCE_COUNTRY_NAME: " + USER_PREFERENCE_COUNTRY_NAME + " \n");
		logger.info("PARAMETER_LOCALE_NAME: " + PARAMETER_LOCALE_NAME + " \n");
		jt.cerrarLectura(bufer);

		logger.info("Parametros \n");
		logger.info("driverPath: " + rutaDirectorio + " \n");
		logger.info("PaginaWeb: " + PaginaWeb + " \n");
		logger.info("******************* \n");
		logger.info("Open Browser and navigate to designed URL \n");
			
		driver.get(PaginaWeb);
		
		logger.info("tiempo de inicio de test " + java.time.LocalDateTime.now());

		procesosRecurrentes.iniciarSesion(driver, "cotizacion", PaginaWeb, SecurityLogin, SecurityPassword,
				USER_PREFERENCE_COUNTRY_NAME, PARAMETER_LOCALE_NAME);

		procesosRecurrentes.validandoSesion(driver, PaginaWeb);
		
		
		///////////////  ENTRAR EN EL MENU PRODUCT TOOL NO PRIVILEGIADO Menu3_16 /////////////////
		WebElement elemento = driver
				.findElement(By.id("Menu3_16"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click()", elemento);// metodo que abre menu de acsele de dos capas

		st.cambiarVentana(driver);
		
		try {

		
			//String separador2[] = this.linea.split("=");
			Thread.sleep(18000);
			
			//SCREENSHOT
			st.captureScreen(driver, rutaDirectorio+"//Screenshots","ProductTool");
		
			logger.info("tiempo de fin de test " + java.time.LocalDateTime.now());
			logger.info("Finalizando inspeccion de product tool");
			
			st.cambiarVentanaPPAL(driver);
			driver.close();
			jt.escribirTxt(rutaestadoEjecucion, "PRODUCT TOOL OK");
			
			} catch (Exception e) {
				st.captureScreen(driver, rutaDirectorio+"//Screenshots","ProductToolFail");
				jt.escribirTxt(rutaestadoEjecucion, "PRODUCT TOOL NOK: "+e.getMessage());
				driver.quit();
				e.printStackTrace();
			}	
	}
	
	

}
