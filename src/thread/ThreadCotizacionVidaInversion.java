package thread;

import java.io.BufferedReader;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import test.IniciarDriver;
import test.ProcesosRecurrentes;
import utils.BrowserFactory;
import utils.DriverFactory;
import utils.JavaTools;
import utils.SeleniumTools;

public class ThreadCotizacionVidaInversion extends Thread {

	JavaTools jt = new JavaTools();
	SeleniumTools st = new SeleniumTools();
	IniciarDriver iniciarDriver;
	public WebDriver driver = null;
	String linea;

	Logger logger = Logger.getLogger("testCotizacion");
	String rutaDirectorio = System.getProperty("user.dir");

	BufferedReader bufer = jt.abrirLectura(rutaDirectorio, "ParametrosAmbiente.txt");
	String lineaParametrosAmbiente = jt.lecturaLinea(bufer);
	String separador1[] = lineaParametrosAmbiente.split("=");
	String PaginaWeb = separador1[3];// Direccion http de la prueba
	int tiempoDelay = Integer.parseInt(separador1[0]);// tiempo de delay de automatizacion
	int tiempoDivWait = Integer.parseInt(separador1[1]);// tiempo wait Big Layer
	int tiempoThrowWait = Integer.parseInt(separador1[2]);// tiempo de los wait

	String ruta = rutaDirectorio + "/nrosCotizacionVidaInversion.txt";
	String rutaParametrosEmision = rutaDirectorio + "/ParametrosEmision.txt";

	public ThreadCotizacionVidaInversion(String linea) {
		super();
		this.linea = linea;

		driver = BrowserFactory.CreateInstance("firefox");
		DriverFactory.getInstance().setDriver(driver);
		driver = DriverFactory.getInstance().getDriver();

	}

	public void run() {

		// ABRIENDO LOG DE ESTADO EJECUCION
		String rutaestadoEjecucion = rutaDirectorio + "/estadoEjecucion.txt";

		try {
			JavaTools jt = new JavaTools();

			ProcesosRecurrentes procesosRecurrentes = new ProcesosRecurrentes();

			Logger logger = Logger.getLogger("testCotizacion");
			PropertyConfigurator.configure("Log4j.properties");

			String SecurityLogin = separador1[4];// login
			String SecurityPassword = separador1[5];// password
			String USER_PREFERENCE_COUNTRY_NAME = separador1[6];// USER_PREFERENCE_COUNTRY_NAME
			String PARAMETER_LOCALE_NAME = separador1[7];// PARAMETER_LOCALE_NAME

			logger.info("SecurityLogin: " + SecurityLogin + " \n");
			logger.info("SecurityPassword: " + SecurityPassword + " \n");
			logger.info("USER_PREFERENCE_COUNTRY_NAME: " + USER_PREFERENCE_COUNTRY_NAME + " \n");
			logger.info("PARAMETER_LOCALE_NAME: " + PARAMETER_LOCALE_NAME + " \n");
			jt.cerrarLectura(bufer);

			logger.info("Parametros \n");
			logger.info("driverPath: " + rutaDirectorio + " \n");
			logger.info("PaginaWeb: " + PaginaWeb + " \n");
			logger.info("******************* \n");
			logger.info("Open Browser and navigate to designed URL \n");

			driver.get(PaginaWeb);

			logger.info("tiempo de inicio de test " + java.time.LocalDateTime.now());

			procesosRecurrentes.iniciarSesion(driver, "cotizacion", PaginaWeb, SecurityLogin, SecurityPassword,
					USER_PREFERENCE_COUNTRY_NAME, PARAMETER_LOCALE_NAME);

			procesosRecurrentes.validandoSesion(driver, PaginaWeb);

			WebElement elemento = driver.findElement(By.id("Menu2_2_1"));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click()", elemento);// metodo que abre menu de acsele

			st.cambiarVentana(driver);

			st.esperaByName(driver, 60, "productsComboBox");
			Select Product = new Select(driver.findElement(By.name("productsComboBox")));
			Product.selectByValue("VidaInversion");

			driver = st.waitSelectPopulatename(driver, 5, "validitiesComboBox");
			Select Vigencia = new Select(driver.findElement(By.name("validitiesComboBox")));
			Vigencia.selectByValue("-1");

			
			String separador2[] = this.linea.split("=");

			logger.info("info separador 2: " + separador2[0] + "\n");
			
			//String hoy = jt.getFechaHoy();// obtiene un String de la fecha de hoy
			//logger.info("fecha de hoy: " + hoy);
			driver.findElement(By.name("initialDate")).sendKeys(separador2[22]);

			driver.findElement(By.name("CreateQuoteButton")).click();

			st.esperaByName(driver, 300, "EventSection:content:Form:continueButton");
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			driver.findElement(By.name("EventSection:content:Form:continueButton")).click();
			logger.info("Entrando en cuerpo de poliza \n");

			// Esperar hasta que el elemento sea visible
			st.visibilityOfElementLocated(driver, tiempoDivWait, "POLVidaInversion_RegistroEjecutivo");

			String nroSerie = separador2[0];
			driver.findElement(By.id("POLVidaInversion_RegistroEjecutivo")).sendKeys(nroSerie);
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);

			// CLASE DE POLIZA
			Select clasePoliza = new Select(driver.findElement(By.id(
					"POLVidaInversion_ClasePoliza")));
			clasePoliza.selectByVisibleText(separador2[1]);// aca se deselecciona la clase de poliza
			
			//POLVidaInversion_AceptoDocContCorreo
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			if(driver.findElements(By.id("POLVidaInversion_AceptoDocContCorreo")).size() > 0) {
				Select DocContractual = new Select(driver.findElement(By.id("POLVidaInversion_AceptoDocContCorreo")));
				DocContractual.selectByIndex(1);
			}
	
			// ASOCIANDO CONTRATANTE
			logger.info("Agregando contratante \n");
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);

			st.clickJSxpath(driver, "//*[@id=\"POLVidaInversion_calculateButton\"]");

			logger.info("Primer calcular \n");

			
			String contratanteName = separador2[2];// nombre del contratante
			try {
				procesosRecurrentes.asociandoContratanteCotizacion(contratanteName, driver, tiempoThrowWait,
						tiempoDivWait, logger, separador2[3]);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			// AGREGANDO OBJETOS ASEGURADOS
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			st.visibilityOfElementLocated(driver, 20, "RiskInformation_newButton");
			logger.info("Agregando unidad de riesgo RiskInformation_newButton\n");
			driver.findElement(By.id("RiskInformation_newButton")).click();

			// FirefoxDriver.findElement(By.name("saveButtonRU")).click();
			logger.info("Información basica \n");
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);

			logger.info("Salvando unidad de riesgo RiskBasicInformationContent_saveButton \n");
			st.visibilityOfElementLocated(driver, 30, "RiskBasicInformationContent_saveButton");
			driver.findElement(By.id("RiskBasicInformationContent_saveButton")).click();

			// AGREGAR OBJETO ASEGURADO BTN AGREGAR
			logger.info("Agregando objeto asegurado btn InsuranceRiskUnit_newButton \n");
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			st.visibilityOfElementLocated(driver, 30, "InsuranceRiskUnit_newButton");
			driver.findElement(By.id("InsuranceRiskUnit_newButton")).click();

			logger.info("Seleccion de plan de objeto asegurado: " + separador2[4] + "\n");
			if (separador2[4].contentEquals("INVERSION")) {
				// Plan Inversion
				st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
				st.visibilityOfElementLocated(driver, 50, "check_PLANVidaInversionPIM");
				driver.findElement(By.id("check_PLANVidaInversionPIM")).click();
			} else {
				// Plan Retiro
				st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
				st.visibilityOfElementLocated(driver, 50, "check_PLANVidaInversion");
				driver.findElement(By.id("check_PLANVidaInversion")).click();
			}

			// ACEPTAR AGREGAR OBJETOS ASEGURADOS
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			driver.findElement(By.name("AddInsuranceObjectTable:buttonForm:addButton")).click();
			// Esperando pantalla de espera finalice
			st.waitVaadin(driver, "waitVaadin", 10);

			// INFORMACION DE LA COTIZACION
			// NOMBRE DEL ASEGURADO
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			driver.findElement(By.name("templateIO:tabPanel:repeaterTab:3:SubTabsInformation:repeater:1:fila:field"))
					.sendKeys(separador2[11]);

			// SEGMENTO
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			st.waitSelectPopulateid(driver, 20, "OAVidaInversion_SegmentoCOT");
			logger.info("Agregando segmento en el campo OAVidaInversion_SegmentoCOT:" + separador2[5] + "\n");
			Select segmento2 = new Select(driver.findElement(By.id("OAVidaInversion_SegmentoCOT")));
			segmento2.selectByVisibleText(separador2[5]);
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait + 5);
			Select segmento22 = new Select(driver.findElement(By.id("OAVidaInversion_SegmentoCOT")));
			segmento22.selectByVisibleText(separador2[5]);

			// FECHA NACIMIENTO
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			String FechaNac = separador2[6];// Fecha Nacimiento
			logger.info(
					"Agregando fecha de nacimiento en el campo OAVidaInversion_FechaNacimientoCOT:" + FechaNac + "\n");
			logger.info("Fecha Nacimiento " + FechaNac + "\n");
			driver.findElement(
					By.name("templateIO:tabPanel:repeaterTab:3:SubTabsInformation:repeater:3:fila:fieldDate"))
					.sendKeys(FechaNac);
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);

			// PERFIL DE INVERSIONISTA 1
			st.waitSelectPopulateid(driver, 30, "OAVidaInversion_PerfilAsesoramiento");
			logger.info("Agregando perfil de inversionista 1 en el campo OAVidaInversion_PerfilAsesoramiento:"
					+ separador2[7] + "\n");
			Select PerfilInversionista1 = new Select(driver.findElement(By.id("OAVidaInversion_PerfilAsesoramiento")));
			PerfilInversionista1.selectByVisibleText(separador2[7]);
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait + 5);
			Select PerfilInversionista11 = new Select(driver.findElement(By.id("OAVidaInversion_PerfilAsesoramiento")));
			PerfilInversionista11.selectByVisibleText(separador2[7]);
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait + 5);

			// PERFIL DE INVERSIONISTA 2
			st.waitSelectPopulateid(driver, 20, "OAVidaInversion_PerfilInversionista");
			logger.info("Agregando perfil de inversionista 2 en el campo OAVidaInversion_PerfilInversionista:"
					+ separador2[8] + "\n");
			Select PerfilInversionista2 = new Select(driver.findElement(By.id("OAVidaInversion_PerfilInversionista")));
			PerfilInversionista2.selectByVisibleText(separador2[8]);
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);

			// PERFIL DE INVERSIONISTA 3
			st.waitSelectPopulateid(driver, 20, "OAVidaInversion_PerfilInversionistaEjec");
			logger.info("Agregando perfil de inversionista 3 en el campo OAVidaInversion_PerfilInversionistaEjec:"
					+ separador2[9] + "\n");
			Select PerfilInversionista3 = new Select(
					driver.findElement(By.id("OAVidaInversion_PerfilInversionistaEjec")));
			PerfilInversionista3.selectByVisibleText(separador2[9]);
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);

			// APORTACION INICIAL
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			logger.info("Agregando Aportacion inicial en el campo OAVidaInversion_TO_AportacionInicial:" + separador2[10]
					+ "\n");
			driver.findElement(By.id("OAVidaInversion_TO_AportacionInicial")).clear();
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			driver.findElement(By.id("OAVidaInversion_TO_AportacionInicial")).sendKeys(separador2[10]);
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			driver.findElement(By.id("OAVidaInversion_TO_SumaAsegFallec")).click();

			if (separador2[20] != null) {
				// APORTE PROGRAMADO
				// MONTO APORTE PROGRAMADO id = OAVidaInversion_TO_MontoAporteProgramado
				st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
				logger.info("MONTO APORTE PROGRAMADO: " + separador2[20] + "\n");
				driver.findElement(By.id("OAVidaInversion_TO_MontoAporteProgramado")).clear();
				st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
				driver.findElement(By.id("OAVidaInversion_TO_MontoAporteProgramado")).sendKeys(separador2[20]);

				st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
				driver.findElement(By.id("OAVidaInversion_TO_SumaAsegFallec")).click();
			}

			if (separador2[21] != null) {
				// FRECUENCIA APORTE PROGRAMADO id = OAVidaInversion_FrecuenciaAportePrograma
				st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
				logger.info("FRECUENCIA APORTE PROGRAMADO: " + separador2[21] + "\n");
				st.waitSelectPopulateid(driver, 20, "OAVidaInversion_FrecuenciaAportePrograma");
				Select FrecuenciaAporteProgramado = new Select(
						driver.findElement(By.id("OAVidaInversion_FrecuenciaAportePrograma")));
				FrecuenciaAporteProgramado.selectByVisibleText(separador2[21]);
				st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			}

			// SALVAR INFORMACION DE LA COTIZACION
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait + 10);
			st.esperaId(driver, 5, "InformationInsurance_saveButton");
			driver.findElement(By.id("InformationInsurance_saveButton")).click();
			logger.info("Salvando Informacion de cotizacion 1 InformationInsurance_saveButton \n");

			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);

			// ASOCIANDO BENEFICIARIO
			String beneficiarioName = separador2[11]; // Nombre predeterminado del beneficiario
			String TBeneficiario = separador2[12]; // Tipo de beneficiario
			try {
				procesosRecurrentes.asociandoBeneficiarioCotizacion(beneficiarioName, TBeneficiario, driver,
						tiempoThrowWait, tiempoDivWait, logger);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// ASOCIANDO ASEGURADO
			String aseguradoName = separador2[13];// Nombre predeterminado del asegurado
			try {
				procesosRecurrentes.asociandoAseguradoCotizacion(aseguradoName, TBeneficiario, driver, tiempoThrowWait,
						tiempoDivWait, logger);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// REQUISITOS
			// AGREGAR
			logger.info("agregar todos los requisitos \n");

			driver.findElement(By.xpath(
					"/html/body/div[1]/div[3]/div[2]/div[2]/div[2]/div[3]/div/div[2]/div/div[5]/div/div[2]/div[5]/div[1]/div[2]/div[2]/div[2]/table/thead/tr/th[9]/div/a/span"))
					.click();

			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			driver.findElement(By.name("PolicyFundForm:AddButton")).click();
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);

			// REQUISITOS
			// TIPO DE CTA 1
			String Tcta = separador2[14];
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			Select Cuenta1 = new Select(driver.findElement(By.name("FundAccountForm:AccountComb")));
			Cuenta1.selectByVisibleText(Tcta);
			logger.info("Agregando cuenta 1 " + Tcta);

			// PORTAFOLIO 1
			String Ptfolio = separador2[15];
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			st.waitSelectPopulatename(driver, 3, "FundAccountForm:AccountPortComb");
			Select Portafolio1 = new Select(driver.findElement(By.name("FundAccountForm:AccountPortComb")));
			Portafolio1.selectByVisibleText(Ptfolio);
			logger.info("Agregando portafolio 1 " + Ptfolio);

			// PORCENTAJE 1
			String Percnt = separador2[16];
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			driver.findElement(By.name("FundAccountForm:PercentAccountPort")).sendKeys(Percnt);
			logger.info("Agregando porcentaje 1 " + Percnt);

			// SALVAR PRIMERA CTA
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			driver.findElement(By.name("InfoAccountForm:saveButton")).click();
			logger.info("Salvando Primera Cta");
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);

			// AGREGAR CTA2
			driver.findElement(By.name("PolicyFundForm:AddButton")).click();
			logger.info("Agregando Segunda Cuenta");
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);

			// TIPO DE CTA 2
			String AccountComb = separador2[17];
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			Select Cuenta2 = new Select(driver.findElement(By.name("FundAccountForm:AccountComb")));
			Cuenta2.selectByVisibleText(AccountComb);
			logger.info("Agregando cuenta 2 " + AccountComb);

			// PORTAFOLIO 2
			String Ptfolio2 = separador2[18];
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			st.waitSelectPopulatename(driver, 3, "FundAccountForm:AccountPortComb");
			Select Portafolio2 = new Select(driver.findElement(By.name("FundAccountForm:AccountPortComb")));
			Portafolio2.selectByVisibleText(Ptfolio2);
			logger.info("Agregando portafolio 1 " + Ptfolio2);

			// PORCENTAJE 2
			String Percnt2 = separador2[19];
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			driver.findElement(By.name("FundAccountForm:PercentAccountPort")).sendKeys(Percnt2);
			logger.info("Agregando porcentaje 2 " + Percnt2);

			// SALVANDO SEGUNDA CTA
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			driver.findElement(By.name("InfoAccountForm:saveButton")).click();
			st.waitSearchWicket(driver);

			// CALCULAR
			String kk = null;
			try {
				kk = procesosRecurrentes.calcularCotizacion(driver, tiempoThrowWait);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String nroCotSeparado = jt.separarNro(kk);
			logger.info("Nro de la cotizacion " + nroCotSeparado);
			jt.escribirTxt(ruta, nroCotSeparado);
			jt.escribirTxt(rutaParametrosEmision, nroCotSeparado);

			// SCREENSHOT
			st.captureScreen(driver, rutaDirectorio + "/Screenshots", "CotizacionVidaEntera");

			// CAMBIAR A VENTANA PPAL
			st.cambiarVentanaPPAL(driver);
			driver.close();
			jt.escribirTxt(rutaestadoEjecucion, "COTIZACION VIDA INVERSION OK");
		} catch (Exception e) {
			// SCREENSHOT
			st.captureScreen(driver, rutaDirectorio + "/Screenshots", "CotizacionVidaEnteraFAIL");
			jt.escribirTxt(rutaestadoEjecucion, "COTIZACION VIDA INVERSION NOK" + e.getMessage());
			driver.quit();
			e.printStackTrace();
		}

		return;
	}

}
