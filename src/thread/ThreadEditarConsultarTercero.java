package thread;

import java.io.BufferedReader;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import test.ProcesosRecurrentes;
import utils.BrowserFactory;
import utils.DriverFactory;
import utils.JavaTools;
import utils.SeleniumTools;

public class ThreadEditarConsultarTercero extends Thread {

	JavaTools jt = new JavaTools();
	SeleniumTools st = new SeleniumTools();
	ProcesosRecurrentes procesosRecurrentes = new ProcesosRecurrentes();

	public WebDriver driver = null;
	String linea;

	Logger logger = Logger.getLogger("testEditarConsultarTercero");
	String rutaDirectorio = System.getProperty("user.dir");

	BufferedReader bufer = jt.abrirLectura(rutaDirectorio, "ParametrosAmbiente.txt");
	String lineaParametrosAmbiente = jt.lecturaLinea(bufer);
	String separador1[] = lineaParametrosAmbiente.split("=");
	String PaginaWeb = separador1[3];// Direccion http de la prueba
	int tiempoDelay = Integer.parseInt(separador1[0]);// tiempo de delay de automatizacion
	int tiempoDivWait = Integer.parseInt(separador1[1]);// tiempo wait Big Layer
	int tiempoThrowWait = Integer.parseInt(separador1[2]);// tiempo de los wait

	String ruta = rutaDirectorio + "/nrosEndosos.txt";
	String rutaParametrosEndoso = rutaDirectorio + "/ParametrosEndoso.txt";

	public ThreadEditarConsultarTercero(String linea) {
		super();
		this.linea = linea;

		driver = BrowserFactory.CreateInstance("firefox");
		DriverFactory.getInstance().setDriver(driver);
		driver = DriverFactory.getInstance().getDriver();

	}

	public void run() {

		// ABRIENDO LOG DE ESTADO EJECUCION
		String rutaestadoEjecucion = rutaDirectorio + "/estadoEjecucion.txt";

		try {

			JavaTools jt = new JavaTools();
			ProcesosRecurrentes procesosRecurrentes = new ProcesosRecurrentes();

			Logger logger = Logger.getLogger("testEditarConsultarTercero");
			PropertyConfigurator.configure("Log4j.properties");

			String SecurityLogin = separador1[4];// login
			String SecurityPassword = separador1[5];// password
			String USER_PREFERENCE_COUNTRY_NAME = separador1[6];// USER_PREFERENCE_COUNTRY_NAME
			String PARAMETER_LOCALE_NAME = separador1[7];// PARAMETER_LOCALE_NAME

			logger.info("SecurityLogin: " + SecurityLogin + " \n");
			logger.info("SecurityPassword: " + SecurityPassword + " \n");
			logger.info("USER_PREFERENCE_COUNTRY_NAME: " + USER_PREFERENCE_COUNTRY_NAME + " \n");
			logger.info("PARAMETER_LOCALE_NAME: " + PARAMETER_LOCALE_NAME + " \n");
			jt.cerrarLectura(bufer);

			logger.info("Parametros \n");
			logger.info("driverPath: " + rutaDirectorio + " \n");
			logger.info("PaginaWeb: " + PaginaWeb + " \n");
			logger.info("******************* \n");
			logger.info("Open Browser and navigate to designed URL \n");

			driver.get(PaginaWeb);

			logger.info("tiempo de inicio de test " + java.time.LocalDateTime.now());

			procesosRecurrentes.iniciarSesion(driver, "cotizacion", PaginaWeb, SecurityLogin, SecurityPassword,
					USER_PREFERENCE_COUNTRY_NAME, PARAMETER_LOCALE_NAME);

			procesosRecurrentes.validandoSesion(driver, PaginaWeb);

			/////// ENTRAR EN EL MENU
			WebElement elemento = driver.findElement(By.id("Menu4_2_2"));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click()", elemento);

			st.cambiarVentana(driver);

			String separador2[] = this.linea.split("=");

			logger.info("info separador 2: " + separador2[0] + "\n");

			///////////////////////////

			st.waitSelectPopulatename(driver, 3, "templateThird:repeaterPanel1:1:fila:repeaterSelect:1:field");

			Select InternHSBC = new Select(
					driver.findElement(By.name("templateThird:repeaterPanel1:1:fila:repeaterSelect:1:field")));
			InternHSBC.selectByVisibleText(separador2[0]);
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			logger.info("Cliente de HSBC? " + separador2[0] + "\n");

			// CIS
			logger.info("Nombre: " + separador2[1] + "\n");
			WebElement tbNombre = driver.findElement(By.id("_CISCliente"));
			tbNombre.sendKeys(separador2[1]);
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);

			// BUSCAR
			driver.findElement(By.name("searchButton")).click();
			logger.info("Buscar \n");
			Thread.sleep(9000);
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);

			// RADIO
			driver.findElement(By.xpath(
					"/html/body/div[1]/div[3]/div[1]/div/div/div[2]/div[1]/div/div/div[4]/div[1]/div[1]/table/tbody/tr/td[1]/div/input"))
					.click();
			logger.info("Seleccionar radio de tercero \n");
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);

			// EDITAR
			driver.findElement(By.name("associateButton")).click();
			logger.info("Editar \n");
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);

			// FECHA DE NACIMIENTO
			logger.info("Fecha de Nacimiento: " + separador2[2] + "\n");
			Thread.sleep(3000);
			Actions actions = new Actions(driver);
			WebElement elementLocator = driver.findElement(By.id("NaturalPerson_BirthDate"));
			elementLocator.clear();
			Thread.sleep(2000);
			actions.contextClick(elementLocator).perform();
			st.waitSearchWicketDisapear(driver, "bigLayer", 15);
			WebElement elementLocator2 = driver.findElement(By.id("NaturalPerson_BirthDate"));
			st.waitSearchWicketDisapear(driver, "bigLayer", 20);
			elementLocator2.sendKeys(separador2[2]);

			// SALVAR FECHA NACIMIENTO
			driver.findElement(By.id("ThirdInformation_searchButton")).click();
			logger.info("Salvando fecha de nacimiento \n");
			st.waitSearchWicketDisapear(driver, "bigLayer", 20);

			// INGRESOS ANUALES
			logger.info("INGRESOS ANUALES: " + separador2[3] + "\n");
			Thread.sleep(3000);
			Actions actions1 = new Actions(driver);
			WebElement elementLocator3 = driver.findElement(By.id("NaturalPerson_TO_IngresoAnual"));
			Thread.sleep(1000);
			elementLocator3.clear();
			Thread.sleep(2000);
			actions1.contextClick(elementLocator3).perform();
			st.waitSearchWicketDisapear(driver, "bigLayer", 15);
			WebElement elementLocator4 = driver.findElement(By.id("NaturalPerson_TO_IngresoAnual"));
			// st.waitSearchWicketDisapear(driver, "bigLayer", 20);
			elementLocator4.sendKeys(separador2[3]);

			// SALVAR IMGRESOS ANUALES
			driver.findElement(By.id("ThirdInformation_searchButton")).click();
			logger.info("Salvando ingresos anuales \n");
			st.waitSearchWicketDisapear(driver, "bigLayer", 20);

			// ACTIVIDAD ECONOMICA
			logger.info("Actividad Economica: " + separador2[4] + "\n");
			st.waitSelectPopulateid(driver, 10, "_ActividadEconom");
			Select ActEc = new Select(driver.findElement(By.id("NaturalPerson_ActividadEconom")));
			st.waitSearchWicketDisapear(driver, "bigLayer", 15);
			ActEc.selectByVisibleText(separador2[4]);
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			Thread.sleep(3000);

			// **GUARDAR INFORMACION DE TERCERO
			Thread.sleep(3000);
			driver.findElement(By.id("ThirdInformation_searchButton")).click();
			logger.info("Guardar Tercero \n");

			// VOLVER A BUSQUEDA DE TERCERO
			Thread.sleep(6000);
			st.waitSearchWicketDisapear(driver, "bigLayer", 20);
			// WebElement elementLocator1 =
			// driver.findElement(By.xpath("/html/body/div[1]/div[3]/div[2]/div[1]/form/div[2]/div[2]/a/span"));
			// actions.contextClick(elementLocator1).perform();
			driver.findElement(By.xpath("/html/body/div[1]/div[3]/div[2]/div[1]/form/div[2]/div[2]/a/span")).click();
			st.waitSearchWicketDisapear(driver, "bigLayer", 15);

			// RADIO 2
			driver.findElement(By.xpath(
					"/html/body/div[1]/div[3]/div[1]/div/div/div[2]/div[1]/div/div/div[4]/div[1]/div[1]/table/tbody/tr/td[1]/div/input"))
					.click();
			logger.info("Seleccionar radio de tercero \n");
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);

			// **CONSULTAR
			driver.findElement(By.xpath(
					"/html/body/div[1]/div[3]/div[1]/div/div/div[2]/div[1]/div/div/div[4]/div[1]/div[2]/div/form/input[2]"))
					.click();
			logger.info("Guardar Tercero \n");

			// SCREENSHOT
			st.captureScreen(driver, rutaDirectorio + "//Screenshots", "EditarConsultarTercero");

			jt.escribirTxt(rutaestadoEjecucion, "EDITAR CONSULTAR TERCERO OK");
		} catch (Exception e) {
			st.captureScreen(driver, rutaDirectorio + "//Screenshots", "EditarConsultarTerceroFAIL");
			jt.escribirTxt(rutaestadoEjecucion, "EDITAR CONSULTAR TERCERO NOK: " + e.getMessage());
			driver.quit();
			e.printStackTrace();
		}

		jt.escribirTxt(ruta, "Finalizando Consulta y Edicion de Terceros \n");
		logger.info("tiempo de fin de test " + java.time.LocalDateTime.now());
		logger.info("Finalizando endosos");
		// CAMBIAR A VENTANA PPAL
		st.cambiarVentanaPPAL(driver);
		driver.close();
		return;

	}

}
