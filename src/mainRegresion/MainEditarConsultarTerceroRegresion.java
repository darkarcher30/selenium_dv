package mainRegresion;
import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;

import javax.mail.MessagingException;

import testRegresion.EditarConsultarTerceroRegresion;
import utils.JavaTools;
import utils.Mail;



public class MainEditarConsultarTerceroRegresion {

	public static void main(String[] args) throws IOException {
		
		
		//BORRADO DE LOG
		JavaTools jt = new JavaTools();
		String rutaDirectorio=System.getProperty("user.dir");
		jt.deleteFile(rutaDirectorio+"\\log\\application.html");
		jt.deleteFile(rutaDirectorio+"\\log\\testlog.log");
		jt.deleteFile(rutaDirectorio+"\\log\\testlog1.log");
		jt.deleteFile(rutaDirectorio+"\\estadoEjecucion.txt");
		
		File file2 = new File(rutaDirectorio+"\\Screenshots");
		jt.purgeDirectory(file2);
		
		
		//CONSULTAR TERCERO
		EditarConsultarTerceroRegresion newEditarConsultarTerceroRegresion = new EditarConsultarTerceroRegresion();

		try {
			newEditarConsultarTerceroRegresion.testConsultarTerceroRegresion();
		} catch (Exception e) {
			e.printStackTrace();
		}
		newEditarConsultarTerceroRegresion.endTest();
				
		System.out.println("De vuelta en el main, EditarConsultarTerceroRegresion");
		
		//ENVIANDO CORREO DE LOG
		Mail newMail= new Mail();
		try {
			newMail.sendMail(null);
		} catch (MessagingException | GeneralSecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


}
	
}