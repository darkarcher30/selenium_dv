package mainRegresion;
import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;

import javax.mail.MessagingException;

import testRegresion.CrearPersonaRegresion;
import utils.JavaTools;
import utils.Mail;



public class MainCrearPersonaRegresion {

	public static void main(String[] args) throws IOException {
		
		//BORRADO DE LOG
		JavaTools jt = new JavaTools();
		String rutaDirectorio=System.getProperty("user.dir");
		jt.deleteFile(rutaDirectorio+"\\log\\application.html");
		jt.deleteFile(rutaDirectorio+"\\log\\testlog.log");
		jt.deleteFile(rutaDirectorio+"\\log\\testlog1.log");
		jt.deleteFile(rutaDirectorio+"\\estadoEjecucion.txt");
		
		File file2 = new File(rutaDirectorio+"\\Screenshots");
		jt.purgeDirectory(file2);
		
		
		//CREAR PERSONA
		CrearPersonaRegresion newCrearPersonaRegresion = new CrearPersonaRegresion();

		try {
			newCrearPersonaRegresion.testCrearPersonaRegresion();
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
		newCrearPersonaRegresion.endTest();
				
		System.out.println("De vuelta en el main, crear persona");
		
		//ENVIANDO CORREO DE LOG
		Mail newMail= new Mail();
		try {
			newMail.sendMail(null);
		} catch (MessagingException | GeneralSecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

}
	
}