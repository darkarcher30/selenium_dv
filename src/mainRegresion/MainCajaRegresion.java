package mainRegresion;
import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;

import javax.mail.MessagingException;

import testRegresion.PagoCajaRegresion;
import utils.JavaTools;
import utils.Mail;


public class MainCajaRegresion {

	public static void main(String[] args) throws IOException {
		
		//BORRADO DE LOG
		JavaTools jt = new JavaTools();
		String rutaDirectorio=System.getProperty("user.dir");
		jt.deleteFile(rutaDirectorio+"\\log\\application.html");
		jt.deleteFile(rutaDirectorio+"\\log\\testlog.log");
		jt.deleteFile(rutaDirectorio+"\\log\\testlog1.log");
		jt.deleteFile(rutaDirectorio+"\\estadoEjecucion.txt");
		
		File file2 = new File(rutaDirectorio+"\\Screenshots");
		jt.purgeDirectory(file2);

		
		//PAGO CAJA
		PagoCajaRegresion newPagoCajaRegresion = new PagoCajaRegresion();

		try {
			newPagoCajaRegresion.testCaja();
			
		} catch (Exception e) {
			e.printStackTrace();

		}
		newPagoCajaRegresion.endTest();
				
		System.out.println("De vuelta en el main, caja");
		
		//ENVIANDO CORREO DE LOG
		Mail newMail= new Mail();
		try {
			newMail.sendMail(null);
		} catch (MessagingException | GeneralSecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


}
	
}