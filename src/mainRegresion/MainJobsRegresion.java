package mainRegresion;
import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;

import javax.mail.MessagingException;

import testRegresion.EjecucionDeJobsRegresion;
import utils.JavaTools;
import utils.Mail;


public class MainJobsRegresion {

	public static void main(String[] args) throws IOException {
		
		//BORRADO DE LOG
		JavaTools jt = new JavaTools();
		String rutaDirectorio=System.getProperty("user.dir");
		jt.deleteFile(rutaDirectorio+"\\log\\application.html");
		jt.deleteFile(rutaDirectorio+"\\log\\testlog.log");
		jt.deleteFile(rutaDirectorio+"\\log\\testlog1.log");
		jt.deleteFile(rutaDirectorio+"\\estadoEjecucion.txt");
		
		File file2 = new File(rutaDirectorio+"\\Screenshots");
		jt.purgeDirectory(file2);
		
		
		//EJECUCION DE JOBS
		EjecucionDeJobsRegresion newJobsRegresion = new EjecucionDeJobsRegresion();

		try {
			newJobsRegresion.testJobs();
		} catch (Exception e) {
			e.printStackTrace();
		}
		newJobsRegresion.endTest();
				
		System.out.println("De vuelta en el main, de jobs");

		//ENVIANDO CORREO DE LOG
		Mail newMail= new Mail();
		try {
		newMail.sendMail(null);
		} catch (MessagingException | GeneralSecurityException e) {
		e.printStackTrace();
		}

}
	
}