package mainRegresion;
import java.io.File;
import java.io.IOException;
import testRegresion.ReportesRegresion;
import utils.JavaTools;


public class MainReportesRegresion {

	public static void main(String[] args) throws IOException {
		
		//BORRADO DE LOG
		JavaTools jt = new JavaTools();
		String rutaDirectorio=System.getProperty("user.dir");
		jt.deleteFile(rutaDirectorio+"\\log\\application.html");
		jt.deleteFile(rutaDirectorio+"\\log\\testlog.log");
		jt.deleteFile(rutaDirectorio+"\\log\\testlog1.log");
		jt.deleteFile(rutaDirectorio+"\\Reports");
		jt.deleteFile(rutaDirectorio+"\\estadoEjecucion.txt");
			
		File file2 = new File(rutaDirectorio+"\\Screenshots");
		jt.purgeDirectory(file2);
		
		File file = new File(rutaDirectorio+"\\Reports");
		jt.purgeDirectory(file);
			
		//EJECUCION DE REPORTES
		ReportesRegresion newReportesRegresion = new ReportesRegresion();

		try {
			newReportesRegresion.testReportes();
			
		} catch (Exception e) {

			e.printStackTrace();
		}
		newReportesRegresion.endTest();
				
		System.out.println("De vuelta en el main, caja");


}
	
}