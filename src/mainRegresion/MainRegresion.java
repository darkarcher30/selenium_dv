package mainRegresion;

import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import javax.mail.MessagingException;
import testRegresion.CotizacionVidaInversionRegresion;
import testRegresion.EditarConsultarTerceroRegresion;
import testRegresion.EjecucionDeJobsRegresion;
import testRegresion.EmisionTestRegresion;
import testRegresion.EndosoRegresion;
import testRegresion.PagoCajaRegresion;
import testRegresion.ProductToolRegresion;
import testRegresion.ReportesRegresion;
import utils.JavaTools;
import utils.Mail;

public class MainRegresion {

	public static void main(String[] args) throws IOException {

		// BORRADO DE LOG
		JavaTools jt = new JavaTools();
		String rutaDirectorio = System.getProperty("user.dir");
		jt.deleteFile(rutaDirectorio + "\\log\\application.html");
		jt.deleteFile(rutaDirectorio + "\\log\\testlog.log");
		jt.deleteFile(rutaDirectorio + "\\log\\testlog1.log");

		File file = new File(rutaDirectorio + "\\Reports");
		jt.purgeDirectory(file);

		File file2 = new File(rutaDirectorio + "\\Screenshots");
		jt.purgeDirectory(file2);

		jt.deleteFile(rutaDirectorio + "\\estadoEjecucion.txt");

		// EJECUTAR BAT DE LIMPIEZA DE HILOS DE SELENIUM
		try {
			System.out.println("Ejecutando bat, geckodriverkiller");
			Process p = Runtime.getRuntime().exec(rutaDirectorio + "\\geckodriverkiller.bat");
			p.waitFor();
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}

		// EDITAR CONSULTAR TERCERO
		EditarConsultarTerceroRegresion newEditarConsultarTerceroRegresion = new EditarConsultarTerceroRegresion();

		try {
			newEditarConsultarTerceroRegresion.testConsultarTerceroRegresion();
		} catch (Exception e) {
			e.printStackTrace();
		}
		newEditarConsultarTerceroRegresion.endTest();

		System.out.println("De vuelta en el main, EditarConsultarTerceroRegresion");

		// COTIZANDO
		CotizacionVidaInversionRegresion newCotizacion = new CotizacionVidaInversionRegresion();

		try {
			newCotizacion.testCotizacion();
		} catch (Exception e) {
			e.printStackTrace();
		}
		newCotizacion.endTest();

		System.out.println("De vuelta en el main, emitiendo");

		// EMITIENDO
		EmisionTestRegresion newEmision = new EmisionTestRegresion();

		try {
			newEmision.testEmision();
		} catch (Exception e) {
			e.printStackTrace();

			newEmision.endTest();
		}

		// EJECUTAR BAT DE LIMPIEZA DE HILOS DE SELENIUM
		try {
			System.out.println("Ejecutando bat, geckodriverkiller");
			Process p = Runtime.getRuntime().exec(rutaDirectorio + "\\geckodriverkiller.bat");
			p.waitFor();
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}

		// PAGO CAJA
		PagoCajaRegresion newPagoCajaRegresion = new PagoCajaRegresion();

		try {
			newPagoCajaRegresion.testCaja();
		} catch (Exception e) {

			e.printStackTrace();
		}
		newPagoCajaRegresion.endTest();

		// EJECUTAR BAT DE LIMPIEZA DE HILOS DE SELENIUM
		try {
			System.out.println("Ejecutando bat, geckodriverkiller");
			Process p = Runtime.getRuntime().exec(rutaDirectorio + "\\geckodriverkiller.bat");
			p.waitFor();
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}

		System.out.println("De vuelta en el main, caja");

		// ENDOSANDO
		EndosoRegresion newEndoso = new EndosoRegresion();

		try {
			newEndoso.testEndoso();
		} catch (Exception e) {
			e.printStackTrace();
		}
		newEndoso.endTest();

		System.out.println("De vuelta en el main, endoso");

		// EJECUCION DE JOBS
		EjecucionDeJobsRegresion newJobsRegresion = new EjecucionDeJobsRegresion();

		try {
			newJobsRegresion.testJobs();
		} catch (Exception e) {
			e.printStackTrace();
		}
		newJobsRegresion.endTest();

		// EJECUTAR BAT DE LIMPIEZA DE HILOS DE SELENIUM
		try {
			System.out.println("Ejecutando bat, geckodriverkiller");
			Process p = Runtime.getRuntime().exec(rutaDirectorio + "\\geckodriverkiller.bat");
			p.waitFor();
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}

		// EJECUCION DE REPORTES
		ReportesRegresion newReportesRegresion = new ReportesRegresion();

		try {
			newReportesRegresion.testReportes();

		} catch (Exception e) {
			e.printStackTrace();
		}
		newReportesRegresion.endTest();

		// REVISION PRODUCT TOOL
		ProductToolRegresion newProductToolRegresion = new ProductToolRegresion();

		try {
			newProductToolRegresion.testProductTool();

		} catch (Exception e) {
			e.printStackTrace();
		}
		newProductToolRegresion.endTest();

		// ENVIANDO CORREO DE LOG
		Mail newMail = new Mail();
		try {
			newMail.sendMail(null);
		} catch (MessagingException | GeneralSecurityException e) {

			e.printStackTrace();
		}

		// EJECUTAR BAT DE LIMPIEZA DE HILOS DE SELENIUM
		try {
			System.out.println("Ejecutando bat, geckodriverkiller");
			Process p = Runtime.getRuntime().exec(rutaDirectorio + "\\geckodriverkiller.bat");
			p.waitFor();
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}

	}

}