package mainRegresion;
import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;

import javax.mail.MessagingException;

import testRegresion.CotizacionVidaInversionRegresion;
import testRegresion.EmisionTestRegresion;
import utils.JavaTools;
import utils.Mail;

public class MainCotizacionMasivaVidaInversion {

	public static void main(String[] args) throws IOException {
		
		//BORRADO DE LOG
		JavaTools jt = new JavaTools();
		String rutaDirectorio=System.getProperty("user.dir");
		jt.deleteFile(rutaDirectorio+"\\log\\application.html");
		jt.deleteFile(rutaDirectorio+"\\log\\testlog.log");
		jt.deleteFile(rutaDirectorio+"\\log\\testlog1.log");
		
		File file = new File(rutaDirectorio+"\\Reports");
		jt.purgeDirectory(file);
		
		File file2 = new File(rutaDirectorio+"\\Screenshots");
		jt.purgeDirectory(file2);
		
		jt.deleteFile(rutaDirectorio+"\\estadoEjecucion.txt");
		
		
		try {
			System.out.println("Ejecutando bat, geckodriverkiller");
	    	Process p = Runtime.getRuntime().exec(rutaDirectorio+"\\geckodriverkiller.bat");
			p.waitFor();
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		
		//COTIZANDO
		CotizacionVidaInversionRegresion newCotizacion = new CotizacionVidaInversionRegresion();

		try {
			newCotizacion.testCotizacion();	
		} catch (Exception e) {
			e.printStackTrace();	
		}
		newCotizacion.endTest();
				
		System.out.println("De vuelta en el main, emitiendo");
		
		
		try {
			System.out.println("Ejecutando bat, geckodriverkiller");
	    	Process p = Runtime.getRuntime().exec(rutaDirectorio+"\\geckodriverkiller.bat");
			p.waitFor();
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		
		
		try {
			System.out.println("Ejecutando bat, geckodriverkiller");
	    	Process p = Runtime.getRuntime().exec(rutaDirectorio+"\\geckodriverkiller.bat");
			p.waitFor();
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		
		//EMITIENDO
		EmisionTestRegresion newEmision = new EmisionTestRegresion();
			
		try {
			newEmision.testEmision();
			
		} catch (Exception e) {
			e.printStackTrace();

		newEmision.endTest();
		}
		
		
		try {
			System.out.println("Ejecutando bat, geckodriverkiller");
	    	Process p = Runtime.getRuntime().exec(rutaDirectorio+"\\geckodriverkiller.bat");
			p.waitFor();
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		
		//ENVIANDO CORREO DE LOG
		Mail newMail= new Mail();
		try {
			newMail.sendMail(null);
		} catch (MessagingException | GeneralSecurityException e) {
			e.printStackTrace();
		}

}
	
}