package test;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

public class IniciarDriver {
	
	
	private WebDriver webDriver;

	public IniciarDriver(WebDriver webDriver) {
		super();
		this.setWebDriver(webDriver);
	}

	public WebDriver iniciarDriver(String PaginaWeb, String driverPath, int delay) {
		WebDriver firefoxDriver;
		System.out.println("encontrar exe geckodriver \n");
		
		String OS = System.getProperty("os.name").toLowerCase();
		
		if (OS.contains("win")){
            System.out.println("This is Windows based system adding .exe");
            driverPath=driverPath+".exe";
		}
		
		System.setProperty("webdriver.gecko.driver", driverPath);
		System.setProperty("Pagina Web", PaginaWeb);
		FirefoxOptions options = new FirefoxOptions();
		System.out.println("creamos nuestro driver \n");
		firefoxDriver = new FirefoxDriver(options);
		firefoxDriver.manage().timeouts().implicitlyWait(delay, TimeUnit.SECONDS);
		setWebDriver(firefoxDriver);
		return firefoxDriver;
	}

	public WebDriver getWebDriver() {
		return webDriver;
	}

	public void setWebDriver(WebDriver webDriver) {
		this.webDriver = webDriver;
	}
	

}
