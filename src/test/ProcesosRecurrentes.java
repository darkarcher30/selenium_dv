package test;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import utils.SeleniumTools;

public class ProcesosRecurrentes {

	SeleniumTools st = new SeleniumTools();
	
	// verificando si no hay errores de validacion
	public void verificandoErroresValidacion(WebDriver driver) {
		boolean Error = driver.getPageSource().contains("La p�liza no fue aplicada debido a \n");
		if (Error == true) {
			System.out.print("Con errores procediendo a ignorar validaci�n \n");

			driver.findElement(By.name("ErrorDialog:content:questionForm:check")).click();
			st.waitSearchWicketDisapear(driver, "bigLayer", 300);
			driver.findElement(By.name("ErrorDialog:content:questionForm:ignoreValidationButton")).click();
			st.waitSearchWicket(driver);
		} else {
			System.out.print("Sin errores continuando a aplicar poliza \n");
		}
	}
	
	public void iniciarSesion(WebDriver driver, String nombrePrueba, String url, String usuario, String clave,
			String instania, String lenguaje) {

		try {

			WebElement user = driver.findElement(By.name("SecurityLogin"));
			WebElement password = driver.findElement(By.name("SecurityPassword"));

			Select language = new Select(driver.findElement(By.name("PARAMETER_LOCALE_NAME")));
			WebElement button_sumit2 = driver.findElement(By.name("SecuritySubmit"));

			user.sendKeys(usuario); /* * Usuario **/
			password.sendKeys(clave);

			String ultimo = url.substring(url.length() - 12);

			if (ultimo.equals("WController/")) {
				Select instance = new Select(driver.findElement(By.name("USER_PREFERENCE_COUNTRY_NAME")));
				instance.selectByVisibleText(instania);
			}

			language.selectByValue(lenguaje);
			;

			st.setInstaciaG(instania);

			button_sumit2.click();
			validandoSesion(driver, url);

		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	public void validandoSesion(WebDriver driver, String url) {

		try {

			System.out.println("Verificando si ya estaba logeado el usuario \n");
			if (driver.getTitle().equals("Acsel-e Workflow")) {
				System.out.println("No estaba logeado");
				System.out.println("Ahora esta logeado");
				new WebDriverWait(driver, 5).until(ExpectedConditions.titleContains("Acsel-e Workflow"));
			} else if (driver.getTitle().equals("Acsel-e Security")) {
				System.out.println("No estaba logeado");
				System.out.println("Ahora esta logeado");
				new WebDriverWait(driver, 5).until(ExpectedConditions.titleContains("Acsel-e Security"));
			} else if (driver.findElement(By.className("CeldaEncabezado")).isDisplayed()
					&& (driver.findElement(By.className("CeldaEncabezado")).getText()).equals("Advertencia!")) {
				System.out.println("Ya estaba logeado");
				System.out.println("Cerrando la otra sesion");

				WebElement btnAceptar = driver.findElement(By.name("SecuritySubmit"));
				btnAceptar.click();
			}
			System.out.println("Ahora esta logueado \n");

			String ultimo = url.substring(url.length() - 12);

			if (ultimo.equals("WController/")) {
				new WebDriverWait(driver, 5).until(ExpectedConditions.titleContains("Acsel-e Workflow"));
			}
			if (ultimo.equals("security/")) {
				new WebDriverWait(driver, 5).until(ExpectedConditions.titleContains("Acsel-e Security"));
			}

		} catch (Exception e) {
			e.printStackTrace();

		}
	}
	
	public void menu2Capas(String primera, String reposo, String segunda, WebDriver driver) {

		Actions builder = new Actions(driver);
		WebElement element = driver.findElement(By.id(primera));
		builder.moveToElement(element).build().perform();
		builder.moveToElement(driver.findElement(By.id(reposo))).build().perform();

		String defaultWindow = driver.getWindowHandle();
		System.out.println("Default window name is- " + defaultWindow);

		driver.findElement(By.id(segunda)).click();

		System.out.print("Ingreso a menu con exito \n");
	}

	
	public void menu3Capas3( String primera, String segunda, String tercera,String cuarta, WebDriver driver) throws InterruptedException {
		
		Actions builder = new Actions(driver);
		Thread.sleep(1000);
		builder.moveToElement(driver.findElement(By.id(primera))).build().perform();
		builder.moveToElement(driver.findElement(By.id(primera))).click().perform();
		Thread.sleep(1000);
		builder.moveToElement(driver.findElement(By.id(segunda))).build().perform();
		builder.moveToElement(driver.findElement(By.id(segunda))).click().perform();
		Thread.sleep(1000);
		builder.moveToElement(driver.findElement(By.id(tercera))).build().perform();
		builder.moveToElement(driver.findElement(By.id(tercera))).click().perform();
		Thread.sleep(1000);
		
		String defaultWindow = driver.getWindowHandle();
		System.out.println("Default window name is- " + defaultWindow);
		
		driver.findElement(By.id(cuarta)).click();

		System.out.print("Ingreso a menu con exito \n");
	}
	
	public void asociandoContratanteCotizacion(String nombreContratante, WebDriver Driver,int tiempoThrowWait, int tiempoDivWait,Logger logger, String tipoOpenItem) throws InterruptedException {
		// ASOCIANDO CONTRATANTE
				String contratanteName = nombreContratante;// nombre del contratante
				st.waitSearchWicketDisapear(Driver, "bigLayer", tiempoDivWait);
				st.visibilityOfElementLocated(Driver, 200, "Contratante_POL_toField");
				Driver.findElement(By.id("Contratante_POL_toField")).sendKeys(contratanteName);

				Thread.sleep(tiempoThrowWait);
				Driver.findElement(By.id("Contratante_POL_toField")).sendKeys(Keys.DOWN);
				Thread.sleep(200);
				Driver.findElement(By.id("Contratante_POL_toField")).sendKeys(Keys.DOWN);
				Thread.sleep(200);
				Driver.findElement(By.id("Contratante_POL_toField")).sendKeys(Keys.ENTER);
				logger.info("Asociar nombre del contratante \n");

				st.esperaByName(Driver, 200,
						"addThird:registerFormParticipation:paymentCollectorBranches:tablePaymentCollectorBranch:0:doctype");
				Driver = st.waitSelectPopulatename(Driver, 5,
						"addThird:registerFormParticipation:paymentCollectorBranches:tablePaymentCollectorBranch:0:doctype");
				Select MC = new Select(Driver.findElement(By.name(
						"addThird:registerFormParticipation:paymentCollectorBranches:tablePaymentCollectorBranch:0:doctype")));
				MC.selectByVisibleText(tipoOpenItem);

				Driver = st.waitSelectPopulatename(Driver, 5,
						"addThird:registerFormParticipation:paymentCollectorBranches:tablePaymentCollectorBranch:0:priority.type");
				Select Prioridad = new Select(Driver.findElement(By.name(
						"addThird:registerFormParticipation:paymentCollectorBranches:tablePaymentCollectorBranch:0:priority.type")));
				Prioridad.selectByVisibleText("1");

				Driver.findElement(By.name(
						"addThird:registerFormParticipation:paymentCollectorBranches:tablePaymentCollectorBranch:0:addPaymentModeButton"))
						.click();
				
				logger.info("Salvando datos de contratante ContratanteaddThird_calculateButton\n");
				st.visibilityOfElementLocated(Driver, 20, "ContratanteaddThird_calculateButton");
				st.waitSearchWicketDisapear(Driver, "bigLayer", tiempoDivWait);
				Driver.findElement(By.id("ContratanteaddThird_calculateButton")).click();
		
	}
	
	
	public void asociandoBeneficiarioCotizacion(String nombreBeneficiario, String TBeneficiario ,WebDriver Driver,int tiempoThrowWait, int tiempoDivWait,Logger logger) throws InterruptedException {
		//ASOCIANDO BENEFICIARIO
		logger.info("beneficiario " + nombreBeneficiario + "\n");
		st.waitSearchWicketDisapear(Driver, "bigLayer", tiempoDivWait);
		st.visibilityOfElementLocated(Driver, 200, "Beneficiario_IO_toField");
		Driver.findElement(By.id("Beneficiario_IO_toField")).sendKeys(nombreBeneficiario);

		Thread.sleep(tiempoThrowWait);
		Driver.findElement(By.id("Beneficiario_IO_toField")).sendKeys(Keys.DOWN);
		Thread.sleep(200);
		Driver.findElement(By.id("Beneficiario_IO_toField")).sendKeys(Keys.DOWN);
		Thread.sleep(200);
		Driver.findElement(By.id("Beneficiario_IO_toField")).sendKeys(Keys.ENTER);

		//Medio de cobro
		Thread.sleep(tiempoThrowWait);
		st.waitSearchWicketDisapear(Driver, "bigLayer", tiempoDivWait);
		Select TipoBeneficiario = new Select(Driver.findElement(By.id(
				"ParticipationsBeneficiaries_TipoBeneficiario")));
				//"addThird:registerFormParticipation:DataTemplate:repeaterPanel2:1:fila:repeaterSelect:1:field")));
		TipoBeneficiario.selectByVisibleText(TBeneficiario);

		st.waitSearchWicketDisapear(Driver, "bigLayer", tiempoDivWait);
		Driver.findElement(By.name("addThird:registerFormParticipation:saveButtonParticipation")).click();

		st.waitSearchWicketDisapear(Driver, "bigLayer", tiempoDivWait);
		
	}
	
	
	public void asociandoAseguradoCotizacion(String aseguradoName, String TBeneficiario ,WebDriver Driver,int tiempoThrowWait, int tiempoDivWait,Logger logger) throws InterruptedException {
		
		logger.info("agregando asegurado \n");
		logger.info("asegurado " + aseguradoName + "\n");
		st.waitSearchWicketDisapear(Driver, "bigLayer", tiempoDivWait);
		st.visibilityOfElementLocated(Driver, 200, "Asegurado_IO_toField");
		Driver.findElement(By.id("Asegurado_IO_toField")).sendKeys(aseguradoName);

		Thread.sleep(tiempoThrowWait);
		Driver.findElement(By.id("Asegurado_IO_toField")).sendKeys(Keys.DOWN);
		Thread.sleep(200);
		Driver.findElement(By.id("Asegurado_IO_toField")).sendKeys(Keys.DOWN);
		Thread.sleep(200);
		Driver.findElement(By.id("Asegurado_IO_toField")).sendKeys(Keys.ENTER);
		st.waitSearchWicketDisapear(Driver, "bigLayer", tiempoDivWait);// COMENTARIO TST
		
	}
	
	
	
	public WebDriver seleccionarTerceroByName(WebDriver driver, String thirdName) {

		System.out.println("listFinancialPlans " + thirdName + "\n");
		st.esperaByName(driver, 500,
				"detailSearch:templateContainer:searchForm:templateThird:repeaterPanel2:6:fila:field");
		driver.findElement(
				By.name("detailSearch:templateContainer:searchForm:templateThird:repeaterPanel2:3:fila:field"))
				.sendKeys(thirdName);

		st.waitSearchWicketDisapear(driver, "bigLayer", 500);
		driver.findElement(By.name("detailSearch:templateContainer:searchForm:searchButton")).click();

		st.waitSearchWicket(driver);
		driver.findElement(By.name("detailSearch:showDetailSearchTable:proof:ThirdPartyRadioGroup")).click();

		st.waitSearchWicketDisapear(driver, "bigLayer", 500);
		driver.findElement(By.name("detailSearch:showDetailSearchTable:proof:TableForm:associateButton")).click();

		System.out.println("Tercero seleccionado por busqueda avanzada \n");

		return driver;
	}

	public WebDriver seleccionarTerceroByCURP(WebDriver driver, String thirdName) {

		driver.findElement(
				By.name("detailSearch:templateContainer:searchForm:templateThird:repeaterPanel2:6:fila:field"))
				.sendKeys(thirdName);

		st.waitSearchWicketDisapear(driver, "bigLayer", 500);
		driver.findElement(By.name("detailSearch:templateContainer:searchForm:searchButton")).click();

		st.waitSearchWicket(driver);

		WebElement elemento = driver
				.findElement(By.name("detailSearch:showDetailSearchTable:proof:ThirdPartyRadioGroup"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click()", elemento);

		st.waitSearchWicketDisapear(driver, "bigLayer", 500);
		driver.findElement(By.name("detailSearch:showDetailSearchTable:proof:TableForm:associateButton")).click();

		System.out.println("Tercero seleccionado por busqueda avanzada \n");

		return driver;
	}
	
	
	public String calcularCotizacion(WebDriver driver,int tiempoThrowWait) throws InterruptedException {
		
		// CALCULAR
		driver.findElement(By.name("calculate")).click();
		Thread.sleep(5000);
		st.waitSearchWicket(driver);

		WebElement elemento2 = driver.findElement(By.xpath("//*[@id=\"content_applyPolicy\"]"));
		JavascriptExecutor executor2 = (JavascriptExecutor) driver;
		executor2.executeScript("arguments[0].click()", elemento2);

		st.waitSearchWicket(driver);

		// VERIFICANDO SI NO HAY ERRORES DE VALIDACION
		this.verificandoErroresValidacion(driver);
		st.waitSearchWicket(driver);

		Thread.sleep(tiempoThrowWait);

		String kk = driver.findElement(By.xpath("//span[contains(@class,'tex_red_12_bold')]")).getAttribute("innerHTML");
		
		return kk;
	}
	
	
		public String calcularSuscripcionEmision(WebDriver driver,int tiempoThrowWait, int tiempoDivWait,Logger logger) throws InterruptedException {
		
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			st.waitSearchWicket(driver);
			Thread.sleep(10000);

			driver.findElement(By.name("calculate")).click();

			logger.info("Cotizacion calculada" + "\n");

			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);

			Thread.sleep(5000);
			st.waitVaadin(driver, "waitVaadin", 10);
			driver.findElement(By.name("EventSection:content:CloseForm:ApplyPolicy")).click();

			st.waitSearchWicket(driver);

			// VERIFICANDO SI HAY ERRORES DE VALIDACION	
			boolean Error = driver.getPageSource().contains("La p�liza no fue aplicada debido a");
			if (Error == true) {
				System.out.print("Con errores procediendo a ignorar validaci�n \n");

				driver.findElement(By.name("ErrorDialog:content:questionForm:check")).click();
				st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
				driver.findElement(By.name("ErrorDialog:content:questionForm:ignoreValidationButton")).click();
				st.waitSearchWicket(driver);
			} else {
				System.out.print("Sin errores continuando a aplicar poliza \n");
			}

			st.waitSearchWicket(driver);
			Thread.sleep(4000);

			WebElement TxtBoxContent = driver.findElement(By.xpath(
					"/html[1]/body[1]/div[6]/form[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/span[2]"));
			String nro = TxtBoxContent.getAttribute("innerHTML");
		
		return nro;
	}
	
		
		public void calcularEndoso(WebDriver driver,int tiempoThrowWait, Logger logger, int tiempoDivWait) throws InterruptedException {
	  
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			st.esperaNameClickable(driver, 5, "calculateButton");
			
			logger.info("calculateButton \n");
			Thread.sleep(6000);
			
			String xpathCalculateBtn2="/html[1]/body[1]/div[1]/div[2]/div[4]/div[1]/div[2]/div[1]/form[1]/input[1]";
			WebElement calcular2 = driver.findElement(By.xpath(xpathCalculateBtn2));
			JavascriptExecutor execut2 = (JavascriptExecutor) driver;
			execut2.executeScript("arguments[0].click()", calcular2);
			
			Thread.sleep(4000);

			st.esperaIdClickable(driver, 10, "content_applyPolicy");
			WebElement elemento22 = driver.findElement(By.id("content_applyPolicy"));
			JavascriptExecutor executor22 = (JavascriptExecutor) driver;
			executor22.executeScript("arguments[0].click()", elemento22);
			Thread.sleep(4000);
			st.waitSearchWicket(driver);
			
		}
		
		
		public void endosar(WebDriver driver,int tiempoThrowWait, Logger logger, int tiempoDivWait,String nroPoliza ,String tipoEndoso,String tipoSolicitud) throws InterruptedException {
			//NRO DE POLIZA
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			String NroPoliza = nroPoliza;// Fecha Nacimiento
			logger.info("Nro Poliza a endosar " + NroPoliza + "\n");
			driver.findElement(By.id("ide")).sendKeys(NroPoliza);
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			
			//BUSCAR NRO POLIZA
			driver.findElement(By.name("searchButton")).click();
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			logger.info("Buscando Poliza" + "\n");
			st.waitSearchWicket(driver);
			
			//SELECCIONANDO POLIZA
			driver.findElement(By.name("ConsultPolicy:ResultSearchSimplePolicy:groupPolicies")).click();
			logger.info("Seleccionando Poliza" + "\n");
			
			//SELECCIONANDO BTN ENDOSAR
			driver.findElement(By.name("container:EndorsePolicyButton")).click();
			logger.info("Seleccionando btnEndosar" + "\n");
			st.waitSearchWicket(driver);
			
			//SELECCIONANDO BTN MODIFICAR
			driver.findElement(By.name("formEdit:EditSearch")).click();
			logger.info("Seleccionando btnModificar" + "\n");
			st.waitSearchWicket(driver);
			
			//INGRESAR SOLICITUD
			logger.info("Ingresar solicitud \n");
			st.waitSelectPopulateid(driver, 25,"event");
			Select is = new Select(driver.findElement(By.id("event")));
			is.selectByVisibleText(tipoSolicitud);
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			
			//TIPO DE SOLICITUD
			logger.info("Tipo de solicitud: endosar \n");
			st.waitSelectPopulateid(driver, 15,"EventTemplateSection_TipoSolicitud");
			Select ts = new Select(driver.findElement(By.id("EventTemplateSection_TipoSolicitud")));
			ts.selectByVisibleText("Endosar");
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
			
			//TIPO DE ENDOSO
			logger.info("Tipo de endoso: "+tipoEndoso+"\n");
			st.waitSelectPopulateid(driver, 15,"EventTemplateSection_TipoEndoso");
			Select te = new Select(driver.findElement(By.id("EventTemplateSection_TipoEndoso")));
			te.selectByVisibleText(tipoEndoso);
			st.waitSearchWicketDisapear(driver, "bigLayer", tiempoDivWait);
		}
}

	
	
	
	


