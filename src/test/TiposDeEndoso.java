package test;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import utils.SeleniumTools;

public class TiposDeEndoso {

	SeleniumTools st = new SeleniumTools();
	ProcesosRecurrentes procesosRecurrentes = new ProcesosRecurrentes();
	
	
	public void cambioBeneficiario(WebDriver FirefoxDriver,Logger logger, String beneficiarioName, String TBeneficiario, int tiempoThrowWait, int tiempoDivWait) throws InterruptedException {
		
		//SELECCIONANDO CAMBIO EN BENEFICIARIO
		FirefoxDriver.findElement(By.xpath("/html/body/div[6]/form/div/div[2]/div/div/div/div[2]/div/div/div/div/div[2]/div[2]/div[1]/div[1]/table/tbody/tr/td[1]/div[4]/div/div/div[4]/div[2]/div/input")).click();
		logger.info("seleccionando Cambio en Beneficiario \n");
		st.waitSearchWicket(FirefoxDriver);
		
		//CONTINUAR ENDOSO
		FirefoxDriver.findElement(By.name("EventSection:content:Form:continueButton")).click();
		logger.info("seleccionando Continuar \n");
		st.waitSearchWicket(FirefoxDriver);
		
		//PASOS PARA LLEGAR AL BENEFICIARIO		
		//SELECCIONANDO UR
		FirefoxDriver.findElement(By.name("container:groupRiskUnit")).click();
		logger.info("Seleccionando UR \n");
		st.waitSearchWicket(FirefoxDriver);
				
		//EDITANDO UR
		FirefoxDriver.findElement(By.name("formUnitRiskSimpleSearch:EditButtonRiskSearch")).click();
		logger.info("Editar Unidad de riesgo \n");
		st.waitSearchWicket(FirefoxDriver);
		
		//GUARDAR UR
		FirefoxDriver.findElement(By.name("saveButtonRU")).click();
		logger.info("Click en objeto asegurado UR \n");
		st.waitSearchWicket(FirefoxDriver);
						
		//SELECCIONANDO OA
		Thread.sleep(2000);
		String oaxpath = "span.waitActionJQ";
		FirefoxDriver.findElement(By.cssSelector(oaxpath)).click();
		logger.info("Seleccionando OA \n");
		st.waitSearchWicket(FirefoxDriver);
		
		//SELECCIONANDO BENEFICIARIO				
		FirefoxDriver.findElement(By.xpath("//*[contains(@id, 'Beneficiario_check')]")).click();
		logger.info("Seleccionando Beneficiario \n");
		st.waitSearchWicket(FirefoxDriver);
						
		//ELIMINANDO BENEFICIARIO
		FirefoxDriver.findElement(By.id("Beneficiario_IO_deleteButton")).click();
		logger.info("Modificar UR \n");
		st.waitSearchWicket(FirefoxDriver);
		
		//HACIENDO CLICK SI EN CONFIRMAR
		FirefoxDriver.findElement(By.name("modalDialogSecurity:content:questionForm:yesOption")).click();
		logger.info("Haciendo click en SI \n");
		st.waitSearchWicket(FirefoxDriver);
		

		// ASOCIANDO BENEFICIARIO
		logger.info("Asociando beneficiario: "+ beneficiarioName +"\n");
		logger.info("Tipo de beneficiario: "+ TBeneficiario +"\n");
		
		procesosRecurrentes.asociandoBeneficiarioCotizacion(beneficiarioName, TBeneficiario, FirefoxDriver, tiempoThrowWait, tiempoDivWait, logger);
		Thread.sleep(5000);
		st.waitSearchWicket(FirefoxDriver);
		
		
	}
	
	
	public void aporteExtraordinario(WebDriver FirefoxDriver,Logger logger, String monto, int tiempoThrowWait, int tiempoDivWait) throws InterruptedException {
	
		logger.info("Procediendo a endoso de aporte extraordinario \n");
		
		//SELECCIONANDO APORTE EXTRAORDINARIO
		FirefoxDriver.findElement(By.xpath("/html/body/div[6]/form/div/div[2]/div/div/div/div[2]/div/div/div/div/div[2]/div[2]/div[1]/div[1]/table/tbody/tr/td[1]/div[4]/div/div/div[4]/div[8]/div/input")).click();
		logger.info("seleccionando Cambio en Beneficiario \n");
		st.waitSearchWicket(FirefoxDriver);
		
		//CONTINUAR ENDOSO
		FirefoxDriver.findElement(By.name("EventSection:content:Form:continueButton")).click();
		logger.info("seleccionando Continuar \n");
		st.waitSearchWicket(FirefoxDriver);
		
		//SELECCIONANDO UR
		FirefoxDriver.findElement(By.name("container:groupRiskUnit")).click();
		logger.info("Seleccionando UR \n");
		st.waitSearchWicket(FirefoxDriver);
				
		//EDITANDO UR
		FirefoxDriver.findElement(By.name("formUnitRiskSimpleSearch:EditButtonRiskSearch")).click();
		logger.info("Editar Unidad de riesgo \n");
		st.waitSearchWicket(FirefoxDriver);
		
		//GUARDAR UR
		FirefoxDriver.findElement(By.name("saveButtonRU")).click();
		logger.info("Click en objeto asegurado UR \n");
		st.waitSearchWicket(FirefoxDriver);
						
		//SELECCIONANDO OA
		Thread.sleep(2000);
		String oaxpath = "span.waitActionJQ";
		FirefoxDriver.findElement(By.cssSelector(oaxpath)).click();
		logger.info("Seleccionando OA \n");
		st.waitSearchWicket(FirefoxDriver);
	
		//APORTE EXTRAORDINARIO
		st.waitSearchWicketDisapear(FirefoxDriver, "bigLayer", tiempoDivWait);
		logger.info("Agregando Aportacion inicial en el campo OAVidaInversion_TO_MontoAporte:"+monto+"\n");
		FirefoxDriver
		.findElement(By.id("OAVidaInversion_TO_MontoAporte")).clear();
		st.waitSearchWicketDisapear(FirefoxDriver, "bigLayer", tiempoDivWait);
		FirefoxDriver
		.findElement(By.id("OAVidaInversion_TO_MontoAporte"))
		.sendKeys(monto);
	
	
	}
	
	
	
}

	
	
	
	


