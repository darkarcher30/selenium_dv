package test;
import java.io.BufferedReader;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import utils.JavaTools;
import utils.SeleniumTools;

public class Login {

	JavaTools jt = new JavaTools();
	SeleniumTools st = new SeleniumTools();
	IniciarDriver iniciarDriver;
	ProcesosRecurrentes procesosRecurrentes = new ProcesosRecurrentes();
	TiposDeEndoso tiposDeEndoso = new TiposDeEndoso();
	private WebDriver driver;

	String rutaDirectorio = System.getProperty("user.dir");
	BufferedReader bufer = jt.abrirLectura(rutaDirectorio, "ParametrosAmbiente.txt");
	String lineaParametrosAmbiente = jt.lecturaLinea(bufer);
	String separador1[] = lineaParametrosAmbiente.split("=");
	int tiempoDelay = Integer.parseInt(separador1[0]);// tiempo de delay de automatizacion
	int tiempoDivWait = Integer.parseInt(separador1[1]);// tiempo wait Big Layer
	int tiempoThrowWait = Integer.parseInt(separador1[2]);// tiempo wait Big Layer
	String PaginaWeb = separador1[3];// Direccion http de la prueba
	public String nroCotSeparado = "vacio";
	

	@Before
	public void startBrowser() {

	}

	@Test
	public void testLogin() throws InterruptedException, IOException {

		Logger logger = Logger.getLogger("testCreacionPersonaFisica");
		PropertyConfigurator.configure("Log4j.properties");

		String SecurityLogin = separador1[4];// login
		String SecurityPassword = separador1[5];// password
		String USER_PREFERENCE_COUNTRY_NAME = separador1[6];// USER_PREFERENCE_COUNTRY_NAME
		String PARAMETER_LOCALE_NAME = separador1[7];// PARAMETER_LOCALE_NAME

		logger.info("SecurityLogin: " + SecurityLogin + " \n");
		logger.info("SecurityPassword: " + SecurityPassword + " \n");
		logger.info("USER_PREFERENCE_COUNTRY_NAME: " + USER_PREFERENCE_COUNTRY_NAME + " \n");
		logger.info("PARAMETER_LOCALE_NAME: " + PARAMETER_LOCALE_NAME + " \n");
		jt.cerrarLectura(bufer);


		logger.info("Parametros \n");
		logger.info("driverPath: " + rutaDirectorio + " \n");
		logger.info("PaginaWeb: " + PaginaWeb + " \n");
		logger.info("******************* \n");
		logger.info("Open Browser and navigate to designed URL \n");
		driver=st.iniciarDriver(PaginaWeb, rutaDirectorio + "/geckodriver", tiempoDelay);
		driver.get(PaginaWeb);
		driver.manage().window();
		driver.manage().window().maximize();
		logger.info("tiempo de inicio de test " + java.time.LocalDateTime.now());

		procesosRecurrentes.iniciarSesion(driver, "cotizacion", PaginaWeb, SecurityLogin, SecurityPassword,
				USER_PREFERENCE_COUNTRY_NAME, PARAMETER_LOCALE_NAME);

		procesosRecurrentes.validandoSesion(driver, PaginaWeb);

	}

	public WebDriver getDriver() {
		return driver;
	}



	@After

	public void endTest() {
	
		Logger logger = Logger.getLogger("testCreacionPersonaFisica");
		PropertyConfigurator.configure("Log4j.properties");
		logger.info("login exitoso \n");
		logger.info("tiempo de fin de login " + java.time.LocalDateTime.now());
		logger.info("Finalizando login");

	}

}