package test;
import java.io.BufferedReader;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import utils.JavaTools;
import utils.SeleniumTools;


public class CotizacionVidaEntera {

	JavaTools jt = new JavaTools();
	SeleniumTools st = new SeleniumTools();
	ProcesosRecurrentes procesosRecurrentes = new ProcesosRecurrentes();

	String rutaDirectorio=System.getProperty("user.dir");
	BufferedReader bufer = jt.abrirLectura(rutaDirectorio,"ParametrosAmbiente.txt");
	String lineaParametrosAmbiente = jt.lecturaLinea(bufer);
	String separador1[] = lineaParametrosAmbiente.split("=");
	int tiempoDelay = Integer.parseInt(separador1[0]);// tiempo de delay de automatizacion
	int tiempoDivWait = Integer.parseInt(separador1[1]);// tiempo wait Big Layer
	int tiempoThrowWait = Integer.parseInt(separador1[2]);// tiempo wait Big Layer
	//String driverPath = separador1[3];// Direccion del driver
	String PaginaWeb = separador1[3];// Direccion http de la prueba
	public WebDriver FirefoxDriver;
	
	public String nroCotSeparado = "vacio";
	
	BufferedReader bufer2 = jt.abrirLectura(rutaDirectorio,"ParametrosCotizacionVidaEntera.txt");

	@Before
	public void startBrowser() {

	}

	@Test
	public void testCotizacion() throws InterruptedException {
		
		Logger logger=Logger.getLogger("testCotizacion");
		PropertyConfigurator.configure("Log4j.properties");

		String SecurityLogin = separador1[4];// login
		String SecurityPassword = separador1[5];// password
		String USER_PREFERENCE_COUNTRY_NAME = separador1[6];// USER_PREFERENCE_COUNTRY_NAME
		String PARAMETER_LOCALE_NAME = separador1[7];// PARAMETER_LOCALE_NAME
		logger.info("Directorio actual: " + rutaDirectorio + "\n");
		logger.info("SecurityLogin: " + SecurityLogin + "\n");
		logger.info("SecurityPassword: " + SecurityPassword + "\n");
		logger.info("USER_PREFERENCE_COUNTRY_NAME: " + USER_PREFERENCE_COUNTRY_NAME + "\n");
		logger.info("PARAMETER_LOCALE_NAME: " + PARAMETER_LOCALE_NAME + "\n");
		
		String ruta = rutaDirectorio + "/nrosCotizacionVidaEntera" + ".txt";
		jt.cerrarLectura(bufer);
		jt.crearTxt(ruta);

		String linea = null;
		while ((linea = jt.lecturaLinea(bufer2)) != null) {

			FirefoxDriver = st.iniciarDriver(PaginaWeb, rutaDirectorio+"/geckodriver", tiempoDelay);

			logger.info("Parametros \n");
			logger.info("driverPath: " + rutaDirectorio + "\n");
			logger.info("PaginaWeb: " + PaginaWeb + "\n");
			logger.info("******************* \n");
			logger.info("Open Browser and navigate to designed URL \n");
			FirefoxDriver.get(PaginaWeb);
			FirefoxDriver.manage().window();
			FirefoxDriver.manage().window().maximize();
			logger.info("tiempo de inicio de test " + java.time.LocalDateTime.now());

			String separador2[] = linea.split("=");

			procesosRecurrentes.iniciarSesion(FirefoxDriver, "cotizacion", PaginaWeb, SecurityLogin, SecurityPassword,
					USER_PREFERENCE_COUNTRY_NAME, PARAMETER_LOCALE_NAME);

			procesosRecurrentes.validandoSesion(FirefoxDriver, PaginaWeb);

			System.out.print("Login exitoso \n");

			procesosRecurrentes.menu2Capas("Menu2", "Menu2_2", "Menu2_2_1", FirefoxDriver);// metodo que abre menu de acsele de dos capas

			st.cambiarVentana(FirefoxDriver);

			st.esperaByName(FirefoxDriver, 60, "productsComboBox");
			Select Product = new Select(FirefoxDriver.findElement(By.name("productsComboBox")));
			Product.selectByValue("VidaEntera");

			FirefoxDriver = st.waitSelectPopulatename(FirefoxDriver, 5, "validitiesComboBox");
			Select Vigencia = new Select(FirefoxDriver.findElement(By.name("validitiesComboBox")));
			Vigencia.selectByValue("-1");

			String hoy = jt.getFechaHoy();// obtiene un String de la fecha de hoy
			logger.info("fecha de hoy: " + hoy);

			FirefoxDriver.findElement(By.name("initialDate")).sendKeys(hoy);
			FirefoxDriver.findElement(By.name("CreateQuoteButton")).click();

			st.esperaByName(FirefoxDriver, 300, "EventSection:content:Form:continueButton");
			st.waitSearchWicketDisapear(FirefoxDriver, "bigLayer", tiempoDivWait);
			FirefoxDriver.findElement(By.name("EventSection:content:Form:continueButton")).click();
			System.out.print("Entrando en cuerpo de poliza \n");

			st.waitSearchWicketDisapear(FirefoxDriver, "bigLayer", tiempoDivWait);

			String listFinancialPlans = separador2[0];// listFinancialPlans
			logger.info("listFinancialPlans " + listFinancialPlans + "\n");
			Select financialPlan = new Select(FirefoxDriver.findElement(By.name("listFinancialPlans")));
			financialPlan.selectByValue(listFinancialPlans);

			System.out.print("Agregando contratante \n");
			FirefoxDriver.findElement(By.name("calculateButton")).click();
			System.out.print("Primer calcular \n");
			st.waitSearchWicketDisapear(FirefoxDriver, "bigLayer", tiempoDivWait);

			FirefoxDriver.findElement(By.id("Contratante_POL_toField")).sendKeys("");

			// ASOCIANDO CONTRATANTE
			String contratanteName = separador2[1];// nombre del contratante
			FirefoxDriver.findElement(By.id("Contratante_POL_toField")).sendKeys(contratanteName);

			Thread.sleep(tiempoThrowWait);

			FirefoxDriver.findElement(By.id("Contratante_POL_toField")).sendKeys(Keys.DOWN);
			FirefoxDriver.findElement(By.id("Contratante_POL_toField")).sendKeys(Keys.ENTER);

			st.waitSearchWicketDisapear(FirefoxDriver, "bigLayer", tiempoDivWait);
			FirefoxDriver.findElement(By.name("AssociateButton")).click();

			// AEHSBC-1594 - seleccionar prima deposito

			st.esperaByName(FirefoxDriver, 1000,
					"addThird:registerFormParticipation:paymentCollectorBranches:tablePaymentCollectorBranch:0:doctype");
			FirefoxDriver = st.waitSelectPopulatename(FirefoxDriver, 5,
					"addThird:registerFormParticipation:paymentCollectorBranches:tablePaymentCollectorBranch:0:doctype");
			Select MC = new Select(FirefoxDriver.findElement(By.name(
					"addThird:registerFormParticipation:paymentCollectorBranches:tablePaymentCollectorBranch:0:doctype")));
			MC.selectByVisibleText("Prima Emitida");

			FirefoxDriver = st.waitSelectPopulatename(FirefoxDriver, 5,
					"addThird:registerFormParticipation:paymentCollectorBranches:tablePaymentCollectorBranch:0:priority.type");
			Select Prioridad = new Select(FirefoxDriver.findElement(By.name(
					"addThird:registerFormParticipation:paymentCollectorBranches:tablePaymentCollectorBranch:0:priority.type")));
			Prioridad.selectByVisibleText("1");

			FirefoxDriver.findElement(By.name(
					"addThird:registerFormParticipation:paymentCollectorBranches:tablePaymentCollectorBranch:0:addPaymentModeButton"))
					.click();

			st.waitSearchWicketDisapear(FirefoxDriver, "bigLayer", tiempoDivWait);
			FirefoxDriver.findElement(By.name("addThird:registerFormParticipation:saveButtonParticipation")).click();
			
			//AGREGANDO UR
			st.waitSearchWicketDisapear(FirefoxDriver, "bigLayer", tiempoDivWait);
			logger.info("Agregando unidad de riesgo \n");
			FirefoxDriver.findElement(By.name("NewButtonRisk")).click();
			logger.info("Segundo guardar \n");

			st.waitSearchWicketDisapear(FirefoxDriver, "bigLayer", tiempoDivWait);
			FirefoxDriver.findElement(By.name("saveButtonRU")).click();
			logger.info("Información general \n");
			
			//Información general
			
			//SEGMENTO
			st.waitSearchWicketDisapear(FirefoxDriver, "bigLayer", tiempoDivWait);
			Select segmento = new Select(FirefoxDriver.findElement(By.name(
					"templateIO:tabPanel:repeaterTab:2:SubTabsInformation:repeater:2:fila:repeaterSelect:1:field")));
			segmento.selectByVisibleText("ADVANCE");
			
			//FECHA NAC

			st.waitSearchWicketDisapear(FirefoxDriver, "bigLayer", tiempoDivWait);
			String FechaNac = separador2[3];// Fecha Nacimiento
			logger.info("Fecha Nacimiento " + FechaNac + "\n");
			FirefoxDriver
					.findElement(
							By.name("templateIO:tabPanel:repeaterTab:2:SubTabsInformation:repeater:3:fila:fieldDate"))
					.sendKeys(FechaNac);

			// GENERO
			st.waitSearchWicketDisapear(FirefoxDriver, "bigLayer", tiempoDivWait);
			Select genero = new Select(FirefoxDriver.findElement(By.name(
					"templateIO:tabPanel:repeaterTab:2:SubTabsInformation:repeater:4:fila:repeaterSelect:1:field")));
			String generoS = separador2[4];// Genero poliza
			logger.info("Genero Pol " + generoS + "\n");
			genero.selectByVisibleText(generoS);

			// FUMADOR
			st.waitSearchWicketDisapear(FirefoxDriver, "bigLayer", tiempoDivWait);
			Select fumador = new Select(FirefoxDriver.findElement(By.name(
					"templateIO:tabPanel:repeaterTab:2:SubTabsInformation:repeater2:1:fila:repeaterSelect:1:field")));
			String fumadorS = separador2[5];// Fumador
			logger.info("Fumador " + fumadorS + "\n");
			fumador.selectByVisibleText(fumadorS);

			// GENERO
			st.waitSearchWicketDisapear(FirefoxDriver, "bigLayer", tiempoDivWait);
			Select genero1 = new Select(FirefoxDriver.findElement(By.name(
					"templateIO:tabPanel:repeaterTab:2:SubTabsInformation:repeater:4:fila:repeaterSelect:1:field")));
			genero1.selectByVisibleText(generoS);
			Thread.sleep(tiempoThrowWait);
			
			// SUMA ASEGURADA
			String sumaAseg = separador2[6];// Suma Asegurada Propuesta
			Thread.sleep(tiempoThrowWait);
			st.waitSearchWicketDisapear(FirefoxDriver, "bigLayer", tiempoDivWait);
			FirefoxDriver
			.findElement(
					By.name("templateIO:tabPanel:repeaterTab:3:SubTabsInformation:repeater:1:fila:field"))
			.clear();
			Thread.sleep(tiempoThrowWait);

			//SUMA ASEGURADA JAVASCRIPT
			WebElement sAsegField = FirefoxDriver
					.findElement(By.name("templateIO:tabPanel:repeaterTab:3:SubTabsInformation:repeater:1:fila:field"));
			JavascriptExecutor executor = (JavascriptExecutor) FirefoxDriver;
			executor.executeScript(
					"document.getElementById('OAVidaEntera_TO_SumaAsegFallec').value='" + sumaAseg + "';", sAsegField);		
			logger.info("Suma Asegurada " + sumaAseg + "\n");
	
			
			st.waitSearchWicketDisapear(FirefoxDriver, "bigLayer", tiempoDivWait);
			FirefoxDriver
			.findElement(
					By.name("templateIO:tabPanel:repeaterTab:3:SubTabsInformation:repeater2:1:fila:field"))
			.click();
			
			st.waitSearchWicketDisapear(FirefoxDriver, "bigLayer", tiempoDivWait);
			Thread.sleep(tiempoThrowWait);
			
			FirefoxDriver
			.findElement(
					By.name("templateIO:tabPanel:repeaterTab:3:SubTabsInformation:repeater:1:fila:field"))
			.sendKeys(sumaAseg);

			
			
			
			logger.info("Suma Asegurada " + sumaAseg + "\n");
			Thread.sleep(tiempoThrowWait);


			
			// BOTON SALVAR
			st.waitSearchWicketDisapear(FirefoxDriver, "bigLayer", tiempoDivWait);
			FirefoxDriver.findElement(By.name("saveButton")).click();
			logger.info("Tercer guardar \n");

			st.waitSearchWicketDisapear(FirefoxDriver, "bigLayer", tiempoDivWait);

			// ASOCIANDO BENEFICIARIO
			System.out.print("agregando asegurado \n");

			String beneficiarioName = separador2[7];// Nombre predeterminado del asegurado
			logger.info("beneficiario " + beneficiarioName + "\n");

			FirefoxDriver.findElement(By.name("AutoRisk:search")).sendKeys(beneficiarioName);
			Thread.sleep(tiempoThrowWait);
			FirefoxDriver.findElement(By.name("AutoRisk:search")).sendKeys(Keys.DOWN);
			FirefoxDriver.findElement(By.name("AutoRisk:search")).sendKeys(Keys.ENTER);
			Thread.sleep(tiempoThrowWait);
			st.waitSearchWicketDisapear(FirefoxDriver, "bigLayer", tiempoDivWait);
			FirefoxDriver.findElement(By.name("AssociateButton")).click();

			st.waitSearchWicketDisapear(FirefoxDriver, "bigLayer", tiempoDivWait);
			FirefoxDriver.findElement(By.name("addThird:registerFormParticipation:saveButtonParticipation")).click();

			st.waitSearchWicketDisapear(FirefoxDriver, "bigLayer", tiempoDivWait);

			// ASOCIANDO ASEGURADO

			String aseguradoName = separador2[8];// Nombre predeterminado del beneficiario
			logger.info("asegurado " + aseguradoName + "\n");

			FirefoxDriver.findElement(By.id("Asegurado_IO_toField")).sendKeys(aseguradoName);
			Thread.sleep(tiempoThrowWait);
			FirefoxDriver.findElement(By.id("Asegurado_IO_toField")).sendKeys(Keys.DOWN);
			FirefoxDriver.findElement(By.id("Asegurado_IO_toField")).sendKeys(Keys.ENTER);
			Thread.sleep(tiempoThrowWait);
			//st.waitSearchWicketDisapear(FirefoxDriver, "bigLayer", tiempoDivWait);
			//FirefoxDriver.findElement(By.id("BeneficiarioaddThird_calculateButton")).click();

			st.waitSearchWicketDisapear(FirefoxDriver, "bigLayer", tiempoDivWait);
			FirefoxDriver.findElement(By.name("addThird:registerFormParticipation:saveButtonParticipation")).click();

			st.waitSearchWicketDisapear(FirefoxDriver, "bigLayer", tiempoDivWait);

			// CALCULAR
			FirefoxDriver.findElement(By.name("calculate")).click();

			st.waitSearchWicket(FirefoxDriver);

			WebElement elemento2 = FirefoxDriver.findElement(By.xpath("//*[@id=\"content_applyPolicy\"]"));
			JavascriptExecutor executor2 = (JavascriptExecutor) FirefoxDriver;
			executor2.executeScript("arguments[0].click()", elemento2);

			st.waitSearchWicket(FirefoxDriver);

			// VERIFICANDO SI NO HAY ERRORES DE VALIDACION
			procesosRecurrentes.verificandoErroresValidacion(FirefoxDriver);
			st.waitSearchWicket(FirefoxDriver);

			Thread.sleep(tiempoThrowWait);

			String kk = FirefoxDriver.findElement(By.xpath("//span[contains(@class,'tex_red_12_bold')]"))
					.getAttribute("innerHTML");

			String nroCotSeparado = jt.separarNro(kk);
			logger.info("Printing " + nroCotSeparado);
			jt.escribirTxt(ruta, nroCotSeparado);
			// FirefoxDriver.quit();
		}
		jt.escribirTxt(ruta,
				"Remover esta linea y Renombrar este archivo como ParametrosEmision.txt para ejecutar la emision de polizas usando este archivo \n");
		
		logger.info("tiempo de fin de test " + java.time.LocalDateTime.now());
		logger.info("Finalizando cotización");

	}

	public WebDriver getFirefoxDriver() {
		return FirefoxDriver;
	}

	public void setFirefoxDriver(WebDriver firefoxDriver) {
		FirefoxDriver = firefoxDriver;
	}

	public String getNroCotSeparado() {
		return nroCotSeparado;
	}

	public void setNroCotSeparado(String nroCotSeparado) {
		this.nroCotSeparado = nroCotSeparado;
	}

	@After

	public void endTest() {

		jt.cerrarLectura(bufer2);

		// FirefoxDriver.quit();

	}

}