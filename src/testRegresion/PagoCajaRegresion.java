package testRegresion;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import test.IniciarDriver;
import test.ProcesosRecurrentes;
import thread.ThreadPagoCaja;
import utils.JavaTools;
import utils.SeleniumTools;



public class PagoCajaRegresion {

	JavaTools jt = new JavaTools();
	SeleniumTools st = new SeleniumTools();
	IniciarDriver iniciarDriver;
	ProcesosRecurrentes procesosRecurrentes = new ProcesosRecurrentes();
	ThreadPagoCaja threadPagoCaja;
	private WebDriver webDriver;
	
	String rutaDirectorio=System.getProperty("user.dir");
	
	
	BufferedReader bufer = jt.abrirLectura(rutaDirectorio,"ParametrosAmbiente.txt");
	String lineaParametrosAmbiente = jt.lecturaLinea(bufer);
	String separador1[] = lineaParametrosAmbiente.split("=");
	int tiempoDelay = Integer.parseInt(separador1[0]);// tiempo de delay de automatizacion
	int tiempoDivWait = Integer.parseInt(separador1[1]);// tiempo wait Big Layer
	int tiempoThrowWait = Integer.parseInt(separador1[2]);// tiempo de los wait
	//String driverPath = separador1[3];// Direccion del driver
	String PaginaWeb = separador1[3];// Direccion http de la prueba
	//public WebDriver FirefoxDriver;
	public String nroCotSeparado = "vacio";
	BufferedReader bufer2 = jt.abrirLectura(rutaDirectorio,"ParametrosCaja.txt");
	
	BufferedReader bufer3 = jt.abrirLectura(rutaDirectorio,"PolizasCreadas.txt");

	@Before
	public void startBrowser() {

	}

	@Test
	public void testCaja() throws InterruptedException, IOException{
		
			Logger logger=Logger.getLogger("testPagoCaja");
			PropertyConfigurator.configure("Log4j.properties");
	
			jt.cerrarLectura(bufer);
		
			String lineaNrosPoliza = null;
			String linea = null;

			ArrayList<Thread> arrThreads = new ArrayList<>();
			int maxSimultaneous=0;
			
			
			linea = jt.lecturaLinea(bufer2);
			while (linea!=null) {
			while (maxSimultaneous<3) {
			
					
					if (linea==null) {
						break;
						}
					lineaNrosPoliza=jt.lecturaLinea(bufer3);
					
					Thread T1 = new Thread(new ThreadPagoCaja(linea,lineaNrosPoliza));         
					T1.start();
					arrThreads.add(T1);	
					maxSimultaneous=maxSimultaneous+1;	
					linea = jt.lecturaLinea(bufer2);
				
				}
			
			
				for (int i = 0; i < arrThreads.size(); i++) 
				{
					arrThreads.get(i).join(); 
				}
				
				arrThreads.clear();
				maxSimultaneous=0;
			}
			
			logger.info("Saliendo del while de pago caja");

			logger.info("tiempo de fin de test " + java.time.LocalDateTime.now());
			logger.info("Finalizando pago caja");

	}

	public WebDriver getFirefoxDriver() {
		return webDriver;
	}

	public void setFirefoxDriver(WebDriver firefoxDriver) {
		this.webDriver = firefoxDriver;
	}

	@After
	public void endTest() {
		jt.cerrarLectura(bufer);
		jt.cerrarLectura(bufer2);
		jt.cerrarLectura(bufer3);

	}

}