package testRegresion;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import test.ProcesosRecurrentes;
import thread.ThreadEmision;
import utils.JavaTools;
import utils.SeleniumTools;

public class EmisionTestRegresion {

	JavaTools jt = new JavaTools();
	SeleniumTools st = new SeleniumTools();
	ThreadEmision threadEmision;
	ProcesosRecurrentes procesosRecurrentes = new ProcesosRecurrentes();

	String rutaDirectorio = System.getProperty("user.dir");
	BufferedReader bufer = jt.abrirLectura(rutaDirectorio, "ParametrosAmbiente.txt");
	String lineaParametrosAmbiente = jt.lecturaLinea(bufer);
	String separador1[] = lineaParametrosAmbiente.split("=");
	int tiempoDelay = Integer.parseInt(separador1[0]);// tiempo de delay de automatizacion
	int tiempoDivWait = Integer.parseInt(separador1[1]);// tiempo wait Big Layer
	int tiempoThrowWait = Integer.parseInt(separador1[2]);// tiempo wait Big Layer
	String PaginaWeb = separador1[3];// Direccion http de la prueba
	public String nroCotSeparado = "vacio";
	BufferedReader bufer2 = jt.abrirLectura(rutaDirectorio, "ParametrosEmision.txt");
	


	@Before
	public void continueBrowser() {

	}

	@Test
	public void testEmision()  throws InterruptedException, IOException{
		
			Logger logger=Logger.getLogger("testEmision");
			PropertyConfigurator.configure("Log4j.properties");
		

			String rutaPolizasCreadas = rutaDirectorio + "/PolizasCreadas.txt";
			jt.crearTxt(rutaPolizasCreadas);

			String linea = null;

			int maxSimultaneous=0;
			ArrayList<Thread> arrThreads = new ArrayList<>();
			
			linea = jt.lecturaLinea(bufer2);
			while (linea!=null) {
			while (maxSimultaneous<3) {
			
					
					if (linea==null) {
						break;
						}
					Thread T1 = new Thread(new ThreadEmision(linea));        
					T1.start();
					arrThreads.add(T1);	
					maxSimultaneous=maxSimultaneous+1;	
					linea = jt.lecturaLinea(bufer2);
				
				}
			
			
				for (int i = 0; i < arrThreads.size(); i++) 
				{
					arrThreads.get(i).join(); 
				}
				
				arrThreads.clear();
				maxSimultaneous=0;
			}
			
		
			logger.info("Saliendo del while");

			logger.info("tiempo de fin de test " + java.time.LocalDateTime.now());
			logger.info("Finalizando cotización");
		

		}

		

	

	@After
	public void endTest() {

		jt.cerrarLectura(bufer);
		jt.cerrarLectura(bufer2);

	}

}
