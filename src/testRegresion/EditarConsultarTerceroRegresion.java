package testRegresion;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import test.IniciarDriver;
import test.ProcesosRecurrentes;
import thread.ThreadEditarConsultarTercero;
import utils.JavaTools;
import utils.SeleniumTools;



public class EditarConsultarTerceroRegresion {

	JavaTools jt = new JavaTools();
	SeleniumTools st = new SeleniumTools();
	IniciarDriver iniciarDriver;
	ProcesosRecurrentes procesosRecurrentes = new ProcesosRecurrentes();
	ThreadEditarConsultarTercero threadEditarConsultarTercero;

	
	String rutaDirectorio=System.getProperty("user.dir");
	
	
	BufferedReader bufer = jt.abrirLectura(rutaDirectorio,"ParametrosAmbiente.txt");
	String lineaParametrosAmbiente = jt.lecturaLinea(bufer);
	String separador1[] = lineaParametrosAmbiente.split("=");
	int tiempoDelay = Integer.parseInt(separador1[0]);// tiempo de delay de automatizacion
	int tiempoDivWait = Integer.parseInt(separador1[1]);// tiempo wait Big Layer
	int tiempoThrowWait = Integer.parseInt(separador1[2]);// tiempo de los wait
	//String driverPath = separador1[3];// Direccion del driver
	String PaginaWeb = separador1[3];// Direccion http de la prueba
	//public WebDriver FirefoxDriver;
	public String nroCotSeparado = "vacio";
	BufferedReader bufer2 = jt.abrirLectura(rutaDirectorio,"ParametrosConsultaEndosoTerceros.txt");

	@Before
	public void startBrowser() {

	}

	@Test
	public void testConsultarTerceroRegresion() throws InterruptedException, IOException{
	
			Logger logger=Logger.getLogger("testEditarConsultarTerceros");
			PropertyConfigurator.configure("Log4j.properties");
			
			
			jt.cerrarLectura(bufer);

			ArrayList<Thread> arrThreads = new ArrayList<>();
			
			String linea = null;
			
			while ((linea = jt.lecturaLinea(bufer2)) != null) {
				
				Thread T1 = new Thread(new ThreadEditarConsultarTercero(linea));                
			    T1.start();
				
				//new ThreadCotizacionVidaInversion(linea).start();
				arrThreads.add(T1);
				logger.info("Bucle de thread");
				
			}
			
			/*
			for (int i = 0; i < arrThreads.size(); i++) 
			{
			    arrThreads.get(i).join(); 
			}
			*/ 
			//Atrapar los hilos en espera hasta que termine el ultimo
			
			logger.info("Saliendo del while de EditarConsultarTerceros");

			logger.info("tiempo de fin de test " + java.time.LocalDateTime.now());
			logger.info("Finalizando EditarConsultarTerceros");

	}


	@After
	public void endTest() {

		jt.cerrarLectura(bufer2);

	}

}