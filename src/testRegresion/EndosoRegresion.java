package testRegresion;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import test.IniciarDriver;
import test.ProcesosRecurrentes;
import thread.ThreadEndoso;
import utils.JavaTools;
import utils.SeleniumTools;



public class EndosoRegresion {

	JavaTools jt = new JavaTools();
	SeleniumTools st = new SeleniumTools();
	IniciarDriver iniciarDriver;
	ProcesosRecurrentes procesosRecurrentes = new ProcesosRecurrentes();
	ThreadEndoso threadEndoso;
	private WebDriver webDriver;
	
	String rutaDirectorio=System.getProperty("user.dir");
	
	
	BufferedReader bufer = jt.abrirLectura(rutaDirectorio,"ParametrosAmbiente.txt");
	String lineaParametrosAmbiente = jt.lecturaLinea(bufer);
	String separador1[] = lineaParametrosAmbiente.split("=");
	int tiempoDelay = Integer.parseInt(separador1[0]);// tiempo de delay de automatizacion
	int tiempoDivWait = Integer.parseInt(separador1[1]);// tiempo wait Big Layer
	int tiempoThrowWait = Integer.parseInt(separador1[2]);// tiempo de los wait
	//String driverPath = separador1[3];// Direccion del driver
	String PaginaWeb = separador1[3];// Direccion http de la prueba
	//public WebDriver FirefoxDriver;
	public String nroCotSeparado = "vacio";
	BufferedReader bufer2 = jt.abrirLectura(rutaDirectorio,"ParametrosEndoso.txt");
	
	String rutaParametrosEndoso = rutaDirectorio + "/ParametrosEndoso.txt";
	
	BufferedReader bufer3 = jt.abrirLectura(rutaDirectorio,"PolizasCreadas.txt");
	

	

	@Before
	public void startBrowser() {

	}

	@Test
	public void testEndoso() throws InterruptedException, IOException{

			Logger logger=Logger.getLogger("testEndoso");
			PropertyConfigurator.configure("Log4j.properties");
			
			
			jt.cerrarLectura(bufer);

			ArrayList<Thread> arrThreads = new ArrayList<>();
			
			String linea = null;
			String lineaNrosPoliza = null;
			
			while ((linea = jt.lecturaLinea(bufer2)) != null) {
				
				lineaNrosPoliza=jt.lecturaLinea(bufer3);
				
				Thread T1 = new Thread(new ThreadEndoso(linea,lineaNrosPoliza));                
			    T1.start();
				
				arrThreads.add(T1);
				logger.info("Bucle de thread");
				
			}
			
			
			for (int i = 0; i < arrThreads.size(); i++) 
			{
			    arrThreads.get(i).join(); 
			}
			
			//Atrapar los hilos en espera hasta que termine el ultimo
			
			logger.info("Saliendo del while de endoso");

			logger.info("tiempo de fin de test " + java.time.LocalDateTime.now());
			logger.info("Finalizando Endosos");


	}

	public WebDriver getFirefoxDriver() {
		return webDriver;
	}

	public void setFirefoxDriver(WebDriver firefoxDriver) {
		this.webDriver = firefoxDriver;
	}

	@After
	public void endTest() {

		jt.cerrarLectura(bufer2);

	}

}