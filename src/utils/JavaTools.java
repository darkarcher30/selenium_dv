package utils;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;



public class JavaTools {

	void println(String msj) {
		System.out.println(msj);

	}
	
	public void purgeDirectory(File dir) {
	    for (File file: dir.listFiles()) {
	        if (file.isDirectory())
	            purgeDirectory(file);
	        file.delete();
	    }
	}

	public String getFechaHoy() {

		Date myDate = new Date();

		String fechaHoy = new SimpleDateFormat("dd-MM-yyyy").format(myDate);
		return fechaHoy;
	}

	public String separarNro(String nro) {
		String nroSeparated = nro.substring(nro.indexOf("(") + 1, nro.indexOf(")"));
		return nroSeparated;
	}

	public void crearTxt(String ruta) {

		FileWriter fichero = null;
		try {
			fichero = new FileWriter(ruta);
			fichero.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void escribirTxt(String ruta, String msg) {
		File file= new File(ruta);
		FileWriter fw;
		PrintWriter pw = null;
		
		try {
		
		if (file.exists())
		{
		   fw = new FileWriter(file,true);//if file exists append to file. Works fine.
		}
		else
		{
		   file.createNewFile();
		   fw = new FileWriter(file);
		}
				
			pw = new PrintWriter(fw);
			pw.println(msg);
			fw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public BufferedReader abrirLectura(String ruta, String NombreArchivo) {
		String Narchivo = ruta +"/"+NombreArchivo;
		String fileName = Narchivo;

		BufferedReader bufer = null;
		try {
			bufer = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), "utf-8"));
		} catch (UnsupportedEncodingException e) {
			System.out.println("El archivo txt no es utf-8 \n");
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			System.out.println("No se encuentra el txt \n");
			e.printStackTrace();
		}
		return bufer;
	}

	public String lectura(BufferedReader bufer) {

		String contenido = "vacio";
		try {
			String linea = bufer.readLine();// lee la proxima linea
			String separador[] = linea.split("=");
			contenido = separador[1];
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return contenido;
	}

	public String lecturaLinea(BufferedReader bufer) {

		String contenido = "vacio";
		try {
			contenido = bufer.readLine();// lee la proxima linea
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return contenido;
	}

	public void cerrarLectura(BufferedReader bufer) {
		try {
			bufer.close();// cerrar buffer

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void deleteFile(String ruta) {
		
		File archivo = new File(ruta);
			
		try {
			if (archivo.delete()) {
				
				System.out.println("El archivo fue borrado satisfactoriamente: "+ruta+"\n");
			}
			else {
				
				System.out.println("No se ha podido borrar el archivo: "+ruta+"\n");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	
	// class variable
	final String lexicon = "ABCDEFGHIJKLMNOPQRSTUVWXYZ12345674890";

	final java.util.Random rand = new java.util.Random();

	// consider using a Map<String,Boolean> to say whether the identifier is being used or not 
	final Set<String> identifiers = new HashSet<String>();

	public String randomIdentifier() {
	    StringBuilder builder = new StringBuilder();
	    while(builder.toString().length() == 0) {
	        int length = rand.nextInt(5)+5;
	        for(int i = 0; i < length; i++) {
	            builder.append(lexicon.charAt(rand.nextInt(lexicon.length())));
	        }
	        if(identifiers.contains(builder.toString())) {
	            builder = new StringBuilder();
	        }
	    }
	    return builder.toString();
	}

}
