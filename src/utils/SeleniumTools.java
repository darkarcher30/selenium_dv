package utils;
import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SeleniumTools {
	
	
	private String instaciaG;
	private String nombreTest;
	private String horaTest;
	

	public void cambiarVentanaPPAL(WebDriver driver) {
	Set<String> handlesSet = driver.getWindowHandles();
    List<String> handlesList = new ArrayList<String>(handlesSet);
    String estaVentana=driver.getWindowHandle();
    System.out.println("esta ventana es: "+estaVentana+" \n");
    //driver.switchTo().window(handlesList.get(1));
    driver.close();
    driver.switchTo().window(handlesList.get(0));
	}
	
	public WebDriver iniciarDriver(String PaginaWeb, String driverPath, int delay) {
		WebDriver firefoxDriver;
		System.out.println("encontrar exe geckodriver \n");
		
		String OS = System.getProperty("os.name").toLowerCase();
		
		if (OS.contains("win")){
            System.out.println("This is Windows based system adding .exe");
            driverPath=driverPath+".exe";
		}
		
		System.setProperty("webdriver.gecko.driver", driverPath);
		System.setProperty("Pagina Web", PaginaWeb);
		FirefoxOptions options = new FirefoxOptions();
		System.out.println("creamos nuestro driver \n");
		firefoxDriver = new FirefoxDriver(options);
		firefoxDriver.manage().timeouts().implicitlyWait(delay, TimeUnit.SECONDS);
		return firefoxDriver;
	}


	public WebDriver entrarPagina(String url, String DriverPath) {

		System.setProperty("webdriver.gecko.driver", DriverPath);
		FirefoxOptions options = new FirefoxOptions();
		Map<String, Object> prefs = new HashMap<String, Object>();
		Map<String, Object> profile = new HashMap<String, Object>();
		Map<String, Object> contentSettings = new HashMap<String, Object>();
		contentSettings.put("notifications", 2);
		profile.put("managed_default_content_settings", contentSettings);
		prefs.put("profile", profile);
		// options.setExperimentalOption("prefs", prefs);
		options.addArguments("--disable-plugins");
		options.addArguments("--start-maximized");
		DesiredCapabilities caps = DesiredCapabilities.firefox();
		caps.setCapability("browser", "Firefox");
		// caps.setCapability("browser_version", "49.0");
		caps.setCapability("os", "Windows");
		caps.setCapability("os_version", "10");
		caps.setCapability("resolution", "1280x1024");
		caps.setCapability("browserstack.debug", "true");
		caps.setCapability(FirefoxOptions.FIREFOX_OPTIONS, options);
		WebDriver driver = new FirefoxDriver(options);

		// Url a ingresar
		driver.get(url);

		// driver.manage().window().maximize();
		return driver;

	}

	

	public void waitSearchWicket(WebDriver driver) {
		System.out.println("aguardando cierre de espera acsele ");
		new WebDriverWait(driver, 10000).until(ExpectedConditions.invisibilityOfElementLocated(By.id("waitMessage")));
	}

	public void waitSearchWicketDisapear(WebDriver driver, String Elemento, int time) {
		System.out.println("Espera que elemento desaparezca " + Elemento);
		new WebDriverWait(driver, time).until(ExpectedConditions.invisibilityOfElementLocated(By.id("Elemento")));
	}

	public void waitVaadin(WebDriver driver, String elemento,int time) {
		System.out.println("Espera buscando");
		new WebDriverWait(driver, time).until((ExpectedConditions.invisibilityOfElementLocated(
				By.id(elemento))));
	}

	public WebDriver esperaXpath(WebDriver driver, int segundos, String elemento) {

		try {

			new WebDriverWait(driver, segundos).until(ExpectedConditions.elementToBeClickable(By.xpath(elemento)));

		} catch (Exception e) {
			e.printStackTrace();
		}

		return driver;

	}

	
	public WebDriver clickJSxpath(WebDriver driver,String elemento) {

		WebElement element = driver.findElement(By.xpath(elemento));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		return driver;
	}
	
	
	public WebDriver esperaId(WebDriver driver, int segundos, String elemento) {

		try {

			new WebDriverWait(driver, segundos).until(ExpectedConditions.visibilityOfElementLocated(By.id(elemento)));

		} catch (Exception e) {
			e.printStackTrace();
		}

		return driver;

	}
	


	public WebDriver esperaIdClickable(WebDriver driver, int segundos, String elemento) {

		try {

			new WebDriverWait(driver, segundos).until(ExpectedConditions.elementToBeClickable(By.id(elemento)));

		} catch (Exception e) {
			e.printStackTrace();
		}

		return driver;

	}
	
	
	public WebDriver esperaXpathClickable(WebDriver driver, int segundos, String elemento) {

		try {

			new WebDriverWait(driver, segundos).until(ExpectedConditions.elementToBeClickable(By.xpath(elemento)));

		} catch (Exception e) {
			e.printStackTrace();
		}

		return driver;

	}
	
	public WebDriver esperaNameClickable(WebDriver driver, int segundos, String elemento) {

		try {

			new WebDriverWait(driver, segundos).until(ExpectedConditions.elementToBeClickable(By.name(elemento)));

		} catch (Exception e) {
			e.printStackTrace();
		}

		return driver;

	}
	
	public WebDriver visibilityOfElementLocated(WebDriver driver, int segundos, String elemento) {

		try {

			new WebDriverWait(driver, segundos).until(ExpectedConditions.visibilityOfElementLocated(By.id(elemento)));

		} catch (Exception e) {
			e.printStackTrace();
		}

		return driver;

	}

	public WebDriver esperaByName(WebDriver driver, int segundos, String elemento) {

		try {

			new WebDriverWait(driver, segundos).until(ExpectedConditions.visibilityOfElementLocated(By.name(elemento)));

		} catch (Exception e) {
			e.printStackTrace();
		}

		return driver;

	}

	public WebDriver waitSelectPopulateXpath(WebDriver driver, Integer segundos, String xPath) {

		try {
			esperaXpath(driver, segundos, xPath);
			Thread.sleep(100);
			Select select;
			for (int second = 0; second <= 60; second++) {
				select = new Select(driver.findElement(By.xpath(xPath)));
				Thread.sleep(10);
				if (select.getOptions().size() > 1) {
					break;
				}
				Thread.sleep(100);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return driver;
	}

	public WebDriver waitSelectPopulateid(WebDriver driver, Integer segundos, String id) {

		try {
			esperaId(driver, segundos, id);
			Thread.sleep(100);
			Select select;
			for (int second = 0; second <= 60; second++) {
				select = new Select(driver.findElement(By.id(id)));
				Thread.sleep(10);
				if (select.getOptions().size() > 1) {
					break;
				}
				Thread.sleep(100);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return driver;
	}
	
	
	public WebDriver waitSelectPopulatexpath(WebDriver driver, Integer segundos, String xpath) {

		try {
			esperaXpath(driver, segundos, xpath);
			Thread.sleep(100);
			Select select;
			for (int second = 0; second <= 60; second++) {
				select = new Select(driver.findElement(By.xpath(xpath)));
				Thread.sleep(10);
				if (select.getOptions().size() > 1) {
					break;
				}
				Thread.sleep(100);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return driver;
	}

	public WebDriver waitSelectPopulatename(WebDriver driver, Integer segundos, String name) {

		try {
			esperaId(driver, segundos, name);
			Thread.sleep(100);
			Select select;
			for (int second = 0; second <= 60; second++) {
				select = new Select(driver.findElement(By.name(name)));
				Thread.sleep(10);
				if (select.getOptions().size() > 1) {
					break;
				}
				Thread.sleep(100);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return driver;
	}

	public void changeLastWindows(WebDriver driver) throws InterruptedException, IOException {
		// Cambiar de ventana
		// getWindowHandles() method returns the ids of all active Windows and its
		// return type will be a Collection Set.
		Set<String> sid = driver.getWindowHandles();
		// Using iterator we can fetch the values from Set.
		List<String> listWindows = new ArrayList<String>(sid);
		String lastId = listWindows.get(listWindows.size() - 1);
		// swtiching control to child Window
		driver.switchTo().window(lastId);
		Thread.sleep(2000);
	}

	public void cambiarVentana(WebDriver driver) {
		// Cambiar de ventana
		// getWindowHandles() method returns the ids of all active Windows and its
		// return type will be a Collection Set.
		Set<String> sid = driver.getWindowHandles();
		// Using iterator we can fetch the values from Set.
		Iterator<String> it = sid.iterator();
		String parentId = it.next();// no se puede cambiar esta linea o no cambia de ventana al no incrementar el
									// iterador
		System.out.println(parentId);
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		String childId = it.next();
		 System.out.println(childId);
		// swtiching control to child Window
		driver.switchTo().window(childId);
	}

	public void regresarVentana(WebDriver driver) throws InterruptedException, IOException {
		// Cambiar de ventana
		// getWindowHandles() method returns the ids of all active Windows and its
		// return type will be a Collection Set.
		Set<String> sid = driver.getWindowHandles();
		// Using iterator we can fetch the values from Set.
		Iterator<String> it = sid.iterator();
		String parentId = it.next();
		System.out.println(parentId);
		Thread.sleep(2000);
		String childId = it.next();
		System.out.println(childId);
		// swtiching control to child Window
		driver.switchTo().window(parentId);
	}

	public void cambioPantalla(WebDriver driver) {
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		driver.get(driver.getCurrentUrl());
	}

	public String horaCarpeta() {
		Calendar calendario = Calendar.getInstance();

		int hora, minutos, segundos, dia, mes, anio;
		String nombreCarpeta;

		hora = calendario.get(Calendar.HOUR_OF_DAY);
		minutos = calendario.get(Calendar.MINUTE);
		segundos = calendario.get(Calendar.SECOND);
		dia = calendario.get(Calendar.DAY_OF_MONTH);
		mes = calendario.get(Calendar.MONTH);
		anio = calendario.get(Calendar.YEAR);

		System.out.println(anio + "-" + (mes + 1) + "-" + dia + "_" + hora + "." + minutos + "." + segundos);
		nombreCarpeta = anio + "-" + (mes + 1) + "-" + dia + "_" + hora + "." + minutos + "." + segundos;
		return nombreCarpeta;
	}

	public boolean isAlertPresent(WebDriver driver) {
		try {
			driver.switchTo().alert();
			return true;
		} catch (NoAlertPresentException Ex) {
			return false;
		}
	}

	public void alertJavaScriptAceptar(WebDriver driver) {

		try {
			// Mensajes de Alerta JavaScript
			if (isAlertPresent(driver)) {
				Alert alert = driver.switchTo().alert();
				String alertmess = alert.getText();
				alert.accept();
				System.out.println("Mensaje del alert: " + alertmess);
				Thread.sleep(1000);
				driver.switchTo().defaultContent();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void ScreenShotJavaScript(String nombrePrueba, String folderName, String titulo,	String ruta2Screen) throws InterruptedException, IOException, AWTException {
		String rutaScreen = "";
		String fileName = "";
		// String oS = System.getProperty("os.name");
		rutaScreen = "C:\\ScrenShots\\" + getInstaciaG() + "\\";
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Rectangle screenRectangle = new Rectangle(screenSize);
		Robot robot = new Robot();
		BufferedImage image = robot.createScreenCapture(screenRectangle);
		System.out.println("tomando screenshot " + titulo);
		System.out.println("Ruta del archivo  " + fileName);
		fileName = rutaScreen + nombrePrueba + "\\" + folderName + "\\"/* + i + "\\" */ + titulo + ".png";
		ImageIO.write(image, "png", new File(fileName));
	}
	
	
	
	public String captureScreen(WebDriver driver,String Path, String ProcessName) {
	    String path;
	    try {
	        WebDriver augmentedDriver = new Augmenter().augment(driver);
	        File source = ((TakesScreenshot)augmentedDriver).getScreenshotAs(OutputType.FILE);
	        path = Path +"/"+ProcessName+"_"+source.getName();
	        FileUtils.copyFile(source, new File(path)); 
	    }
	    catch(IOException e) {
	        path = "Failed to capture screenshot in "+ProcessName+": " + e.getMessage();
	    }
	    return path;
	}
	
	

	public void waitforElementThenClick(WebDriver driver, WebElement element) throws InterruptedException {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.elementToBeClickable(element));
			element.click();
		} catch (Exception e) {
		}
	}
	
	
	public void tsleep(int time) throws InterruptedException {
		Thread.sleep(time);
		}


	public String getInstaciaG() {
		return instaciaG;
	}

	public void setInstaciaG(String instaciaG) {
		this.instaciaG = instaciaG;
	}

	public String getNombreTest() {
		return nombreTest;
	}

	public void setNombreTest(String nombreTest) {
		this.nombreTest = nombreTest;
	}

	public String getHoraTest() {
		return horaTest;
	}

	public void setHoraTest(String horaTest) {
		this.horaTest = horaTest;
	}
	
	
	public String getQuotationValue(WebDriver driver) {

		String quotationValue = null;

		return quotationValue;
	}
	
	

}
