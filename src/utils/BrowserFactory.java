package utils;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

public class BrowserFactory {
	
	public static WebDriver CreateInstance(String browser) {
		
		WebDriver driver = null;
		String driverPath=System.getProperty("user.dir");
		
		try {
			
			if (browser.equalsIgnoreCase("chrome")) {
				
				System.setProperty("webdriver.chrome.driver", driverPath+"/chromedriver");

				ChromeOptions options = new ChromeOptions();
				options.addArguments("--start--maximized");
				driver = new ChromeDriver(options);	
			}
			
				if (browser.equalsIgnoreCase("firefox")) {
				System.out.println("encontrar exe geckodriver \n");
				
				String OS = System.getProperty("os.name").toLowerCase();
				
				driverPath=driverPath+"/geckodriver";
				
				if (OS.contains("win")){
		            System.out.println("This is Windows based system adding .exe");
		            driverPath=driverPath+".exe";
				}
				
				System.setProperty("webdriver.gecko.driver", driverPath);
				
				FirefoxOptions options = new FirefoxOptions();
					
				String rutaDirectorio=System.getProperty("user.dir");
				
				options.addArguments("-private");
				options.addPreference("browser.download.folderList", 2);
				options.addPreference("browser.download.manager.showWhenStarting", false);
				options.addPreference("browser.download.dir", rutaDirectorio+"\\Reports");
				options.addPreference("browser.helperApps.neverAsk.saveToDisk", "application/octet-stream");
				
				System.out.println("creamos nuestro driver \n");
				driver = new FirefoxDriver(options);
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				
			}
		
	}
		catch (Exception e){
			
			e.printStackTrace();
			return driver;
		}
		return driver;
		}

}
