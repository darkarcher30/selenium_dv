package utils;
import org.openqa.selenium.WebDriver;

public class DriverFactory {
	
	private DriverFactory()
	{
		//Do nothing. Do not allow to initialize this class from outside
		
	}
	
	private static DriverFactory instance = new DriverFactory();
	
	public static DriverFactory getInstance() {
		
		return instance;
	}
	
	ThreadLocal<WebDriver> driver = new ThreadLocal<WebDriver>();
	
	public WebDriver getDriver() {
		
		return driver.get();	
	}
	
	
	public void setDriver (WebDriver driverParam)// call this method to set the driver object
	{
		
		driver.set(driverParam);
	}
	
	public void removeDriver() //Quits the driver and closes the browser
	{
		driver.get().quit();
		driver.remove();
		
	}
	
	
}
